#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import time
import datetime
import pytz
from pprint import pprint
from datetime import datetime, timedelta
import re, json
import pymysql
import traceback
from urllib.request import urlopen

import pandas as pd
import sys

import pandas as pd
db = pymysql.connect(host='localhost', port=3306, user='ssq', passwd='tesiISELLI-2017', db='SenSquare')
cursorCov = db.cursor()


today = datetime.now().date()
yesterday = today - timedelta(days=1)
urlRegioni = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv'
col_list_regioni = ["data", "denominazione_regione", "lat", "long", "nuovi_positivi"]
df_regioni = pd.read_csv(urlRegioni,delimiter=',')
data_dati_regioni = df_regioni.data
denominazione_regione_datiRegioni = df_regioni.denominazione_regione
nuovi_positivi_regioni = df_regioni.nuovi_positivi
lat_positivi_regioni = df_regioni.lat
long_positivi_regioni = df_regioni.long


def inserisci_nuovi_positivi(lat_positivi, long_positivi, n_positivi, citta_regione):
    #per prima cosa, qualora non esista ancora, inserisco il nuovo tipo di dato in data classe per identificare i nuovi positivi
    try:
        cursorCov.execute("SELECT ID from DataClasses WHERE (ID = 'COVD')") #controllo che non sia già presente
        if len(cursorCov.fetchall()) == 0:
            #se non è presente lo inserisco io
            cursorCov.execute("INSERT INTO DataClasses(ID, data_type, unit_of_measure) \
                        VALUES ('%s', '%s', '%s')" % ("COVD", "Nuovi Positivi", "unità"))
            db.commit()
        else:
            print()
    except:
        #print("errore causato dall'inserimento del  nuovo datatype in dataclasses")  
        print(traceback.format_exc())

    #faccio solo un'inserimento al giorno, quindi controllo che non siano già stati inseriti i dati della giornata di ieri
    yesterdayModified = str(yesterday)+ " 00:00:00" #adeguo la stringa, utile ad effettuare il confronto
    ansCov = cursorCov.fetchall()
    print(ansCov)
    try:                
        #controllo per l'inserimento dei datastrems
        # ------ NOTA: l'apice singolo" ' " per la valle d'Aosta genera problemi
        #citta_regione.strip("'") 
        citta_regione = citta_regione.replace("'", '')
        #print(citta_regione)#valle d'Aosta senza apice 

        cursorCov.execute("SELECT * FROM DataStreams WHERE ('%s' = any (SELECT device_ID FROM DataStreams WHERE data_class = 'COVD'))" %citta_regione)
        

        datiCov = cursorCov.fetchall()
        streamsInseritiCovid = len(datiCov) 
        print('\t\t\t\tSTREAMS INSERITI COVID (PRIMO PRINT): ', streamsInseritiCovid, "città: ",citta_regione, "\n dati: ")

        #se non sono ancora stati inseriti, inserisco gli streams
        if streamsInseritiCovid == 0:

            stream_name_cov = citta_regione

            cursorCov.execute("INSERT INTO DataStreams(device_ID, name, data_class, last_entry_ID, reliability, accuracy, update_rate, description) \
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (citta_regione, stream_name_cov, "COVD", "-1", "100", "100", "1.0", "Totale positivi covid-19"))
            #print('--------\t\t\t stream COVID inserito in datastreams, città:',citta_regione)
            #print('\t\t\t\tSTREAMS INSERITI COVID (SECONDO PRINT)): ', streamsInseritiCovid, "città: ",citta_regione, "\n dati: ")
            db.commit();

       
        #  ------------------------ INIZIO QUERY CHE RIEMPIE TABELLA MEASUERMENTS ------------------------
        #
        #
        #  Per prima cosa devo controllare che non sia ancora stato inserita la misurazione COVID nella giornata di ieri
        #  QUINDI seleziono i codici ID dalla tabella datastreams, delle righe che hanno data_class = COV
        #  Poi vado a vedere se nella tabella measuremts, nella giornata di ieri, sono già stati inseiriti questi codici. 
        #  Se non sono ancora stati inseriti, li inserisco.
        #
        # 
        #  
        #seleziono tutti i data stream id dalla tabella datastream, CON data class uguale a COV
        streamCov = "SELECT ID FROM DataStreams WHERE data_class = 'COVD'"
        cursorCov.execute(streamCov)
        #scorro i data streams trovati e verifico se sono stati inseriti nella giornata di ieri
        myresult = cursorCov.fetchall()
        misurazioniInseriteIeri = 0
        for x in myresult:
            print(x)
            idStream = ''.join(str(x)) #converto la tupla in stringa
            print("tupla in stringa: ",idStream)
            idStream = idStream.replace("(", '')
            idStream = idStream.replace(")", '')
            idStream = idStream.replace(",", '')
            print("tupla in stringa: ",idStream)

            
            cursorCov.execute("SELECT ID FROM Measurements WHERE (timestamp='"+yesterdayModified+"' AND data_stream_ID='"+idStream+"')")

            misurazioniInseriteIeri = len(cursorCov.fetchall())
           
            print("MISURAZIONI DENTRO IL FOR ",misurazioniInseriteIeri)

    
       
        print(misurazioniInseriteIeri, " <--- misurazioniInseriteIeri e yesterdayModified = ",yesterdayModified, "")
        if misurazioniInseriteIeri == 0:
            if len(ansCov) == 0:
                cursorCov.execute("SELECT ID from DataStreams WHERE (device_ID = '" + citta_regione + "' AND data_class = 'COVD')") 
                stream_ID = cursorCov.fetchall()[0][0]
            else:
                stream_ID = ansCov[0][0]
            cursorCov.execute("INSERT INTO Measurements(data_stream_ID, GPS_latitude, GPS_longitude, MGRS_coordinates, value, timestamp,  Sensor_type) \
                                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (stream_ID, lat_positivi, long_positivi, "-1", n_positivi, yesterday, "COV"))
            db.commit()
            print("New type measurements covid inserted")
        else:
            print("measurements COVID è gia stata inserita per oggi")
            
    except:
        print("")
        print(traceback.format_exc())


i = 0 #ri-azzero l'indice
for r in data_dati_regioni:
    if str(yesterday) in str(r): #vengono convertite in stringhe per verificare che la data di ieri sia contenuta nella parte di data (senza orario) del file json
        print(data_dati_regioni[i], '\t\t ', nuovi_positivi_regioni[i],' \t\t', denominazione_regione_datiRegioni[i] )
        #inserisco nel DB solo i dati reperiti nella giornata di ieri
        inserisci_nuovi_positivi(lat_positivi_regioni[i], long_positivi_regioni[i], nuovi_positivi_regioni[i], denominazione_regione_datiRegioni[i])
    i = i + 1

db.close()
