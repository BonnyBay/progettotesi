#!/usr/bin/env python
#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Script che ottiene la lista del tipo di dati presente su OpenWeather

import requests
import time
import datetime
import pytz
from pprint import pprint
from datetime import datetime, timedelta
import re, json
import pymysql
import traceback
from urllib.request import urlopen

import pandas as pd
import sys
#print(sys.path)
urlRegioni = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv'
df = pd.read_csv(urlRegioni,delimiter=',')



#bologna
latitudeLocal = 44.4990326
longitudeLocal = 11.191602
apikey = 'a358be9af27e9b1c98f08ecda4844b11'
url = 'http://api.openweathermap.org/data/2.5/weather?lat={}&lon={}&appid={}&units=metric'.format(latitudeLocal,longitudeLocal,apikey)
res = requests.get(url)
data = res.json()

db = pymysql.connect(host='localhost', port=3306, user='ssq', passwd='tesiISELLI-2017', db='SenSquare')
cursor = db.cursor()
# -------------------------------------- script per dati meteo --------------------------------------

citta = data['name']
temp = data['main']['temp'] 
humidity = data['main']['humidity']
wind_speed = data['wind']['speed']
idWeather= data['id']
print(data)
today= datetime.now()


try:
    cursor.execute("SELECT ID from DataClasses WHERE (ID = 'TEMP')") #controllo che non sia già presente
    if len(cursor.fetchall()) == 0:
            #se non è presente lo inserisco io
        cursor.execute("INSERT INTO DataClasses(ID, data_type, unit_of_measure) \
                        VALUES ('%s', '%s', '%s')" % ("TEMP", "temperatura", "celsius"))
        db.commit()
    else:
        print()
except:
    print(traceback.format_exc())
try:
    cursor.execute("SELECT ID from DataClasses WHERE (ID = 'HUMY')") #controllo che non sia già presente
    if len(cursor.fetchall()) == 0:
            #se non è presente lo inserisco io
        cursor.execute("INSERT INTO DataClasses(ID, data_type, unit_of_measure) \
                        VALUES ('%s', '%s', '%s')" % ("HUMY", "umidità", "percent"))
        db.commit()
    else:
            #print("We already got this data type in dataclasses")
        print()
except:
        #print("errore causato dall'inserimento del  nuovo datatype in dataclasses")  
    print(traceback.format_exc())
try:
    cursor.execute("SELECT ID from DataClasses WHERE (ID = 'WISP')") #controllo che non sia già presente
    if len(cursor.fetchall()) == 0:
            #se non è presente lo inserisco io
        cursor.execute("INSERT INTO DataClasses(ID, data_type, unit_of_measure) \
                        VALUES ('%s', '%s', '%s')" % ("WISP", "velocità vento", "m/s"))
        db.commit()
            #print("New type inserted")
    else:
            #print("We already got this data type in dataclasses")
        print()
except:
    print(traceback.format_exc())

try: 
	cursor.execute("SELECT ID from Devices WHERE (ID = '" + citta + "')")
	if len(cursor.fetchall()) == 0:
		cursor.execute("UPDATE Devices(ID, name, device_type, participant_ID, daycount) \
				VALUES ('%s', '%s', '%s', '%s', '%s')" % (citta, citta, "OpenWeather", "3","1")) #metto 1 come daycount come test
		db.commit()
		print("Device inserted")
	else:
		print("We already got this device")
except:
	db.rollback()



ans = cursor.fetchall()
print("ANS: ",ans)
stream_name = citta+"città"

def inserisci_wind_speed():
    #controllo che quello stream per quella città non sia stato ancora inserito 
    cursor.execute("SELECT * FROM DataStreams WHERE ('%s' = any (SELECT device_ID FROM DataStreams WHERE data_class = 'WISP'))" %citta)
    #print('cursor.fetchall() ',cursor.fetchall())
    dati = cursor.fetchall()
    streamsInseriti = len(dati)
    #se non sono ancora stati inseriti, inserisco gli streams
    if streamsInseriti == 0:
        cursor.execute("INSERT INTO DataStreams(device_ID, name, data_class, last_entry_ID, reliability, accuracy, update_rate, description) \
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (citta, stream_name, "WISP", "-1", "100", "100", "1.0", "velocità del vento"))
        print('--------\t\t\t stream WISP è STATO ORA inserito in datastreams')
    if len(ans) == 0:
        cursor.execute("SELECT ID from DataStreams WHERE (device_ID = '" + citta + "' AND data_class = 'WISP')")
        stream_ID = cursor.fetchall()[0][0]
        #print('stream ID: ',stream_ID)

    else:
        stream_ID = ans[0][0]
    db.commit()

    cursor.execute("INSERT INTO Measurements(data_stream_ID, GPS_latitude, GPS_longitude, MGRS_coordinates, value, timestamp,  Sensor_type) \
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (stream_ID, latitudeLocal, longitudeLocal, "-1", wind_speed, today, "OWM"))
    db.commit()


def inserisci_temperatura():
   
    #controllo che quello stream per quella città non sia stato ancora inserito 
    cursor.execute("SELECT * FROM DataStreams WHERE ('%s' = any (SELECT device_ID FROM DataStreams WHERE data_class = 'TEMP'))" %citta)
    dati2 = cursor.fetchall()
    streamsInseritiTemp = len(dati2)    
    #se non sono ancora stati inseriti, inserisco gli streams
    if streamsInseritiTemp == 0:
        cursor.execute("INSERT INTO DataStreams(device_ID, name, data_class, last_entry_ID, reliability, accuracy, update_rate, description) \
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (citta, stream_name, "TEMP", "-1", "100", "100", "1.0", "temperatura"))

        print('--------\t\t\t stream TEMP è STATO ORA inserito in datastreams')
    if len(ans) == 0:
        cursor.execute("SELECT ID from DataStreams WHERE (device_ID = '" + citta + "' AND data_class = 'TEMP')")
        stream_ID = cursor.fetchall()[0][0]
    else:
        stream_ID = ans[0][0]
    cursor.execute("INSERT INTO Measurements(data_stream_ID, GPS_latitude, GPS_longitude, MGRS_coordinates, value, timestamp,  Sensor_type) \
					VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (stream_ID, latitudeLocal, longitudeLocal, "-1", temp, today, "OWM"))
    db.commit()
def inserisci_umidita():
    #CONTROLLO INSERIMENTO DATA STREAMS
    cursor.execute("SELECT * FROM DataStreams WHERE ('%s' = any (SELECT device_ID FROM DataStreams WHERE data_class = 'HUMY'))" %citta)
    streamsInseriti = len(cursor.fetchall())
    #se non sono ancora stati inseriti, inserisco gli streams
    if streamsInseriti == 0:
        cursor.execute("INSERT INTO DataStreams(device_ID, name, data_class, last_entry_ID, reliability, accuracy, update_rate, description) \
                        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (citta, stream_name, "HUMY", "-1", "100", "100", "1.0", "Umidità"))
        print('--------\t\t\t stream HUMY è STATO ORA inserito in datastreams')

    if len(ans) == 0:
        cursor.execute("SELECT ID from DataStreams WHERE (device_ID = '" + citta + "' AND data_class = 'HUMY')")
        stream_ID = cursor.fetchall()[0][0]
    else:
        stream_ID = ans[0][0]
    cursor.execute("INSERT INTO Measurements(data_stream_ID, GPS_latitude, GPS_longitude, MGRS_coordinates, value, timestamp,  Sensor_type) \
					VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (stream_ID, latitudeLocal, longitudeLocal, "-1", humidity, today, "OWM"))
    db.commit()

try:
    inserisci_wind_speed()
    inserisci_temperatura()
    inserisci_umidita()
except:
	db.rollback()
	print(traceback.format_exc())

db.close()
