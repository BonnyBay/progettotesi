package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.InstanzeDeiTemplate.AdapterInstanzeSenSquare;
import com.example.anhealth.MainActivity;
import com.example.anhealth.MostraTemplateCostruiti.AdapterTuoiTemplate;
import com.example.anhealth.MostraTemplateCostruiti.FragmentTemplateLocali;
import com.example.anhealth.MostraTemplateCostruiti.FragmentTemplateRemoti;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class SchermataTueInstanze extends AppCompatActivity {
    RadioGroup radioGroup;
    RadioButton selectedRadioButton;
    Button buttonSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_tue_instanze);

        buttonSubmit = (Button) findViewById(R.id.btnSubmit2);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup2);


        buttonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectedRadioButton  = (RadioButton)findViewById(radioGroup.getCheckedRadioButtonId());
                String scelta = selectedRadioButton.getText().toString();
                if(scelta.equals("Istanze Locali")){
                    getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_istanze, new FragmentIstanzeLocali()).commit();
                }else{
                    getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_istanze, new FragmentIstanzeRemote()).commit();
                }
            }
        });


    }


}



