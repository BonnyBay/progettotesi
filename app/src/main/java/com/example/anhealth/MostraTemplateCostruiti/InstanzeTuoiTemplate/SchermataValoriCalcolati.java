package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.example.anhealth.R;

import java.util.ArrayList;


public class SchermataValoriCalcolati extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private AdapterTuoiValori mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_valori_calcolati);
    }

    public void costruisciRecyclerView(Context context, ArrayList<Double> listaValoriOttenuti) {

        mRecyclerView = findViewById(R.id.fragment_rec_vedi_valori);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //mAdapter = new AdapterTuoiValori(listaValoriOttenuti);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        ArrayList<Double> listaValoriOttenuti = (ArrayList<Double>) intent.getSerializableExtra("vettoreValoriCalcolati");
        if(listaValoriOttenuti != null){
            if(!listaValoriOttenuti.isEmpty()){
                costruisciRecyclerView(this, listaValoriOttenuti);
                mRecyclerView.setAdapter(mAdapter);

            }
        }

    }
}
