package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.MeasurementsLocal;
import com.example.anhealth.Database.MisurazioniSalute;
import com.example.anhealth.Database.Regola;
import com.example.anhealth.InstanzeDeiTemplate.NotificationHelper;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BroadcastGeofenceTuaIstanza extends BroadcastReceiver {

    int idInstanza;
    private Context context;
    NotificationHelper notificationHelper;
    private static final String TAG = "BroadcastGeofenceTuaIst";
    private CustomServiceLocal customServiceLocal;
    private AppDatabase appDatabase;
    private String url = "";


    private  String [] arrayStreams;

    private ArrayList<Integer> idMisurazioniOttenute = new ArrayList<Integer>();
    private ArrayList<MeasurementsLocal> listaMisurazioni = new ArrayList<MeasurementsLocal>();

    private ArrayList<String> vettore_parametri_locali = new ArrayList<String>();


    private ArrayList<Double> listeDiMedie = new ArrayList<Double>();

    private Intent intent;

    private String nomeParametri = "";


    private ArrayList<String> par_Sensquare_E_locali = new ArrayList<String>(); //conterrà in ordine la lista dei par trattati sensquare più i locali
    private int countLoc = 0;
    private int countSens = 0;
    private ArrayList<Double> lista_finale = new ArrayList<Double>(); //conterrà i valori medi per ogni parametro, in ordine di come compaiono nella struttura del template
    private List<MisurazioniSalute> misurazioniSalute = new ArrayList<MisurazioniSalute>();
    private int contatoreValOttenuti = 0;
    private ArrayList<Double> listaDiMedieLocali = new ArrayList<Double>();

    private double media_tra_le_colonnine = 0;
    String [] data_class;
    String[] stringaSplittata;
    SharedPreferences prefs;
    private static Timer timer;
    private static TimerTask timerTask;
    private static boolean uscito = true;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);


        this.context = context;
        this.intent = intent;
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "datiReportDB").build();

        notificationHelper = new NotificationHelper(context);


        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if(geofencingEvent.hasError()){
            Log.d(TAG, "onReceive: ERROR receiving geofence event... ");
            return;
        }

        List<Geofence> geofenceList = geofencingEvent.getTriggeringGeofences(); //ottengo una lista di geofences che hanno triggerato (fatto scattare) il mio geofence transition
        for(Geofence geofence: geofenceList){
            Log.d(TAG, "onReceive: "+geofence.getRequestId());
        }

        int transitionType = geofencingEvent.getGeofenceTransition();

        switch (transitionType){
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                timer = new Timer();

                Toast.makeText(context, "Sei entrato dentro la zona dell'istanza", Toast.LENGTH_SHORT).show();
                //notificationHelper.sendHighPriorityNotification("SEI ENTRATO NELL'AREA DELL'INSTANZA", "Sei entrato nell'area Geofence", MappaTuaIstanza.class);
                idInstanza = intent.getIntExtra("idTuaInstanza2",0);
                uscito = false;
                //eseguo l'instanza sulla base dell'id e degli id stream presenti in quella zona
                Log.d(TAG, "OTTENUTO DALL'INTENT idInstanza "+idInstanza);
                //new AsyncTaskGetIstanzaFiltrata().execute();

                int minuti_shared = prefs.getInt("pickerVal", 5);
                //moltiplico il valore minuti_shared per 1000
                Log.d(TAG, "minuti_shared: "+minuti_shared);
                int minuti = minuti_shared*60*1000;

                //Set the schedule function and rate

                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        //Prima controllo che il service sia in running. Se non è più in running, interrompo l'esecuzione del timer ...
                        if (!MyService.isRunning()) { //controllo che il monitoraggio della posizione usando il fused location sia attiva.
                            timer.cancel();
                        } else if(uscito==false){
                            Log.d(TAG, " --------------------------------- run: ALTRO GIRO ISTANZA " + idInstanza + " ---------------------------------");
                            //inizializzo
                            contatoreValOttenuti = 0;
                            idMisurazioniOttenute.clear();
                            listaMisurazioni.clear();
                            lista_finale.clear();
                            listeDiMedie.clear();
                            listaMisurazioni.clear();
                            listaDiMedieLocali.clear();
                            misurazioniSalute.clear();
                            par_Sensquare_E_locali.clear();
                            vettore_parametri_locali.clear();
                            countLoc = 0;
                            countSens = 0;
                            //conteggio = 0;
                            //conteggioMisurazioni = 0;
                            conta_chiamate = 0;
                            listaDiRisultati.clear();

                            //mi salvo nel database che l'istanza è in stato attivo

                            new AsyncTaskGetStatoIstanza2().execute();

                        }else{
                            cancel();
                            timer.purge();
                        }
                    }
                };

                timer.scheduleAtFixedRate(timerTask,0, minuti);


                break;
                /*
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                //il DWEEL va sampato ogni tot minuti
                Toast.makeText(context, "Sei dentro la zona da 10 secondi", Toast.LENGTH_SHORT).show();
                idInstanza = intent.getIntExtra("idTuaInstanza2",0);

                //eseguo l'instanza sulla base dell'id e degli id stream presenti in quella zona
                Log.d(TAG, "idInstanza "+idInstanza);
                new AsyncTaskGetIstanzaFiltrata().execute();


                break;*/
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                uscito = true;
                Toast.makeText(context, "Sei uscito dalla zona dell'istanza", Toast.LENGTH_SHORT).show();
                notificationHelper.sendHighPriorityNotification("SEI USCITO DALL'AREA DELL'INSTANZA", "Sei uscito dall'area Geofence", MappaTuaIstanza.class);
                //svuolo la lista dell'area geofence
                listaMisurazioni.clear();
                timerTask.cancel();
                timer.purge();
                timer.cancel();
                //somma = 0;
                break;
        }


    }




    //async task per ottenere il valore delle misurazioni
    public class AsyncTaskAggiungiMisurazione extends AsyncTask<MeasurementsLocal, Void, Void> {
        @Override
        protected Void doInBackground(MeasurementsLocal... measurementsLocal) {
            appDatabase.daoMeasurements().aggiungiMisurazione(measurementsLocal[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "MISURAZIONE SALVATA NEL DATABASE ");
            //Toast.makeText(context, "Misurazione aggiunta al database",
              //      Toast.LENGTH_SHORT).show();

            //dopo che ho salvato nel database le misurazioni, devo prenderle e calcolare la media
        }
    }

    /** ############### STRATEGIA IMPLEMENTATIVA ###############
     *
     * Recupero dalla tabella regole,tutte le regole che possiedono
     * l'id del template uguale a quello del template sul quale si basa
     * la seguente istanza
     *
     * */
    //questo id lo ritroveremo anche nella tabella regole, perchè ogni regola possiede l'id del template al quale fa riferimento
    long idTemplateAssociato = 0; // id template associato all'istanza e recuperato dal database.
    public void caricaRegole(){
        //oppure lo prendo dal db
        idTemplateAssociato = Long.parseLong(customServiceLocal.getArgs());
        Log.d(TAG, "ID TEMPLATE PRESO DAL DB DELLA TABELLA ISTANZE: "+idTemplateAssociato);
        //ottento le regole
        new AsyncTaskGetRegoleFiltrate().execute();
    }

    int risultato = 0; //per capire che tipo di risultato mi restituisce ogni regola
    ArrayList<Integer> listaDiRisultati = new ArrayList<Integer>();
    boolean lanciaNotifica = false;
    double valoreCalcolato = 0.0;
    ArrayList<Double> listaValoriOttenuti = new ArrayList<Double>();

    public void calcolaValoreTemplate(){
        int contatore = 0; //variabile che mi serve per recuperare il giusto valor medio (la sua posizione è uguale a quella della lista regole sottostante)
        for(Regola regola:listaRegole){

            Log.d(TAG, "parametro "+regola.getParametro());
            Log.d(TAG, "template id associato "+regola.getTemplate_id());

            //mi calcolo il valore di ogni regola
            double mediaParametro = lista_finale.get(contatore); // esempio media del parametro PM10
            String operatorePrimario = regola.getOperatorePrimo();
            double valoreCostante = regola.getCostante();
            switch (operatorePrimario){
                case "=":
                    if(mediaParametro == valoreCostante){
                        risultato = 1;
                    }else{
                        risultato = 0;
                    }
                    listaDiRisultati.add(risultato);

                    break;
                case ">":
                    if(mediaParametro > valoreCostante){
                        risultato = 1;
                        //devo comunque mostrare LA MEDIA del valore nella schermata sottostante la mappa
                    }else{
                        risultato = 0;
                    }
                    listaDiRisultati.add(risultato);

                    break;
                case "<":
                    if(mediaParametro < valoreCostante){
                        risultato = 1;
                    }else{
                        risultato = 0;

                    }
                    listaDiRisultati.add(risultato);

                    break;
                case "*":
                    valoreCalcolato = mediaParametro * valoreCostante;
                    risultato = 1;
                    listaValoriOttenuti.add(valoreCalcolato);
                    listaDiRisultati.add(risultato);

                    break;
                case "/":
                    valoreCalcolato = mediaParametro / valoreCostante;
                    risultato = 1;
                    listaValoriOttenuti.add(valoreCalcolato);
                    listaDiRisultati.add(risultato);

                    break;
                case "-":
                    valoreCalcolato = mediaParametro - valoreCostante;
                    risultato = 1;
                    listaValoriOttenuti.add(valoreCalcolato);
                    listaDiRisultati.add(risultato);

                    break;
                case "+":
                    valoreCalcolato = mediaParametro + valoreCostante;
                    risultato = 1;
                    listaValoriOttenuti.add(valoreCalcolato);
                    listaDiRisultati.add(risultato);

                    break;
            }


            Log.d(TAG, "OPERATORE SECONDO: --> "+regola.getOperatoreSecondo());

            if(regola.getOperatoreSecondo().equals("Nessun operatore inserito")){//vuol dire che ho inserito solo una regola

                //non c'è l'operatore centrale

            }else{//se ci fosse l'operatore centrale ...
                switch (regola.getOperatoreSecondo()){
                    case "AND":
                        //controllo che gli ultimi due valori salvati nell'arraylist siano ENTRAMBI 1
                        if(listaDiRisultati.get(contatore) == 1 && listaDiRisultati.get(contatore-1) == 1){
                            risultato = 1;
                        }else {
                            risultato = 0;
                        }
                        listaDiRisultati.add(risultato);
                        break;
                    case "OR":
                        //controllo che almeno uno degli ultimi due valori salvati nell'arraylist sia 1
                        if(listaDiRisultati.get(contatore) == 1 || listaDiRisultati.get(contatore-1) == 1){
                            if(listaDiRisultati.get(contatore) == 0){
                                listaDiRisultati.remove(listaDiRisultati.get(contatore));
                            }else if(listaDiRisultati.get(contatore-1) == 0){
                                listaDiRisultati.remove(listaDiRisultati.get(contatore-1));
                            }
                            Log.d(TAG, "DENTRO ALL OR: "+listaDiRisultati);
                            risultato = 1;
                        }else {
                            risultato = 0;
                        }
                        listaDiRisultati.add(risultato);
                        break;
                    case "=": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        //verifico la loro uguaglianza
                        if(listaValoriOttenuti.get(contatore) == listaValoriOttenuti.get(contatore-1)){
                            risultato = 1;
                        }else{
                            risultato = 0;
                        }
                        listaDiRisultati.add(risultato);
                        break;
                    case ">": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        //verifico la loro uguaglianza
                        if(listaValoriOttenuti.get(contatore-1) > listaValoriOttenuti.get(contatore)){
                            risultato = 1;
                        }else{
                            risultato = 0;
                        }
                        listaDiRisultati.add(risultato);
                        break;
                    case "<": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        //verifico la loro uguaglianza
                        if( listaValoriOttenuti.get(contatore-1) < listaValoriOttenuti.get(contatore)){
                            risultato = 1;
                        }else{
                            risultato = 0;

                        }
                        listaDiRisultati.add(risultato);
                        break;
                    case "*": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        listaValoriOttenuti.remove(listaValoriOttenuti.get(contatore));
                        Log.d(TAG, "rimosso "+listaValoriOttenuti.get(contatore));
                        listaValoriOttenuti.remove(listaValoriOttenuti.get(contatore));
                        Log.d(TAG, "rimosso "+listaValoriOttenuti.get(contatore));
                        valoreCalcolato = listaValoriOttenuti.get(contatore) * listaValoriOttenuti.get(contatore-1);
                        Log.d(TAG, "sostituiti con: "+valoreCalcolato);
                        listaValoriOttenuti.add(valoreCalcolato);
                        break;
                    case "/": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        valoreCalcolato = listaDiRisultati.get(contatore) / listaDiRisultati.get(contatore-1);
                        Log.d(TAG, "sostituiti con: "+valoreCalcolato);
                        listaValoriOttenuti.add(valoreCalcolato);
                        break;
                    case "-": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        valoreCalcolato = listaDiRisultati.get(contatore) - listaDiRisultati.get(contatore-1);
                        Log.d(TAG, "sostituiti con: "+valoreCalcolato);
                        listaValoriOttenuti.add(valoreCalcolato);
                        break;
                    case "+": //controllo gli ultimi due valori nell'arraylist del valori ottenuti
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        listaDiRisultati.remove(listaDiRisultati.get(contatore));
                        Log.d(TAG, "rimosso "+listaDiRisultati.get(contatore));
                        valoreCalcolato = listaDiRisultati.get(contatore) *+listaDiRisultati.get(contatore-1);
                        Log.d(TAG, "sostituiti con: "+valoreCalcolato);
                        listaValoriOttenuti.add(valoreCalcolato);
                        break;
                }
            }


            contatore++;
        }

        lanciaNotifica = false;
        for(int i = 0; i<listaDiRisultati.size(); i++){
            //se c'è almneno uno zero, la condizione del template risulta essere false
            if(listaDiRisultati.get(i) == 0){
                lanciaNotifica = false;
                break;
            }else{
                lanciaNotifica = true;
            }
        }
        Log.d(TAG, "LANCIA NOTIFICAAAA "+lanciaNotifica);
        if(lanciaNotifica){
            notificationHelper.sendHighPriorityNotification("Istanza calcolata!", "La condizione posta dal template risulta essere vera. Controlla l'output nella schermata sottostante la mappa.", MappaTuaIstanza.class);
            lanciaNotifica = false; //per far capire che ho già lanciato la notifica
            //aggiungo i valori alla schermata sottostante la mappa

        }//else mostro comunque i valori nella schermata sottostante


        //PER TESTARE
        Log.d(TAG, "LISTA DEI RISULTATI DEL TEMPLATE ");
        for(int i = 0; i<listaDiRisultati.size(); i++){
            Log.d(TAG, " "+listaDiRisultati.get(i));
        }
        Log.d(TAG, "LISTA DEI VALORI CALCOLATI DAL TEMPLATE ");
        for(int i = 0; i<listaValoriOttenuti.size(); i++){
            Log.d(TAG, " "+listaValoriOttenuti.get(i));
        }

        //in questo modo quando ri aggiorno l'activity non va in crash, grazie ad un controllo nell'onMapReady
        SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("addGeofenceOnes", true);
        editor.apply();
        //mostro i valori nella recycler view che sta sulla activity: MappaTuoiTemplate.java

        String [] parString = nomeParametri.split(" ");

        ArrayList<String> vettoreParametri = new ArrayList<String>();
        if(customServiceLocal.getNomiParametriLocali().equals("")){
            for(int i = 0; i<parString.length; i++){
                //non inserisco i doppioni
                //if(!vettoreParametri.contains(parString[i])){
                    vettoreParametri.add(parString[i]);
                //}
            }
        }else{
            for(int i = 0; i<par_Sensquare_E_locali.size(); i++){
                //non inserisco i doppioni
                //if(!vettoreParametri.contains(par_Sensquare_E_locali.get(i))){
                    vettoreParametri.add(par_Sensquare_E_locali.get(i));
                //}
            }
        }

        ArrayList<Double> listaMedieSenzaDoppioni = new ArrayList<Double>();

        for(int i = 0; i<lista_finale.size(); i++){
            //if(!listaMedieSenzaDoppioni.contains(lista_finale.get(i))){
                listaMedieSenzaDoppioni.add(lista_finale.get(i));

            //}
        }
        ArrayList<String> vettoreLivelloPericolo = new ArrayList<String>(); //indica il livello di pericolosità del valore
        //il size di lista finale dovrebbe essere uguale aquello di vettoreParametri

        Log.d(TAG, "----------------------lista_finale: "+lista_finale);
        Log.d(TAG, "----------------------vettoreParametri: "+vettoreParametri);
        // gestione della stampa del livello che è indicatore, raggiunto dal valore dei parametri
        for(int i = 0; i<lista_finale.size(); i++){
            //if(i<vettoreParametri.size()){ //evito l'index out of bound
                switch (vettoreParametri.get(i)){
                    case "PM10":
                        if(lista_finale.get(i)>50){
                            vettoreLivelloPericolo.add("Critico");
                        }else{
                            vettoreLivelloPericolo.add("Normale");
                        }
                        break;
                    case "PM25":
                        if(lista_finale.get(i)>25){ //anche se 25 è la media annuale
                            vettoreLivelloPericolo.add("Critico");
                        }else{
                            vettoreLivelloPericolo.add("Normale");
                        }
                        break;
                    case "O3OZ":
                        if(lista_finale.get(i)>120){ //uso la media oraria(120), quella giornaliera è 180
                            vettoreLivelloPericolo.add("Critico");
                        }else{
                            vettoreLivelloPericolo.add("Normale");
                        }
                        break;
                    case "COPR":
                        if(lista_finale.get(i)>10){
                            vettoreLivelloPericolo.add("Critico");
                        }else{
                            vettoreLivelloPericolo.add("Normale");
                        }
                        break;
                    default:
                        vettoreLivelloPericolo.add("-");//caso in cui ho i parametri locali
                        break;

                }
            //}

        }

        //conteggio = 0; //la ri-inizializzo qualora venga rieseguito il geofence
        MappaTuaIstanza.setListaParametri(vettoreParametri);
        MappaTuaIstanza.setVettoreValori(listaMedieSenzaDoppioni);
        MappaTuaIstanza.setLivello(vettoreLivelloPericolo);
        MappaTuaIstanza.getMappaTuaIstanza().updateUI(intent);

    }

    //ottengo le regole associate a quel determinato template id
    List<Regola> listaRegole = new ArrayList<Regola>();
    public class AsyncTaskGetRegoleFiltrate extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            listaRegole = appDatabase.daoRegole().getRegolaConFiltro(idTemplateAssociato);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(context, "Regole recuperate dal database ",
              //      Toast.LENGTH_SHORT).show();
            Log.d(TAG, "------ CALCOLO IL VALORE DEL TEMPLATE ------ ");
            calcolaValoreTemplate();

        }
    }



    //metodo che calcola la media delle misurazioni, raggruppate per ogni id del datastream che contengono


    /**
     * Nella tesi questo metodo si chiama "preparaLista"
     * */
    public void calcolaMedia(){
        // -------------------- Devo calcolare la media dei valori che hanno lo stesso id datastream --------------------
        /*
        for(int j = 0; j<arrayStreams.length; j++){ //contiene tutti gli id stream
            Log.d(TAG, "ID STREAM DA CONTROLLARE: "+arrayStreams[j]);
            for(int i = 0; i < listaMisurazioni.size(); i++){
                Log.d(TAG, "ID dello stream: "+arrayStreams[j]);
                if(Integer.parseInt(arrayStreams[j]) == listaMisurazioni.get(i).data_stream_id){
                    Log.d(TAG, "Valore ottenuto da sensquare: "+listaMisurazioni.get(i).getValue());
                    somma = somma +listaMisurazioni.get(i).getValue();
                    numeroValori++;
                }
            }
            //aggiungo la somma all'array list delle medie.
            //ogni posizione dell'array list dovrebbe rappresentare l'id di un data stream
            media = somma / numeroValori;
            listeDiMedie.add(media);
            lista_finale.add(media);
            Log.d(TAG, "MEDIA AGGIUNTA ALL'ARRAY LIST DELLE MEDIE: "+media);
            Log.d(TAG, "PER calcolare la media sono stati usati "+numeroValori+" valori");
            //risetto i valori a 0
            somma = 0;
            media = 0;
            numeroValori = 0;
            //Log.d(TAG, "Lunghezza del vettore delle medie :"+listeDiMedie.size());
            //Log.d(TAG, "Lunghezza del vettore delle misurazioni :"+listaMisurazioni.size())
        }*/

        //a questo punto ho riempito tutto il vettore delle medie con i valori di sensquare.
        //tuttavia se ci fossero parametiri locali, dovrei aggiungere anche loro
        Log.d(TAG, "customServiceLocal.getNomiParametriLocali: "+customServiceLocal.getNomiParametriLocali());
        if(!customServiceLocal.getNomiParametriLocali().equals("")) {//controllo che ci siano parametri locali
            lista_finale.clear();
            String strutturaDelTemplate = customServiceLocal.getStruttura_template();
            String [] strutturaSplittata = strutturaDelTemplate.split(" ");

            String parSensquare = customServiceLocal.getNomiParametri();
            String [] parSensquareSplittata = parSensquare.split(" ");
            Log.d(TAG, "parSensquareSplittata[0]: "+parSensquareSplittata[0]);

            //debugging
            Log.d(TAG, "vettore_parametri_locali.size: "+vettore_parametri_locali);
            Log.d(TAG, "listaDiMedieLocali.size: "+listaDiMedieLocali);
            //fine test debugging

            for(int k = 0; k<strutturaSplittata.length; k++){
                for(int t = 0; t<vettore_parametri_locali.size(); t ++){
                    if(strutturaSplittata[k].equals(vettore_parametri_locali.get(t))){
                        par_Sensquare_E_locali.add(vettore_parametri_locali.get(t));
                        lista_finale.add(listaDiMedieLocali.get(countLoc));
                        countLoc++;
                    }
                }

                for(int t = 0; t<parSensquareSplittata.length; t ++){
                    if(strutturaSplittata[k].equals(parSensquareSplittata[t])){
                        par_Sensquare_E_locali.add(parSensquareSplittata[t]);
                        lista_finale.add(listeDiMedie.get(countSens));
                        countSens++;
                    }
                }
            }
        }
        Log.d(TAG, "lista medie sensquare: "+listeDiMedie);
        //listeDiMedie.clear();
        Log.d(TAG, "lista medie locale: "+listaDiMedieLocali);
        //listaDiMedieLocali.clear();
        Log.d(TAG, "vettore_parametri_locali: "+vettore_parametri_locali);
        Log.d(TAG, "--------- lista_finale: "+lista_finale);
        countLoc = 0;
        countSens = 0;
    }


    public class AsyncTaskGetValoreSalute extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... stringsPar) {
            misurazioniSalute = appDatabase.daoMisurazioniSalute().getUltimoValoreInserito(stringsPar[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "Misurazioni salute ottenute",
            //Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Misurazioni salute ottenute");

            contatoreValOttenuti++;
            if(contatoreValOttenuti<vettore_parametri_locali.size()){
                for(MisurazioniSalute misurazione : misurazioniSalute){
                    Log.d(TAG, "misurazione.getValore(): "+misurazione.getValore()+ " "+misurazione.getData_class());
                    listaDiMedieLocali.add(misurazione.getValore());
                    Log.d(TAG, "listaDiMedieLocali prese dal db (una sola per ogni parametro): "+listaDiMedieLocali);
                }
                new AsyncTaskGetValoreSalute().execute(vettore_parametri_locali.get(contatoreValOttenuti));
            }else{
                //aggiungo i valori ottenuti alla lista di medie
                for(MisurazioniSalute misurazione : misurazioniSalute){
                    Log.d(TAG, "misurazione.getValore(): "+misurazione.getValore()+ " "+misurazione.getData_class());
                    listaDiMedieLocali.add(misurazione.getValore());
                    Log.d(TAG, "listaDiMedieLocali prese dal db (una sola per ogni parametro): "+listaDiMedieLocali);
                }
                // Log.d(TAG, "listaDiMedieLocali: "+listaDiMedieLocali);
                calcolaMedia();
                caricaRegole();

            }


        }
    }

    int conta_chiamate = 0;
    int conteggio_valori = 0;
    double media_valori_misurazioni = 0.0;
    //ottengo le colonnine nella zona e prendo i valori dell'ultima misurazione
    public void ottieniUltimaMisurazione(String urlPaginaDaMostrare){
        RequestQueue queue = Volley.newRequestQueue(context);
        final String url = urlPaginaDaMostrare; //DA CICLARE IN UN FOR per fare tante chiamate quante sono le pagine che contengono i dati richiesti

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for(int i = 0; i<jsonArray.length(); i++) {
                                JSONObject results = jsonArray.getJSONObject(i);

                                Log.d(TAG, "PARAMETRI ASSOCIATI A QUESTO DATA STREAM: " + results.getString("data_class"));

                                //IMPORTANTE PER OTTENERE IL VALORE DELLA MISURAZIONE ... HO UN OGGETTO DENTRO UN ALTRO OGGETTO, ma cosi facendo otterrei solo un valore ...
                                JSONObject measurement = results.getJSONObject("measurement");

                                double valore_misurazione = measurement.getDouble("value");
                                conteggio_valori++;
                                Log.d(TAG, "valore_misurazione "+valore_misurazione);
                                media_tra_le_colonnine = media_tra_le_colonnine + valore_misurazione;

                            }
                            conta_chiamate++;
                            Log.d(TAG, "data_class.length: "+data_class.length);
                            if(conta_chiamate < data_class.length){

                                //calcolo la media
                                media_tra_le_colonnine = media_tra_le_colonnine / conteggio_valori; // qui è dove calcolo la media tra le colonnine
                                Log.d(TAG, "media_tra_le_colonnine: "+media_tra_le_colonnine);
                                listeDiMedie.add(media_tra_le_colonnine);
                                lista_finale.add(media_tra_le_colonnine);

                                media_tra_le_colonnine = 0;
                                conteggio_valori = 0; // lo ri-inizializzo


                                String nuovoUrl = "http://sensquare.disi.unibo.it/api/v1/streams/nearby/"+stringaSplittata[0]+"/"+stringaSplittata[1]+"/"+stringaSplittata[2]+"/"+data_class[conta_chiamate]+"/";
                                ottieniUltimaMisurazione(nuovoUrl);

                            }else{
                                conta_chiamate = 0;
                                Log.d(TAG, "conteggio_valori: "+conteggio_valori);
                                Log.d(TAG, "media_valori_misurazioni: "+media_valori_misurazioni);
                                media_tra_le_colonnine = media_tra_le_colonnine / conteggio_valori;
                                listeDiMedie.add(media_tra_le_colonnine);
                                lista_finale.add(media_tra_le_colonnine);


                                media_tra_le_colonnine = 0;
                                conteggio_valori = 0;


                                Log.d(TAG, "listeDiMedie: "+listeDiMedie);
                                //proseguo
                                if(customServiceLocal.getNomiParametriLocali().equals("")){//se è vuoto (vuol dire che non ci sono parametri locali) procedo solo con i parametri di sensquare
                                    calcolaMedia();
                                    caricaRegole();
                                    Log.d(TAG, "------ NON CI SONO PARAMETRI LOCALI ----");

                                }else{
                                    //per ciascun parametro locale, recupero l'ultimo valore inserito e lo aggiungo alle medie
                                    //quindi non vi è un propria media del parametro locale, dal momento che c'è un solo valore
                                    String [] parametri_locali = customServiceLocal.getNomiParametriLocali().split(",");
                                    for(int k = 0; k<parametri_locali.length; k++){
                                        vettore_parametri_locali.add(parametri_locali[k]);
                                    }
                                    new AsyncTaskGetValoreSalute().execute(vettore_parametri_locali.get(0));

                                }



                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());
                    }
                }
        );
        queue.add(objectRequest);
    }



    //ottengo l'istanza dal database
    public class AsyncTaskGetIstanzaFiltrata extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "+++++++++++++ doInBackground: "+idInstanza);
            customServiceLocal = appDatabase.daoCustomServiceLocal().getIstanzaFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(context, "Istanza recuperata dal database "+customServiceLocal.getId_data_stream_utili(),
            //Toast.LENGTH_SHORT).show();
            //do un delay di 6 secondi per dar tempo all'istanza di arrivare
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // ----------------------------------------------------------------------------------------------
                    // ----------------------------------------| IMPORTANTE |----------------------------------------
                    // ----------------------------------------------------------------------------------------------
                    //una volta ottenuta l'istanza, faccio una chiamata API per ottenere tutte le misurazioni associate agli id data stream di questa istanza
                    String stringaIdStreams = customServiceLocal.getId_data_stream_utili();
                    nomeParametri = customServiceLocal.getNomiParametri();
                    Log.d(TAG, "ECCO LA STRINGA DAL DB "+stringaIdStreams);
                    arrayStreams = stringaIdStreams.split(" "); //contiene tutti solo un data stream (presente nella zona) per ogni data_class trattato dal template
                    //Log.d(TAG, "Lunghezza degli id stream trattati da questa istanza. "+arrayStreams.length);

                    //faccio la chiamata api per ottenere le misurazioni associate a quel id_data_stream
                    //Log.d(TAG, "primo id stream: "+arrayStreams[0]);
                    url = "http://sensquare.disi.unibo.it/api/v1/streams/"+arrayStreams[0]+"/";
                    Log.d(TAG, "DATA STREAM TRATTATI (POSSONO ESSERE RIPETUTI). SIZE = "+arrayStreams.length);
                    for(int i = 0; i<arrayStreams.length; i++){
                        Log.d(TAG, "data stream: "+arrayStreams[i]);
                    }

                    //recupero le misurazioni

                    String data_class_string = customServiceLocal.getNomiParametri();
                    data_class = data_class_string.split(" ");

                    String coordERAggio = prefs.getString("coordERaggio", " ");
                    Log.d(TAG, "coordERAggio"+coordERAggio);
                    stringaSplittata = coordERAggio.split("\\s+");
                    if(data_class[0] != ""){ //se c'è almeno un data class di sensquare...
                        ottieniUltimaMisurazione("http://sensquare.disi.unibo.it/api/v1/streams/nearby/"+stringaSplittata[0]+"/"+stringaSplittata[1]+"/"+stringaSplittata[2]+"/"+data_class[0]+"/");

                    }


                }
            }, 6000);
        }
    }
    public class AsyncTaskAggiornaStatoIstanza extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoCustomServiceLocal().aggiornaStatoIstanza("Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza aggiornato ad Attiva");

            //da qui parte il calcolo di tutta l'istanza ecc ecc ...
            new AsyncTaskGetIstanzaFiltrata().execute();



        }
    }

    CustomServiceLocal istanza ;
    public class AsyncTaskGetStatoIstanza2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            istanza = appDatabase.daoCustomServiceLocal().getIstanzaFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza recuperato");

            if(istanza.getStatoIstanza().equals("Attiva")){
                new AsyncTaskGetIstanzaFiltrata().execute();
            }else{
                Log.d(TAG, " timer.scheduleAtFixedRate STOPPATO perchè l'istanza è stata disattivata");
                timer.cancel();
            }

        }
    }




}

