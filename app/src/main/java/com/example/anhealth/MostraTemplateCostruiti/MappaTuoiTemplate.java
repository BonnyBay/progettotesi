package com.example.anhealth.MostraTemplateCostruiti;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.MisurazioniSalute;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTuoiValori;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.MappaTuaIstanza;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.anhealth.R;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MappaTuoiTemplate extends FragmentActivity implements OnMapReadyCallback,  GoogleMap.OnMapLongClickListener {

    private GoogleMap mMap;
    private GeofencingClient geofencingClient;


    private static final String TAG = "MapsActivity";
    private String GEOFENCE_ID = "SOME_GEOFENCE_ID";
    private int FINE_LOCATION_ACCESS_REQUEST_CODE = 10001;
    private int BACKGROUND_LOCATION_ACCESS_REQUEST_CODE = 10002;

    private double latitudineInstanza = 44.49;
    private double longitudineInstanza = 11.34;
    private double raggioInstanza = 1000;
    LatLng locationInstanza;

    private double latitudineCliccata = 0.0;
    private double longitudineCliccata = 0.0;

    private Button aumentaRaggioBtn, instanziaTemplateBtn;

    private AppDatabase appDatabase;
    private EditText noteTuoTemplate, titoloInstanza;

    private String url = "";
    private String data_class_trattati = ""; //dataclass di sensquare
    private String getData_class_trattati_localmente = "";

    private int eseguiUnAltroCiclo = 0; //indice che viene incrementato nell onPost dell asycntask
    private  String[] data_class;
    private int indiceDataClassSuccessivo = 0;

    private  LatLng locationDataStream;

    private Marker myMarker;
    private ArrayList<Marker> listaDeiMarkerAggiunti = new ArrayList<Marker>();
    private ArrayList<Integer> listaIdDataStreamDaUsare = new ArrayList<Integer>();
    private String [] parLocali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mappa_tuoi_template);

        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        aumentaRaggioBtn = findViewById(R.id.aumentaRaggioIdTuoTemplate);
        instanziaTemplateBtn = findViewById(R.id.instanziaTuoTemplate);

        noteTuoTemplate = findViewById(R.id.noteTuoTemplate);
        titoloInstanza = findViewById(R.id.titoloInstanzaId);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mappaTuoiTemplate);
        mapFragment.getMapAsync(this);

        geofencingClient = LocationServices.getGeofencingClient(this);


        final String templateId = getIntent().getStringExtra("temp_id");
        final String strutturaTemplate = getIntent().getStringExtra("strutturaTemplate");//recupero la struttura del template


        data_class_trattati = getIntent().getStringExtra("lista_data_class");//recupero i vari data_class trattati dal template
        getData_class_trattati_localmente = getIntent().getStringExtra("lista_data_class_locale");

        locationInstanza = new LatLng(latitudineInstanza, longitudineInstanza);

        aumentaRaggioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                costruisciAlertInserimentoRaggio(locationInstanza);
            }
        });

        instanziaTemplateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 *  Sezione in cui controllo che ci siano parametri locali se sono stati inseriti data class locali all'interno del
                 *  template, quali per esempio battito ecc ecc ...
                 *
                 * */
                parLocali = getData_class_trattati_localmente.split(",");
                if(!parLocali[0].equals("")){
                    Log.d(TAG, "Nel template sono stati usati parametri locali");

                    new AsyncTaskGetMisurazioniSalute().execute(parLocali[0]);

                }

                //converto l'arraylist, contentente i data stream sulla mappa, in una stringa
                String idStreams = "";
                if(!listaIdDataStreamDaUsare.isEmpty()){ // se non è vuoto, quindi: se ci sono data stream sulla mappa
                    for(int i = 0; i<listaIdDataStreamDaUsare.size(); i++){
                        idStreams = idStreams+listaIdDataStreamDaUsare.get(i)+" "; // alla fine conterrà tutti gli id necessari dei data stream dal quale recuperere le misurazioni
                    }
                }
                Log.d(TAG, "STREAMS DA PASSARE: "+idStreams);
                //if idDataStreamScelto != 0 &&
                String [] numStreamOttenuti = idStreams.split(" ");
                //!idStreams.equals("") modificato con:
                if( !numStreamOttenuti[0].equals("")){
                    Log.d(TAG, "Stream ottenuti"+numStreamOttenuti[0]+".");
                    Log.d(TAG, "data class .length "+data_class.length);;
                    if( numStreamOttenuti.length == data_class.length){ //se cosi, vuol dire che è stato settato e SOLO se è diverso da zero (e quindi uguale alla lunghezza di data class) si  può procedere con l'istanziazione
                        //il campo titolo non può essere vuoto e nemmeno la descrizione
                        if(latitudineCliccata != 0.0 && longitudineCliccata != 0.0 && noteTuoTemplate.getText().length() != 0 && titoloInstanza.getText().length() != 0){ //in questo caso vuol dire che ho posizionato l'area circolare da qualhe parte
                            //devo creare una nuova instanza e di conseguenza riempio il database delle instanza
                            CustomServiceLocal instanzaCreata = new CustomServiceLocal();
                            instanzaCreata.setDeployment_radius(raggioInstanza);
                            instanzaCreata.setGps_latitude(latitudineCliccata);
                            instanzaCreata.setGps_longitude(longitudineCliccata);
                            instanzaCreata.setDescrizione(noteTuoTemplate.getText().toString());
                            instanzaCreata.setTitolo(titoloInstanza.getText().toString());
                            instanzaCreata.setArgs(templateId);
                            instanzaCreata.setStruttura_template(strutturaTemplate);
                            instanzaCreata.setStatoIstanza("Non Attiva");
                            Log.d(TAG, "STREAMS PASSATI: "+idStreams);
                            instanzaCreata.setId_data_stream_utili(idStreams);
                            String parametri = "";
                            for(int i = 0; i<data_class.length; i++){
                                parametri = parametri + data_class[i]+ " ";
                            }
                            instanzaCreata.setNomiParametri(parametri);


                            //per prima cosa controllo che ci siano misurazioni locali inserite
                            Log.d(TAG, "parLocali.length: "+parLocali.length);
                            Log.d(TAG, "contaParLocali.size() "+contaParLocali.size());

                            boolean parContenuto = false;
                            if(!parLocali[0].equals("")){ //se il template tratta parametri locali...
                                for(int j = 0; j<parLocali.length; j++){//controllo se ogni parametro LOCALE trattato dal template, è presente del db
                                    if(contaParLocali.contains(parLocali[j])){
                                        parContenuto = true;
                                    }else{
                                        parContenuto = false;
                                        break;
                                    }
                                }
                                if(parContenuto){//se il controllo precedente è passato, posso istanziare
                                    Log.d(TAG, "Par trattati localmente salvati nel db: "+getData_class_trattati_localmente);
                                    instanzaCreata.setNomiParametriLocali(getData_class_trattati_localmente);
                                    new AsyncTaskInserisciInstanza().execute(instanzaCreata); //aggiungo l'istanzaCreata
                                    //riazzero
                                    latitudineCliccata = 0.0;
                                    longitudineCliccata = 0.0;
                                }

                            }else{//se non tratta parametri locali, posso istanziare
                                instanzaCreata.setNomiParametriLocali("");//se ricado in questo else vuol dire che non ci sono parametri locali
                                new AsyncTaskInserisciInstanza().execute(instanzaCreata);
                            }

                            /*if(!parLocali[0].equals("") && parLocali.length >= contaParLocali.size()){
                                new AsyncTaskInserisciInstanza().execute(instanzaCreata); //aggiungo l'istanzaCreata
                            }
                            if(parLocali[0].equals("")){
                                new AsyncTaskInserisciInstanza().execute(instanzaCreata);
                            }*/
                            //new AsyncTaskInserisciInstanza().execute(instanzaCreata);

                        }else{
                            Toast.makeText(getApplicationContext(), "Devi riempire tutti i campi e posizionare l'instanza tenendo premuto in un punto sulla mappa ",
                                    Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Assenza di uno o più data stream nella zona, utili ad istanziare il template ",
                                Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Assenza di uno o più data stream nella zona, utili ad istanziare il template ",
                            Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng bologna = new LatLng(44.49, 11.34);
        //LatLng bologna = new LatLng(latitudineInstanza, longitudineInstanza);
        mMap.addMarker(new MarkerOptions().position(bologna).title("Marker di Bologna"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bologna, 14));

        enableUserLocation();

        mMap.setOnMapLongClickListener(this);
        //mMap.setOnMarkerClickListener(this);

    }

    private void enableUserLocation(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            mMap.setMyLocationEnabled(true);
        }else{
            //chiedo i permessi
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                //mostro il dialog dove spiego perchè ho bisogno dei permessi e poi li chiedo all'utente
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);
            }else{
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == FINE_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                mMap.setMyLocationEnabled(true);

            }else{
                //non ho i permessi
            }
        }

        //permesso di BACKGROUND_LOCATION_ACCESS_REQUEST_CODE
        if(requestCode == BACKGROUND_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                Toast.makeText(this, "Puoi aggiungere geofences", Toast.LENGTH_SHORT).show();

            }else{
                //non ho i permessi
                Toast.makeText(this, "Servono i permessi di background location access!!!!", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private void addMarker(LatLng latLng){
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.addMarker(markerOptions);
    }
    private void addCircle(LatLng latLng, double radius){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(radius);
        circleOptions.strokeColor(Color.argb(255, 0, 128, 0));
        circleOptions.fillColor(Color.argb(64, 0, 128, 0));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);
    }

    public void costruisciAlertInserimentoRaggio(final LatLng latLng){
        AlertDialog.Builder dialogRaggio = new AlertDialog.Builder(MappaTuoiTemplate.this);
        dialogRaggio.setTitle("Inserisci il raggio");

        final EditText raggioInseritoEdit = new EditText(MappaTuoiTemplate.this);
        raggioInseritoEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialogRaggio.setView(raggioInseritoEdit);

        dialogRaggio.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String raggioInseritoStringa = raggioInseritoEdit.getText().toString();
                raggioInstanza = Integer.parseInt(raggioInseritoStringa);
                onResume();
                mMap.clear(); //tolgo i marker precedenti, COSI POSIZIONO UN SOLO MARKER SULLA MAPPA

            }
        });
        dialogRaggio.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialogRaggio.show();
    }


    public class AsyncTaskInserisciInstanza extends AsyncTask<CustomServiceLocal, Void, Void> {
        @Override
        protected Void doInBackground(CustomServiceLocal... instanzas) {
            //istanzas[0] rappresenta l'istanza passata come parametro all'asynctask
            appDatabase.daoCustomServiceLocal().aggiungiInstanza(instanzas[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Istanza aggiunta ",
                    Toast.LENGTH_SHORT).show();
            //rimuovo gli oggetti presenti sulla mappa
            mMap.clear();
            //svuoto i campi titolo e descrizione
            noteTuoTemplate.setText("");
            titoloInstanza.setText("");

        }
    }

    private void handleMapLongClick(LatLng latLng){ //richiamato dal metodo onMApLongClick
        mMap.clear(); //tolgo i marker/cerchi precedenti cosi posiziono un solo marker sulla mappa ed un solo cerchio
        addMarker(latLng);
        addCircle(latLng, raggioInstanza);
    }

    int dataStreamCorretto = 0;
    ArrayList<String> data_class_list = new ArrayList<String>();
    @Override
    public void onMapLongClick(LatLng latLng) {
        handleMapLongClick(latLng);
        Toast.makeText(this, "Hai cliccato sulle seguenti coordinate: "+latLng.latitude +" "+latLng.longitude, Toast.LENGTH_SHORT).show();
        latitudineCliccata = latLng.latitude;
        longitudineCliccata = latLng.longitude;
        // ------------------------------------------------------------------------------
        // ------------------------------------------------------------------------------
        //mostro i data stream presenti in questa zona, se ce ne sono
        // per prima cosa splitto la stringa che ho ricevuto con il getExtra e cerco di capire quanti dataclass ci sono
        data_class = data_class_trattati.split(",");

        for(int i = 0; i<data_class.length; i++){
            //il controllo che è commentato permette di non avere data class doppi, ma a me serve che siano ripetuti
            if(!data_class_list.contains(data_class[i])){
                data_class_list.add(data_class[i]);
            }
        }
        Log.d(TAG, "data_class_list: "+data_class_list.size());
        Log.d(TAG, "data_class: "+data_class.length);



        url = "http://sensquare.disi.unibo.it/api/v1/streams/nearby/"+latitudineCliccata+"/"+longitudineCliccata+"/"+raggioInstanza+"/"+data_class[0]+"/";

        Log.d(TAG, "PARAMETRO "+" "+eseguiUnAltroCiclo+" "+data_class[0]);
        //PER OGNI DATA CLASS TRATTATA DA QUESTO TEMPLATE DEVO FARE UNA CHIAMATA API, per poi mostrare il data stream sulla mappa
        //ma prima devo svuotare il contenuto degli array list qualora siano già stati utilizzati
        if(!listaDeiMarkerAggiunti.isEmpty()){ // se non è vuoto
            listaDeiMarkerAggiunti.clear();
        }
        if(!vettoreIdDataStream.isEmpty()){
            vettoreIdDataStream.clear();
        }
        if(!listaIdDataStreamDaUsare.isEmpty()){
            listaIdDataStreamDaUsare.clear();
        }


        indiceDataClassSuccessivo = 0;
        dataStreamCorretto = 0;

        //new AsyncTaskGetAPIMisurazioni().execute();
        mostraUlterioriPagine(url);

    }

    ArrayList<Integer> vettoreIdDataStream = new ArrayList<Integer>();

    ArrayList<Colonnine> vettoreOggettiMiei = new ArrayList<Colonnine>();

    public void mostraUlterioriPagine(String urlPaginaDaMostrare){
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = urlPaginaDaMostrare; //DA CICLARE IN UN FOR per fare tante chiamate quante sono le pagine che contengono i dati richiesti

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject results = jsonArray.getJSONObject(i);

                                int id_data_stream = results.getInt("id"); // ID DEL DATA STREAM
                                Log.d(TAG, "ID DATA STREAM PRESENTI DENTRO L'AREA : "+id_data_stream);
                                Log.d(TAG, "PARAMETRI ASSOCIATI A QUESTO DATA STREAM: "+results.getString("data_class"));

                                //IMPORTANTE PER OTTENERE IL VALORE DELLA MISURAZIONE ... HO UN OGGETTO DENTRO UN ALTRO OGGETTO, ma cosi facendo otterrei solo un valore ...
                                JSONObject measurement = results.getJSONObject("measurement");

                                double lat_gps = measurement.getDouble("gps_latitude");
                                double long_gps = measurement.getDouble("gps_longitude");
                                locationDataStream = new LatLng(lat_gps, long_gps);
                                myMarker = mMap.addMarker(new MarkerOptions()
                                        .position(locationDataStream)
                                        .title(results.getString("data_class"))
                                        //.snippet("Latitudine: "+lat_gps+"\nLongitudine: "+long_gps)
                                        .snippet("("+lat_gps+", "+long_gps+")")

                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))); //.setDraggable(true);//.visible(false));//

                                listaDeiMarkerAggiunti.add(myMarker);

                                vettoreIdDataStream.add(id_data_stream);

                                Colonnine mioOggetto = new Colonnine(results.getString("data_class"), id_data_stream);
                                vettoreOggettiMiei.add(mioOggetto);
                            }


                            indiceDataClassSuccessivo ++;
                            if(indiceDataClassSuccessivo<data_class.length) {
                                Log.d(TAG, "data class passata all url " + data_class[indiceDataClassSuccessivo]);
                                String urlNuovo = "http://sensquare.disi.unibo.it/api/v1/streams/nearby/" + latitudineCliccata + "/" + longitudineCliccata + "/" + raggioInstanza + "/" + data_class[indiceDataClassSuccessivo] + "/";
                                //può, quindi, essere richiamato piu volte
                                mostraUlterioriPagine(urlNuovo);
                            }else{
                                for(int i = 0; i<data_class.length; i++){
                                    Log.d(TAG, "listaDeiMarkerAggiunti: "+listaDeiMarkerAggiunti.get(i).getTitle());
                                    for(int j = 0; j<vettoreOggettiMiei.size(); j++){
                                        Log.d(TAG, "data_class: "+data_class[i]);

                                        if(vettoreOggettiMiei.get(j).getTitolo().equals(data_class[i]) ){//data_class.length

                                            listaIdDataStreamDaUsare.add(vettoreOggettiMiei.get(j).getId_data_stream());
                                            break;

                                        }
                                    }
                                    if(listaIdDataStreamDaUsare.size() ==data_class.length){
                                        break;
                                    }
                                }
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());
                    }
                }
        );
        queue.add(objectRequest);
    }

    private ArrayList<String> contaParLocali = new ArrayList<String>(); //vettore che serve per verifica che i par locali inseriti abbiano almeno una misurazione, il size finale sarà al massimo di 4
    private List<MisurazioniSalute> misurazioniSalute = new ArrayList<MisurazioniSalute>();
    private int contaMis = 0;
    public class AsyncTaskGetMisurazioniSalute extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... stringsPar) {
            Log.d(TAG, "stringsPar[0]: "+stringsPar[0]);
            misurazioniSalute = appDatabase.daoMisurazioniSalute().getMisurazioniSaluteFiltrate(stringsPar[0]);
            //misurazioniSalute = appDatabase.daoMisurazioniSalute().getMeasurements();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "Misurazioni salute ottenute",
            //Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Misurazioni salute ottenute");
            //Log.d(TAG, "Es mis ottenuta: "+misurazioniSalute.get(0).getData_class());

            for(MisurazioniSalute misurazioneOttenuta : misurazioniSalute){
                Log.d(TAG, "misurazioneOttenuta id: "+misurazioneOttenuta.getId());
                Log.d(TAG, "misurazioneOttenuta class: "+misurazioneOttenuta.getData_class());
                Log.d(TAG, "misurazioneOttenuta valore: "+misurazioneOttenuta.getValore());




                Log.d(TAG, "  misurazioneOttenuta.getData_class(): "+  misurazioneOttenuta.getData_class());
                //se il template contiene BATT allora deve esserci almeno un report con BATT inserito

                if(misurazioneOttenuta.getData_class().equals("BATT")){
                    if(!contaParLocali.contains("BATT")){//se ancora non lo contiene lo aggiungo
                        contaParLocali.add("BATT");
                    }
                }
                if(misurazioneOttenuta.getData_class().equals("PMIN")) {
                    if(!contaParLocali.contains("PMIN")){
                        contaParLocali.add("PMIN");
                    }
                }
                if(misurazioneOttenuta.getData_class().equals("PMAX")) {
                    if(!contaParLocali.contains("PMAX")){
                        contaParLocali.add("PMAX");
                    }
                }
                if(misurazioneOttenuta.getData_class().equals("TMPC")) {
                    if(!contaParLocali.contains("TMPC")){
                        contaParLocali.add("TMPC");
                    }
                }

            }
            Log.d(TAG, "conta misurazioni salute = "+contaParLocali.size());

            contaMis++;
            if(contaMis<parLocali.length){
                new AsyncTaskGetMisurazioniSalute().execute(parLocali[contaMis]);
            }

        }
    }


}
