package com.example.anhealth.MostraTemplateCostruiti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTueInstanze;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class SchermataTemplateCostruiti extends AppCompatActivity {
    private static AppDatabase appDatabase;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerView2;

    private AdapterTuoiTemplate mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<CustomServiceTemplateLocal> templateLocale;
    private AdapterTueInstanze.OnNoteListener interfaceClick;
    private RecyclerItemClickListener mRecyclerItemClickListener;



    private ArrayList<String> arrayTitoli = new ArrayList<String>();
    private ArrayList<Integer> arrayID = new ArrayList<Integer>();
    private SharedPreferences prefs;

    RadioGroup radioGroup;
    RadioButton selectedRadioButton;
    Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_template_costruiti);
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        buttonSubmit = (Button) findViewById(R.id.btnSubmit);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);


        buttonSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectedRadioButton  = (RadioButton)findViewById(radioGroup.getCheckedRadioButtonId());
                String scelta = selectedRadioButton.getText().toString();
                if(scelta.equals("Template Locali")){
                    getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_tuoi_temp, new FragmentTemplateLocali()).commit();

                }else{
                    getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_tuoi_temp, new FragmentTemplateRemoti()).commit();

                }
            }
        });
    }





}
