package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.I_Tuoi_Template_Recycler.AdapterTemplateSenSquare;
import com.example.anhealth.InstanzeDeiTemplate.VediInstanzeSensquare;
import com.example.anhealth.MostraTemplateCostruiti.AdapterTuoiTemplate;
import com.example.anhealth.MostraTemplateCostruiti.MostraRegoleContenute;
import com.example.anhealth.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTueInstanze  extends RecyclerView.Adapter<AdapterTueInstanze.TueInstanzeHolder>{
    Context mContext;
    private List<CustomServiceLocal> mCustomServiceLocal;
    public ArrayList<String> mtitolo;
    public ArrayList<Integer> mId;
    boolean mostraRemoti = false;

    private OnNoteListener mOnNoteListener;
    public class TueInstanzeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView id, titolo;
        OnNoteListener mOnNoteListener;
        public TueInstanzeHolder(@NonNull View itemView,OnNoteListener onNoteListener,boolean mostraRemoti) {
            super(itemView);
            if(mostraRemoti){
                titolo = itemView.findViewById(R.id.idTemplateTuoiText4);
            }else{
                titolo = itemView.findViewById(R.id.idTemplateTuoiText3);
            }

            mOnNoteListener = onNoteListener;
        }

        @Override
        public void onClick(View v) {
            mOnNoteListener.onItemClick(getAdapterPosition());
        }
    }

    public AdapterTueInstanze(boolean mostraRemoti, ArrayList<String> titoloIstanza, ArrayList<Integer> id, Context context,  OnNoteListener mOnNoteListener) {
        this.mostraRemoti = mostraRemoti;
        mtitolo = titoloIstanza;
        mId = id;
        notifyDataSetChanged();
        this.mContext = context;
        this.mOnNoteListener = mOnNoteListener;
    }

    @NonNull
    @Override
    public AdapterTueInstanze.TueInstanzeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(mostraRemoti){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedi_instanze_sensquare, parent, false);
            AdapterTueInstanze.TueInstanzeHolder myHolder = new AdapterTueInstanze.TueInstanzeHolder(v, mOnNoteListener, true);
            return myHolder;
        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.riga_tue_instanze, parent, false);
            AdapterTueInstanze.TueInstanzeHolder myHolder = new AdapterTueInstanze.TueInstanzeHolder(v, mOnNoteListener, false);
            return myHolder;
        }


       // View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.riga_tue_instanze, parent, false);
        //AdapterTueInstanze.TueInstanzeHolder myHolder = new AdapterTueInstanze.TueInstanzeHolder(v, mOnNoteListener);
        //return myHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterTueInstanze.TueInstanzeHolder holder, final int position) {
        //holder.titolo.setText(String.valueOf(mCustomServiceLocal.get(position).getTitolo()));
        String name = mtitolo.get(position);
        holder.titolo.setText(name);
    }

    @Override
    public int getItemCount() {
        return mtitolo.size();
    }


    public interface OnNoteListener{
        void onItemClick(int position);
    }
}
