package com.example.anhealth.MostraTemplateCostruiti;


//classe utile a raggruppare il titolo dei data class con i loro rispettivi data stream id (sulla base di quelli presenti sulla mappa)
public class Colonnine {
    String titolo = ""; //nome parametro
    int id_data_stream = 0; //conterrà tutti gli id delle colonnine presenti nell'area associate a quel determinato parametro
    public Colonnine(String titolo, int id_data_stream){
        this.titolo = titolo;
        this.id_data_stream = id_data_stream;
    }

    public int getId_data_stream() {
        return id_data_stream;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setId_data_stream(int id_data_stream) {
        this.id_data_stream = id_data_stream;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }
}
