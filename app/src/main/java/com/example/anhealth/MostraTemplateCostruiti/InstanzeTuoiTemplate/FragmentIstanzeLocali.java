package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.InstanzeDeiTemplate.AdapterInstanzeSenSquare;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class FragmentIstanzeLocali extends Fragment implements RecyclerView.OnItemTouchListener{
    private View view;

    private static AppDatabase appDatabase;
    private RecyclerView mRecyclerView;
    private AdapterTueInstanze mAdapter;
    //private RecyclerView.LayoutManager mLayoutManager;
    private List<CustomServiceLocal> instanzaLocale;
    private ArrayList<Integer> arrayID = new ArrayList<Integer>();
    private AdapterTueInstanze.OnNoteListener interfaceClick;
    Context context;
    private SharedPreferences prefs;
    private RecyclerItemClickListener mRecyclerItemClickListener;

    public FragmentIstanzeLocali() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_istanze_locali, container, false);
        mRecyclerView = view.findViewById(R.id.fragment_rec_vedi_tue_instanze);

        context = getActivity();
        appDatabase = Room.databaseBuilder(getActivity().getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        //new AsyncTaskGetTueInstanze().execute();

        new AsyncTaskGetTueInstanze().execute();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        //costruisciRecyclerView();
    }

    public void costruisciRecyclerView() {

        mRecyclerView.setHasFixedSize(true);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        instanzaLocale = appDatabase.daoCustomServiceLocal().getInstanza();

        //mLayoutManager = new LinearLayoutManager(getActivity());
        //mRecyclerView.setLayoutManager(mLayoutManager);
        //new AsyncTaskGetTueInstanze().execute();

        //recupero titoli e id delle istanze e le metto in un vettore per poi caricarle nella recycler view tutte insieme
        for(CustomServiceLocal istanzaRecuperata : instanzaLocale ){
            arrayTitoli.add(istanzaRecuperata.getTitolo());
            arrayID.add(istanzaRecuperata.getId());
        }
        mAdapter = new AdapterTueInstanze(false, arrayTitoli, arrayIDIstanze, getActivity(), interfaceClick);

    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    ArrayList<String> arrayTitoli = new ArrayList<String>();
    ArrayList<Integer> arrayIDIstanze = new ArrayList<Integer>();


    public class AsyncTaskGetTueInstanze extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            costruisciRecyclerView();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //AdapterInstanzeSenSquare adapter = new AdapterInstanzeSenSquare(arrayTitoli, arrayID, getActivity(), interfaceClick); //passo i parametri al costruttore per poi mostrarli

            mRecyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getActivity(), mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {
                            Toast.makeText(getActivity(), "instanzaLocale.getpos ID "+instanzaLocale.get(position).getId(),
                                    Toast.LENGTH_LONG).show();
                            //Intent intent = new Intent(SchermataTueInstanze.this, VediInstanzeDettagli.class);
                            //startActivity(intent);
                            prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            final long instanzaScelta = instanzaLocale.get(position).getId();

                            editor.putLong("idInstanzaSceltaTua", instanzaScelta); //cosi l'activity fara una request api con quell id preciso
                            Log.d("testIstanza", "ISTANZA CLICCATA "+instanzaScelta);
                            editor.apply();

                            String coordinateERaggio = instanzaLocale.get(position).getGps_latitude()+" "+instanzaLocale.get(position).getGps_longitude()+" "+instanzaLocale.get(position).getDeployment_radius()+" ";
                            String descDaPassare = instanzaLocale.get(position).getDescrizione();
                            String titoloDaPAssare = instanzaLocale.get(position).getTitolo();
                            int idDaPassare = instanzaLocale.get(position).getId();
                            String strutturaTemplate = instanzaLocale.get(position).getStruttura_template();
                            Intent intent = new Intent(getActivity(), VediInstanzeDettagli.class);
                            intent.putExtra("coordERagg", coordinateERaggio);
                            intent.putExtra("descr", descDaPassare);
                            intent.putExtra("tit", titoloDaPAssare);
                            intent.putExtra("idInstTua", idDaPassare);
                            intent.putExtra("struttInst", strutturaTemplate);
                            getActivity().startActivity(intent);
                        }

                        @Override public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    })
            );
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
