package com.example.anhealth.MostraTemplateCostruiti;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.AggiornaReport;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.MainActivity;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTueInstanze;
import com.example.anhealth.MyAdapter;
import com.example.anhealth.R;
import com.example.anhealth.RecVediRep;

import java.util.List;

public class AdapterTuoiTemplate { //  extends RecyclerView.Adapter<AdapterTuoiTemplate.TuoiTemplateHolder>
    /*Context context;
    private List<CustomServiceTemplateLocal> mCustomServiceTemplateLocal;
    private SharedPreferences prefs;
    private AdapterTueInstanze.OnNoteListener mOnNoteListener;

    public class TuoiTemplateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView  id, titolo;
        AdapterTueInstanze.OnNoteListener mOnNoteListener;

        public TuoiTemplateHolder(@NonNull View itemView, AdapterTueInstanze.OnNoteListener onNoteListener) {
            super(itemView);
            titolo = itemView.findViewById(R.id.idTemplateTuoiText2);
            prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
            mOnNoteListener = onNoteListener;
        }

        @Override
        public void onClick(View v) {
            mOnNoteListener.onItemClick(getAdapterPosition());
        }
    }

    public AdapterTuoiTemplate(List<CustomServiceTemplateLocal> template, Context context, AdapterTueInstanze.OnNoteListener mOnNoteListener) {
        mCustomServiceTemplateLocal = template;
        notifyDataSetChanged();
        this.context = context;
        this.mOnNoteListener = mOnNoteListener;
    }

    @NonNull
    @Override
    public AdapterTuoiTemplate.TuoiTemplateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedi_tuoi_template, parent, false);

        AdapterTuoiTemplate.TuoiTemplateHolder myHolder = new AdapterTuoiTemplate.TuoiTemplateHolder(v, mOnNoteListener);

        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTuoiTemplate.TuoiTemplateHolder holder, int position) {
        //mostro i dati all'interno di ogni riga
        //holder.id.setText(String.valueOf(mCustomServiceTemplateLocal.get(position).getId()));
        holder.titolo.setText(String.valueOf(mCustomServiceTemplateLocal.get(position).getTitolo()));
    }

    @Override
    public int getItemCount() {
        return mCustomServiceTemplateLocal.size();
    }

    public interface OnNoteListener{
        void onItemClick(int position);
    }*/

}
