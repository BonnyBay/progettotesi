package com.example.anhealth.MostraTemplateCostruiti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.Database.Regola;
import com.example.anhealth.InstanzeDeiTemplate.VediInstanzeSensquare;
import com.example.anhealth.MainActivity;
import com.example.anhealth.R;

import java.util.List;

public class MostraRegoleContenute extends AppCompatActivity {
    private static final String TAG = "MostraRegoleContenute";
    private TextView id, descrizione, parametriTrattati, operazioni, costanti, titolo;
    private List<Regola> mRegola;
    private AppDatabase appDatabase;
    private SharedPreferences prefs;
    boolean unaRegolaInserita = false; //utile a capire se la prima regola in assoluto è stata stampata
    private Button instanziaTuoTemplateBtn;
    private String strutturaTemplate = "";
    private String idTempScelto = "";

    private Context mContext;
    private String lista_data_class = ""; //stringa che conterrà tutti i data class trattati da quel template
    private String lista_data_class_locale = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostra_regole_contenute);
        SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        mContext = this;

        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        id = findViewById(R.id.idTemplTuo);
        titolo = findViewById(R.id.titTempTuo);
        descrizione = findViewById(R.id.idDescrip);
        parametriTrattati = findViewById(R.id.argsTuoTemp);
        operazioni = findViewById(R.id.idOptTuoTemplate);
        //costanti = findViewById(R.id.idCostanti);
        instanziaTuoTemplateBtn = findViewById(R.id.instanziaTemplateIdTuoTemp);
        new AsyncTaskGetRegole().execute();

    }

    public void mostraRegole(){
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        //mostro solo le regole che hanno l'id del template cliccato
        long idTemplateScelto = prefs.getLong("idTemplateScleto", 0);
        idTempCorretto = idTemplateScelto+"";
        String nomeTempl = prefs.getString("nomeTempl","");
        long idRegolaPeecedente = 0; //serve per evitare che le regole vengano stampate piu volte
        long idTemp = 0;
        for( int i = 0; i<mRegola.size(); i++){
            long idRegola = mRegola.get(i).getId();
            idTemp = mRegola.get(i).getTemplate_id();
            //Log.d(TAG, "mostraRegole: "+idTemp);
            editor.putLong("templateIdPassato",idTemp);

            if(idTemp == idTemplateScelto){ //occhio a non confondersi: la tabella regola ha un id SUO ed un ID che fa riferimento al template
                if(idRegolaPeecedente != idRegola){ //vuol dire che questa regola non è ancora stata stampata, se la condizione è vera: stampo
                    id.setText(Long.toString(idRegola));
                    titolo.setText(nomeTempl);
                    String param = mRegola.get(i).getParametro();
                    parametriTrattati.append(param+", ");
                    idRegolaPeecedente = idRegola; //me la salvo per confrontarla nella prossima iterazione e vedere se è già stata stampata

                    String operatori = mRegola.get(i).getOperatorePrimo();
                    String operatoriCentrale = mRegola.get(i).getOperatoreSecondo(); //operatori tra le regole, qualora ce ne sia più di una

                    double costante= mRegola.get(i).getCostante();
                    if(unaRegolaInserita == false){
                        operazioni.append(param+" "+operatori+" "+costante+" ");
                        //aggiungo il parametro alla lista dei data class
                        //per PARAM intendo DATA_CLASS
                        if(param.equals("BATT") || param.equals("PMIN") || param.equals("PMAX") || param.equals("TMPC")){
                            lista_data_class_locale = param + ",";
                            unaRegolaInserita = true;
                        }else{ //vuol dire che è un parametro di sensquare
                            lista_data_class = param+",";
                            unaRegolaInserita = true;
                        }
                    }else{
                        if(param.equals("BATT") || param.equals("PMIN") || param.equals("PMAX") || param.equals("TMPC")){
                            operazioni.append(operatoriCentrale+" "+param+" "+operatori+" "+costante+" ");
                            lista_data_class_locale = lista_data_class_locale + param + ",";
                        }else{ //vuol dire che è un parametro di sensquare
                            operazioni.append(operatoriCentrale+" "+param+" "+operatori+" "+costante+" ");
                            lista_data_class = lista_data_class + param+",";
                        }

                    }

                }

            }

        }
        Log.d("LISTA_DATA_CLASS", "Lista data class trattati:  "+lista_data_class); //contiene le data class trattati, anche ripetuti!
        strutturaTemplate = operazioni.getText().toString(); //mi salvo la struttura del template in una stringa, che poi passerò alla Mappa. La stringa verrà splittata ed utilizzata
        idTempScelto = idTemp+"";
        Log.d(TAG, "PASSERò QUESTO TEMPLATE ID: "+idTempScelto);
    }
    String idTempCorretto = "";

    public class AsyncTaskGetRegole extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            mRegola = appDatabase.daoRegole().getRegole();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "regole caricate",
                    Toast.LENGTH_SHORT).show();
            mostraRegole();

            instanziaTuoTemplateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //AL CLICK, mi salvo i valori da passare alla mappa in modo tale che lei possa recuperare le misurazioni ed eseguire i calcoli
                    Intent intent = new Intent(mContext, MappaTuoiTemplate.class);
                    intent.putExtra("temp_id", idTempCorretto);
                    intent.putExtra("strutturaTemplate", strutturaTemplate);
                    /**
                     *   ------------------ STRATEGIA IMPLEMENTATIVA ------------------
                     *   Ho due tipi diversi di data class:
                     *   uno è utile per i parametri di sensquare, l'altro è utile per i parametri monitorati
                     *   dall'utente.
                     *
                     * */
                    intent.putExtra("lista_data_class", lista_data_class);
                    intent.putExtra("lista_data_class_locale", lista_data_class_locale);
                    mContext.startActivity(intent);

                }
            });

        }
    }

}
