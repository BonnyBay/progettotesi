package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.InstanzeDeiTemplate.MapsActivity;
import com.example.anhealth.MainActivity;
import com.example.anhealth.R;
import com.google.android.gms.common.api.PendingResult;

import java.util.ArrayList;
import java.util.List;

public class VediInstanzeDettagli extends AppCompatActivity {
    private TextView titoloText, id, descrizioneText, parametriTrattati, latitudineText, longitudineText, raggioText, strutInstanzaText, statoIstanza;
    private Button instanziaTemplateBtn;
    private SharedPreferences prefs;
    private Animation scale_up, scale_down;
    int idTuaInstanza = 0;
    private String TAG = "VediInstanzeDettagli";
    AppDatabase appDatabase;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vedi_instanze_dettagli);
        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "datiReportDB").build();


        titoloText = findViewById(R.id.titInstTua);
        id = findViewById(R.id.idInstTua);
        descrizioneText = findViewById(R.id.idDescripTemTua);
        parametriTrattati = findViewById(R.id.argsTuaInstanza);

        instanziaTemplateBtn = findViewById(R.id.attivaInstanzaTua);

        latitudineText = findViewById(R.id.idLatTuaInstanza);
        longitudineText = findViewById(R.id.idLongTuaInstanza);
        raggioText = findViewById(R.id.idRaggioTuaInstanza);
        strutInstanzaText = findViewById(R.id.strutturaText);

        statoIstanza = findViewById(R.id.statoIstanzaTua);

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        long idTuaInstanzaSceltaDaVisualizzare = prefs.getLong("idInstanzaSceltaTua", 1);

        String coordinateERaggio = getIntent().getStringExtra("coordERagg");
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("coordERaggio", coordinateERaggio);
        editor.apply();

        String[] stringaSplittata = coordinateERaggio.split("\\s+");
        final double latitudineInstanza = Double.parseDouble(stringaSplittata[0]); //NELLA POSIZIONE 0 C'è LA LATITUDINE
        final double longitudineInstanza = Double.parseDouble(stringaSplittata[1]); //NELLA POSIZIONE 1 C'è LA LONGITUDINE
        final double raggioInstanza = Double.parseDouble(stringaSplittata[2]); //NELLA POSIZIONE 2 C'è IL RAGGIO


        String descrizione = getIntent().getStringExtra("descr");
        String titolo = getIntent().getStringExtra("tit");
        idTuaInstanza = getIntent().getIntExtra("idInstTua",0);
        String strutturaInstanza = getIntent().getStringExtra("struttInst");

        latitudineText.append(String.valueOf(latitudineInstanza));
        longitudineText.append(String.valueOf(longitudineInstanza));
        raggioText.append(String.valueOf(raggioInstanza));
        descrizioneText.append(" "+descrizione);
        titoloText.setText(titolo);
        id.setText(Integer.toString(idTuaInstanza));
        strutInstanzaText.append(strutturaInstanza);
        Log.d("vediInstanze ", "id tua instanza "+idTuaInstanza);
        //al click sul pulsante apro la mappa, ma PRIMA devo parsarmi la stringa
        final Intent intent = new Intent(getApplicationContext(), MappaTuaIstanza.class);
        intent.putExtra("latitudineInstanza2", latitudineInstanza);
        intent.putExtra("longitudineInstanza2", longitudineInstanza);
        intent.putExtra("raggioInstanza2", raggioInstanza);
        intent.putExtra("idTuaInstanza2", idTuaInstanza);

        final NumberPicker numberPicker = findViewById(R.id.numberPicker);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(120);
        //numberPicker.setTextSize(80f);
        instanziaTemplateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if(istanza.getStatoIstanza().equals("Attiva")){
                        new AsyncTaskAggiornaStatoIstanzaANonAttiva().execute(istanza.getId());
                    }else{
                        new AsyncTaskAggiornaStatoAdAttiva().execute(istanza.getId());
                    }*/
                    SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    Log.d(TAG, "valore picker "+numberPicker.getValue());
                    editor.putInt("pickerVal", numberPicker.getValue());
                    editor.apply();
                    Log.d(TAG, "idTuaInstanza2: "+idTuaInstanza);
                    //apro la mappa e la prima cosa che dovrà fare sarà recuperare gli id dei data stream associati a questa istanza
                    startActivity(intent);

                }
        });


    }

    @Override
    protected void onStart() {
        new AsyncTaskGetStatoIstanza().execute();

        super.onStart();

    }

    CustomServiceLocal istanza ;
    public class AsyncTaskGetStatoIstanza extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            istanza = appDatabase.daoCustomServiceLocal().getIstanzaFiltrata(idTuaInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza recuperato");

            Log.d(TAG, "stato: "+istanza.getStatoIstanza());

            statoIstanza.setText(istanza.getStatoIstanza());
            if(istanza.getStatoIstanza().equals("Attiva")){
                instanziaTemplateBtn.setText("Visiona Istanza");
            }else{
                instanziaTemplateBtn.setText("Attiva Istanza");

            }



        }
    }
    public class AsyncTaskAggiornaStatoAdAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoCustomServiceLocal().aggiornaStatoIstanza("Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza aggiornato ad Attiva");



        }
    }


    public class AsyncTaskAggiornaStatoIstanzaANonAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoCustomServiceLocal().aggiornaStatoIstanza("Non Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            //instanziaTemplateBtn.setText("Attiva Istanza");

            Log.d(TAG, "Stato istanza aggiornato a Non Attiva");
            //PROVO A RI-AGGIORNARE L'ACTIVITY
            //onStart();


        }
    }

}
