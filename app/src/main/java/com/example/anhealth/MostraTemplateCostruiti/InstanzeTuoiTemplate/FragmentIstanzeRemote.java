package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.InstanzeDeiTemplate.AdapterInstanzeSenSquare;
import com.example.anhealth.InstanzeDeiTemplate.VediInstanzeSensquare;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FragmentIstanzeRemote extends Fragment implements AdapterView.OnItemSelectedListener{
    private View view;


    private RecyclerView mRecyclerView;
    private TextView titoloTemplate;

    private ArrayList<String> arrayTitoli = new ArrayList<String>();
    private ArrayList<Integer> arrayID = new ArrayList<Integer>();
    private ArrayList<String> arrayLatitudine = new ArrayList<String>();
    private ArrayList<String> arrayLongitudine = new ArrayList<String>();
    private ArrayList<String> arrayRaggio = new ArrayList<String>();
    private ArrayList<String> arrayDescrizione  = new ArrayList<String>();;
    private ArrayList<String> arrayIdPar = new ArrayList<String>();
    private ArrayList<String> arrayOp = new ArrayList<String>();
    private ArrayList<String> arrayDsid = new ArrayList<String>();
    private ArrayList<String> arrayTemplateId = new ArrayList<String>();

    //url delle pagine successive alla prima
    private String urlSecondaPagina = "";
    private SharedPreferences prefs;
    private String url;
    private AdapterTueInstanze.OnNoteListener interfaceClick;
    private RecyclerItemClickListener mRecyclerItemClickListener;
    public FragmentIstanzeRemote() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_istanze_remote, container, false);

        mRecyclerView = view.findViewById(R.id.recyclerInstanceSensquare);
        prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        url ="http://sensquare.disi.unibo.it/api/v1/showinst"; //DA CICLARE IN UN FOR per fare tante chiamate quante sono le pagine che contengono le instanze

        mostraUlterioriPagine(url);


        return view;
    }

    //metodo che mostra le instanze contenute in altre pagine
    public void mostraUlterioriPagine(String urlPaginaDaMostrare){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = urlPaginaDaMostrare; //DA CICLARE IN UN FOR per fare tante chiamate quante sono le pagine che contengono le instanze
        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            //JSONArray jsonArgs = response.getJSONArray("json_args");
                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject results = jsonArray.getJSONObject(i);

                                int idIstanza = results.getInt("id"); //l'id lo metterò nelle sharedPreferences e mi servirà per mostrare il tmepalte qualora l'utente ci clicchi sopra
                                String titoloInstString = results.getString("title");

                                String latitudine = results.getString("deployment_center_lat");
                                String longitudine = results.getString("deployment_center_lon");
                                String raggio = results.getString("deployment_radius");
                                String tempId = results.getString("template_id");

                                arrayID.add(idIstanza);
                                arrayTitoli.add(titoloInstString);
                                arrayLatitudine.add(latitudine);
                                arrayLongitudine.add(longitudine);
                                arrayRaggio.add(raggio);

                                String descrizione = results.getString("description");
                                arrayDescrizione.add(descrizione);

                                arrayTemplateId.add(tempId);


                                try{
                                    mRecyclerView.addOnItemTouchListener(
                                            new RecyclerItemClickListener(getActivity(), mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                                                @Override public void onItemClick(View view, int position) {
                                                    Toast.makeText(getActivity(), "instanzaLocale.getpos ID " + arrayID.get(position),
                                                            Toast.LENGTH_LONG).show();
                                                    prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = prefs.edit();
                                                    final int instanzaScelta = arrayID.get(position);
                                                    editor.putLong("idInstanzaScelta", instanzaScelta); //cosi l'activity fara una request api con quell id preciso
                                                    editor.apply();
                                                    //FORMATO DELLA STRINGA DA PASSARE ALLA PROSSIMA ACTIVITY:
                                                    //--------- LATITUDINE LONGITUDINE RAGGIO ---------
                                                    String coordinateERaggio = arrayLatitudine.get(position)+" "+arrayLongitudine.get(position)+" "+arrayRaggio.get(position)+" ";
                                                    String descDaPassare = arrayDescrizione.get(position);
                                                    String titoloDaPAssare = arrayTitoli.get(position);
                                                    int idDaPassare = arrayID.get(position);
                                                    String templId = arrayTemplateId.get(position);

                                                    Intent intent = new Intent(getActivity(), VediInstanzeSensquare.class);
                                                    intent.putExtra("coordinateERaggio", coordinateERaggio); //metto in un putExtra i valori da mostrare nella prossima activity
                                                    intent.putExtra("descrizione", descDaPassare);
                                                    intent.putExtra("titolo", titoloDaPAssare);
                                                    intent.putExtra("idInstanza", idDaPassare);
                                                    intent.putExtra("templateID", templId);
                                                    getActivity().startActivity(intent);

                                                }

                                                @Override public void onLongItemClick(View view, int position) {
                                                    // do whatever
                                                }
                                            })
                                    );
                                    setAdapter();
                                }catch (Exception e){
                                    Log.d("errore", "Errore nel caricamento dei dati");
                                }
                            }

                            //controllo che il campo next contenga un nuovo url, e se lo contiene lo salvo per eseguire ulteriori richieste API
                            String urlPaginaSuccessiva = response.getString("next");

                            if(!urlPaginaSuccessiva.equals(null)){ // se non è vuoto..
                                urlSecondaPagina = urlPaginaSuccessiva;
                                mostraUlterioriPagine(urlSecondaPagina); //qui l'url sarà diverso dal primo, infatti conterrà le indicazioni per la pagina successiva da caricare
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());

                    }
                }
        );
        queue.add(objectRequest);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public void setAdapter(){
        AdapterTueInstanze adapter = new AdapterTueInstanze(true, arrayTitoli, arrayID, getActivity(), interfaceClick); //passo i parametri al costruttore per poi mostrarli
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }




}
