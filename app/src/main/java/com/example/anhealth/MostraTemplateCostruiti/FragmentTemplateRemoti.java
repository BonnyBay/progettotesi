package com.example.anhealth.MostraTemplateCostruiti;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.I_Tuoi_Template_Recycler.AdapterTemplateSenSquare;
import com.example.anhealth.I_Tuoi_Template_Recycler.SchermataTemplateSenSquare;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTueInstanze;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FragmentTemplateRemoti extends Fragment  implements AdapterView.OnItemSelectedListener{
    private View view;
    private RecyclerView mRecyclerView;

    private ArrayList<String> arrayTitoli = new ArrayList<String>();
    private ArrayList<Integer> arrayID = new ArrayList<Integer>();
    private RecyclerItemClickListener mRecyclerItemClickListener;
    private AdapterTueInstanze.OnNoteListener interfaceClick;
    private SharedPreferences prefs;

    private String url = "";
    private String urlSecondaPagina = "";

    public FragmentTemplateRemoti() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_template_remoti, container, false);

        prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        mRecyclerView = view.findViewById(R.id.fragment_rec_vedi_temp_remoti);

        url = "http://sensquare.disi.unibo.it/api/v1/showtemp";
        mostraTemplateSensquare(url);

        return view;
    }

    public void costruisciRecyclerConTemplateSensquare(){
        //Log.d("fragTempRemoti", "costruisciRecyclerConTemplateSensquare: SONO QUI");
        //il booleano true indica che il template è remoto, non locale
        AdapterTemplateSenSquare adapter = new AdapterTemplateSenSquare(true, arrayTitoli, arrayID,getActivity(), interfaceClick); //true mostro template remoti
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }

    /**
     * Metodi che servono a recuperare i template da SenSquare
     *
     * */
    //metodo che mostra le instanze contenute in altre pagine
    public void mostraTemplateSensquare(String urlPaginaDaMostrare){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final String url = urlPaginaDaMostrare; //DA CICLARE IN UN FOR per fare tante chiamate quante sono le pagine che contengono le instanze

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject results = jsonArray.getJSONObject(i);

                                int idTemplate = results.getInt("id"); //l'id lo metterò nelle sharedPreferences e mi servirà per mostrare il tmepalte qualora l'utente ci clicchi sopra
                                String titoloTemString = results.getString("title");
                                arrayID.add(idTemplate);
                                arrayTitoli.add(titoloTemString);
                                //setAdapter();
                                    mRecyclerView.addOnItemTouchListener(
                                            new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                                @Override public void onItemClick(View view, int position) {
                                                    Toast.makeText(getActivity(), "instanzaLocale.getpos ID "+arrayID.get(position),
                                                            Toast.LENGTH_LONG).show();

                                                    prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                                                    final int idTemplate = arrayID.get(position);
                                                    SharedPreferences.Editor editor = prefs.edit();
                                                    Log.e("idInserito", idTemplate+"");
                                                    editor.putInt("idTemplateDaVisualizzare", idTemplate); //cosi l'activity fara una request api con quell id preciso
                                                    editor.apply();

                                                    Intent intent = new Intent(getActivity(), SchermataTemplateSenSquare.class);
                                                    getActivity().startActivity(intent);
                                                }
                                                @Override public void onLongItemClick(View view, int position) {
                                                    // do whatever
                                                }
                                            })
                                    );
                                    costruisciRecyclerConTemplateSensquare();
                            }
                            if(!response.getString("next").equals(null)){ // se non è vuoto..
                                String urlPaginaSuccessiva = response.getString("next");
                                Log.d("SchermataTemplateCost", "response.getString(\"next\") "+response.getString("next"));
                                urlSecondaPagina = urlPaginaSuccessiva;
                                mostraTemplateSensquare(urlSecondaPagina); //qui l'url sarà diverso dal primo, infatti conterrà le indicazioni per la pagina successiva da caricare
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());
                    }
                }
        );
        queue.add(objectRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
