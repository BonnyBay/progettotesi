package com.example.anhealth.MostraTemplateCostruiti;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.I_Tuoi_Template_Recycler.AdapterTemplateSenSquare;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTueInstanze;
import com.example.anhealth.R;
import com.example.anhealth.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class FragmentTemplateLocali extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static AppDatabase appDatabase;
    private RecyclerView mRecyclerView;

    private AdapterTemplateSenSquare mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<CustomServiceTemplateLocal> templateLocale;
    private AdapterTueInstanze.OnNoteListener interfaceClick;
    private RecyclerItemClickListener mRecyclerItemClickListener;

    private SharedPreferences prefs;

    private View view;

    public FragmentTemplateLocali() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        appDatabase = Room.databaseBuilder(getActivity(), AppDatabase.class, "datiReportDB").build();
        prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        view =  inflater.inflate(R.layout.fragment_template_locali, container, false);
        mRecyclerView = view.findViewById(R.id.fragment_rec_vedi_tuoi_template);

        new AsyncTaskGetTuoiTemplate().execute();

        return view;
    }

    ArrayList<String> arrayNomi = new ArrayList<String>();
    ArrayList<Integer> arrayID = new ArrayList<Integer>();

    public void costruisciRecyclerView() {
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        templateLocale = appDatabase.daoCustomServiceTemplateLocal().getTemplates();

        //mAdapter = new AdapterTuoiTemplate(templateLocale, getActivity(), interfaceClick);
        //mAdapter = new AdapterTuoiTemplate(true, getActivity(), interfaceClick);
        for(CustomServiceTemplateLocal tempLoc : templateLocale){
            arrayNomi.add(tempLoc.getTitolo());
            arrayID.add((int) tempLoc.getId());
        }

        mAdapter = new AdapterTemplateSenSquare(false, arrayNomi, arrayID, getActivity(), interfaceClick);

    }


    public class AsyncTaskGetTuoiTemplate extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            costruisciRecyclerView();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            mRecyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getActivity(), mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {
                            //Toast.makeText(getActivity(), "template Locale.getpos ID "+templateLocale.get(position).getId(),
                              //      Toast.LENGTH_LONG).show();
                            //Intent intent = new Intent(SchermataTueInstanze.this, VediInstanzeDettagli.class);
                            //startActivity(intent);
                            long iD = templateLocale.get(position).getId();
                            String nomeTemplate = templateLocale.get(position).getTitolo();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putLong("idTemplateScleto", iD); //metto l'id del template da visualizzare nelle shared preferences
                            Log.d("FragmentTempLoc", "idTemplateScleto: "+iD);
                            editor.putString("nomeTempl", nomeTemplate);
                            editor.apply();
                            Intent intent = new Intent(getActivity(), MostraRegoleContenute.class);
                            getActivity().startActivity(intent);
                        }

                        @Override public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    })
            );
            mRecyclerView.setAdapter(mAdapter);




        }
    }
}
