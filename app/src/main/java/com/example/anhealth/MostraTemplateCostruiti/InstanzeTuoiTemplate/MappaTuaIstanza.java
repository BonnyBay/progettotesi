package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.List;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.Database.ValoriIstanze;
import com.example.anhealth.MontaTemplate.SchermataCostruzione;
import com.example.anhealth.NoticationBroadcastreceiver;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.anhealth.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MappaTuaIstanza extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GeofencingClient geofencingClient;
    private GeofenceHelperTuaIstanza geofenceHelperTuaIstanza;
    private Button rimuoviGeofence;
    private static final String TAG = "MappaTuaIstanza";
    private String GEOFENCE_ID = "SOME_GEOFENCE_ID";
    private int FINE_LOCATION_ACCESS_REQUEST_CODE = 10001;
    private int BACKGROUND_LOCATION_ACCESS_REQUEST_CODE = 10002;

    private double latitudineInstanza;
    private double longitudineInstanza;
    private float raggioInstanza;
    LatLng locationInstanza;

    int idInstanza;
    private AppDatabase appDatabase;
    private static Context context;
    private SharedPreferences prefs;
    private ValoriIstanze valoriIstanze;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mappa_tua_istanza);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.context = this;
        geofencingClient = LocationServices.getGeofencingClient(this);
        geofenceHelperTuaIstanza = new GeofenceHelperTuaIstanza(this);
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        //recupero le informazioni per impostare l'area del geofencing, data dall'instanza
        double latitudineInstanzaStringa = getIntent().getDoubleExtra("latitudineInstanza2", 0.0);
        latitudineInstanza = latitudineInstanzaStringa;
        double longitudineInstanzaStringa = getIntent().getDoubleExtra("longitudineInstanza2", 0.0);
        longitudineInstanza = longitudineInstanzaStringa;
        double raggioInstanzaStringa = getIntent().getDoubleExtra("raggioInstanza2", 0.0);
        raggioInstanza = Float.parseFloat(String.valueOf(raggioInstanzaStringa));
        locationInstanza = new LatLng(latitudineInstanza, longitudineInstanza);


        //me le salvo per evitare bug in fase di ri-apertura della mappa al click sulle notifiche
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(raggioInstanza != 0.0 || raggioInstanza != 0) {
            editor.putFloat("raggio", raggioInstanza);
            editor.putFloat("lat", Float.parseFloat(String.valueOf(latitudineInstanza)));
            editor.putFloat("long", Float.parseFloat(String.valueOf(longitudineInstanza)));

            editor.apply();
        }//altrimenti questo valore dovrebbe già essere settato nelle shared



        int idStringa = getIntent().getIntExtra("idTuaInstanza2", 0);
        Log.d(TAG, "Istanza arrivata alla mappa: "+idStringa);
        idInstanza = idStringa;


        mRecyclerView = findViewById(R.id.fragment_rec_vedi_valori);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //final List<String> geofenceId = new ArrayList<String>();
        //geofenceId.add(GEOFENCE_ID);


        rimuoviGeofence = findViewById(R.id.rimuoviGeofence);
        rimuoviGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(rimuoviGeofence.getText().toString().equals("Disattiva Istanza")){
                    //recupero lo stato dell'instanza dal db, e se effettivamente è attiva allora procedo con la rimozione del geofence ecc ecc..
                    new AsyncTaskGetStatoIstanza().execute();
                }else if(rimuoviGeofence.getText().toString().equals("Attiva Istanza")){
                    new AsyncTaskGetStatoIstanza().execute();


                }


            }
        });




    }
    float radius = 0;
    @Override
    protected void onStart() {
        new AsyncTaskGetStatoIstanza3().execute();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            Log.d(TAG, "onStart: service partito foreground");
            startForegroundService(new Intent(this, MyService.class));
        }else{
            Log.d(TAG, "onStart: service partito");
            startService(new Intent(this, MyService.class));
        }

        super.onStart();
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        //mMap.setMyLocationEnabled(true);
        enableUserLocation();
        SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        radius = prefs.getFloat("raggio", 0);
        //Log.d(TAG, "radius: "+radius);
        float lat = prefs.getFloat("lat", 0);
        float longit = prefs.getFloat("long", 0);

        if(radius != 0){ //potrei aggiungere i controlli anche su lat e long

            LatLng bologna = new LatLng(lat, longit);
            mMap.addMarker(new MarkerOptions().position(bologna).title("Marker di Bologna"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bologna, 14));

            mMap.clear(); //tolgo i marker precedenti, COSI POSIZIONO UN SOLO MARKER SULLA MAPPA
            addMarker(locationInstanza);
            addCircle(locationInstanza, raggioInstanza);


            //addGeofence(locationInstanza, radius); //era raggioInstanza, non radius
            //new AsyncTaskGetStatoIstanza3().execute();

        }else{
            Toast.makeText(MappaTuaIstanza.this, "Errore. Devi ricaricare la mappa. ", Toast.LENGTH_LONG).show();

        }

    }
    private void enableUserLocation(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            mMap.setMyLocationEnabled(true);
        }else{
            //chiedo i permessi
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                //mostro il dialog dove spiego perchè ho bisogno dei permessi e poi li chiedo all'utente
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);
            }else{
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);

            }
        }
    }
    private void addGeofence(LatLng latLng, float radius){
        Log.d(TAG, "ID INSTANZA "+idInstanza);

        //  NOTA: assegno l'id come geofenceID, perchè ogni istanza ha un id unico e quindi ogni geofence è unico ed associato all'istanza
        //GEOFENCE_ID = GEOFENCE_ID + " " +idInstanza;
        //Log.d(TAG, "GEOFENCE_ID: "+GEOFENCE_ID);
        Geofence geofence = geofenceHelperTuaIstanza.getGeofence(GEOFENCE_ID, latLng, radius, Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_EXIT);
        //LO AGGIUNGO AD UNA LISTA
        GeofencingRequest geofencingRequest = geofenceHelperTuaIstanza.getGeofencingRequest(geofence);
        //preparo l'intent
        PendingIntent pendingIntent = geofenceHelperTuaIstanza.getPendingIntent(idInstanza); //pending intent verso il broadcast
        //prima di aggiungere ilg eofence devo verificare che effettivamente l'istanza non sia attiva
        //se fosse attiva non dovrei aggiungere il geofence
        if(istanza.getStatoIstanza().equals("Non Attiva")){
            geofencingClient.addGeofences(geofencingRequest, pendingIntent)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //aggiungere geofence alla lista dei geofence addedd!!!!

                            Log.d(TAG, "onSuccess: Geofence Added...");

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            String errorMessage = geofenceHelperTuaIstanza.getErrorString(e);
                            Toast.makeText(MappaTuaIstanza.this, "Errore Geofencing, assicurati di aver impostato 'Consenti Sempre' nelle impostazioni di localizzazione di Geo Monitor. ", Toast.LENGTH_LONG).show();
                            Log.d(TAG, "onFailure: "+errorMessage);
                        }
                    });
        }else {
            Log.d(TAG, "Non aggiungo il geofence perchè l'istanza è già attiva ");
        }


        // --------------------- IMPORTANTEEEEE ---------------------
        Log.d(TAG, "ID GEOFENCE: "+ geofence.getRequestId());
    }


    private void removeGeofence(){
        geofencingClient.removeGeofences(geofenceHelperTuaIstanza.getPendingIntent(idInstanza))
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                        Log.d(TAG, "onSuccess: GEOFENCE RIMOSSO!");
                        //fermo il service
                        Intent myService = new Intent(MappaTuaIstanza.this, MyService.class);

                        // -------------------- DEVO FERMARE IL SERVICE SOLO SE è L'ULTIMA ISTANZA DA RIMUOVERE --------------------
                        //stopService(myService);
                        // ---------------------------------------------------------------------------------------------------------

                        rimuoviGeofence.setText("Attiva Istanza");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                        Log.d(TAG, "onFailure: GEOFENCE NON RIMOSSO!");
                    }
                });
    }



    private void addMarker(LatLng latLng){
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.addMarker(markerOptions);
    }
    private void addCircle(LatLng latLng, float radius){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(radius);
        circleOptions.strokeColor(Color.argb(255, 0, 128, 0));
        circleOptions.fillColor(Color.argb(64, 0, 128, 0));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == FINE_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                //mMap.setMyLocationEnabled(true);

            }else{
                //non ho i permessi
            }
        }


        //permessio di BACKGROUND_LOCATION_ACCESS_REQUEST_CODE
        if(requestCode == BACKGROUND_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                Toast.makeText(this, "Puoi aggiungere geofences", Toast.LENGTH_SHORT).show();

            }else{
                //non ho i permessi
                Toast.makeText(this, "Servono i permessi di background location access!!!!", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private RecyclerView mRecyclerView;
    private AdapterTuoiValori mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public void costruisciRecyclerView(Context context, ArrayList<Double> listaValoriOttenuti, ArrayList<String> listaParametri, ArrayList<String> livello) {


        mAdapter = new AdapterTuoiValori(listaValoriOttenuti,listaParametri, livello);
    }



    public static MappaTuaIstanza mappaTuaIstanza;
    public static ArrayList<Double> listaMedie = new ArrayList<Double>();
    public static ArrayList<String> listaParametri = new ArrayList<String>();
    public static ArrayList<String> livello = new ArrayList<String>();

    public MappaTuaIstanza(){
        mappaTuaIstanza = this;
    }
    public static MappaTuaIstanza getMappaTuaIstanza(){
        return mappaTuaIstanza;
    }
    public static void setVettoreValori(ArrayList<Double> vettoreMedieOttenuti){
        listaMedie = vettoreMedieOttenuti;
    }
    public static void setListaParametri(ArrayList<String> listaParOttenuta){
        listaParametri = listaParOttenuta;
    }
    public static void setLivello(ArrayList<String> livelloOttenuto){
        livello = livelloOttenuto;
    }
    void updateUI(Intent intent) {

        Date dataOggi = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        formattedDate = df.format(dataOggi);


        Log.d(TAG, "updateUI: eseguo");
            //ArrayList<Double> listaValoriOttenuti = (ArrayList<Double>) intent2.getSerializableExtra("vettoreValoriCalcolati");
            if(listaMedie != null){
                if(!listaMedie.isEmpty()){
                    //finish();
                    //Intent intent3 = new Intent(getApplicationContext(), MappaTuaIstanza.class);
                    //startActivity(intent3);
                    Log.d(TAG, "liste medie size: "+listaMedie.size());
                    Log.d(TAG, "liste listaParametri size: "+listaParametri.size());
                    Log.d(TAG, "liste livello size: "+livello.size());

                    costruisciRecyclerView(this, listaMedie, listaParametri, livello);
                    mRecyclerView.setAdapter(mAdapter);

                    if(!listaMedie.isEmpty() && !listaParametri.isEmpty()){
                        valoriIstanze = new ValoriIstanze();
                        valoriIstanze.setNomeParametro(listaParametri.get(0));
                        valoriIstanze.setValoreOttenuto(listaMedie.get(0));
                        valoriIstanze.setDataInserimento(formattedDate);
                        new AsyncTaskAggiungiValoreIstanza().execute(valoriIstanze);

                    }


                }
            }
    }

    private int contatoreInserimenti = 0;
    private String formattedDate = "";
    public class AsyncTaskAggiungiValoreIstanza extends AsyncTask<ValoriIstanze, Void, Void> {
        @Override
        protected Void doInBackground(ValoriIstanze... valoriIstanzes) {
            appDatabase.daoValoriIstanze().aggiungiValoreIstanza(valoriIstanzes[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "Template aggiunto ",
              //      Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Valor medio parametro aggiunto al database");

            contatoreInserimenti++;
            if(contatoreInserimenti<listaMedie.size()){
                valoriIstanze = new ValoriIstanze();
                valoriIstanze.setNomeParametro(listaParametri.get(contatoreInserimenti));
                valoriIstanze.setValoreOttenuto(listaMedie.get(contatoreInserimenti));
                valoriIstanze.setDataInserimento(formattedDate);
                new AsyncTaskAggiungiValoreIstanza().execute(valoriIstanze);
            }else{
                contatoreInserimenti = 0;
            }

        }
    }


    public class AsyncTaskAggiornaStatoIstanzaAdAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoCustomServiceLocal().aggiornaStatoIstanza("Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza aggiornato ad Attiva");



        }
    }


    public class AsyncTaskAggiornaStatoIstanzaANonAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            //dove integers[0] rappresenta l'id dell'istanza
            appDatabase.daoCustomServiceLocal().aggiornaStatoIstanza("Non Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza aggiornato a Non Attiva");
        }
    }

    CustomServiceLocal istanza ;
    public class AsyncTaskGetStatoIstanza extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            istanza = appDatabase.daoCustomServiceLocal().getIstanzaFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza recuperato");

            Log.d(TAG, "Stato: "+istanza.getStatoIstanza()+ " e id istanza recuperata con questo stato è: "+istanza.getId());

            if(rimuoviGeofence.getText().toString().equals("Disattiva Istanza")){
                //procedo con la rimozione del geofence
                removeGeofence();
                //aggiorno lo stato dell'instanza nel db a "non attiva"
                new AsyncTaskAggiornaStatoIstanzaANonAttiva().execute(istanza.getId());
            }else if(rimuoviGeofence.getText().toString().equals("Attiva Istanza")){
                Log.d(TAG, "radius: "+radius);
                //addGeofence(locationInstanza, radius);
                Log.d(TAG, "onClick: ISTANZA ATTIVATA"+idInstanza);
                rimuoviGeofence.setText("Disattiva Istanza");

                addGeofence(locationInstanza,radius);


                new AsyncTaskAggiornaStatoIstanzaAdAttiva().execute(idInstanza);

                onStart(); //si occupa di startare il service
            }



        }
    }


    public class AsyncTaskGetStatoIstanza3 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            istanza = appDatabase.daoCustomServiceLocal().getIstanzaFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza recuperato");


            if(istanza.getStatoIstanza().equals("Attiva")){
                rimuoviGeofence.setText("Disattiva Istanza");
            }else{
                rimuoviGeofence.setText("Attiva Istanza");

            }


        }
    }

}
