package com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.InstanzeDeiTemplate.VediInstanzeSensquare;
import com.example.anhealth.MostraTemplateCostruiti.AdapterTuoiTemplate;
import com.example.anhealth.MostraTemplateCostruiti.MostraRegoleContenute;
import com.example.anhealth.MyAdapter;
import com.example.anhealth.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTuoiValori  extends RecyclerView.Adapter<AdapterTuoiValori.TuoiValoriHolder>{
    ArrayList<Double> valori = new ArrayList<Double>();
    ArrayList<String> listaParametri = new ArrayList<String>();
    ArrayList<String> livello = new ArrayList<String>();
    public class TuoiValoriHolder extends RecyclerView.ViewHolder {
        public TextView parametro, valore, livello_pericolosita;
        public TuoiValoriHolder(@NonNull View itemView) {
            super(itemView);
            valore = itemView.findViewById(R.id.valId);
            parametro = itemView.findViewById(R.id.parId);
            livello_pericolosita = itemView.findViewById(R.id.pericolositaText);
        }


    }

    public AdapterTuoiValori(ArrayList<Double> valoriRicevuti, ArrayList<String> parametri, ArrayList<String> livelloOttenuto) {
        valori = valoriRicevuti;
        listaParametri = parametri;
        livello = livelloOttenuto;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterTuoiValori.TuoiValoriHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.riga_tuoi_valori, parent, false);
        AdapterTuoiValori.TuoiValoriHolder myHolder = new TuoiValoriHolder(v);
        return myHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterTuoiValori.TuoiValoriHolder holder, final int position) {
        holder.valore.setText(String.valueOf(valori.get(position)));
        holder.parametro.setText(String.valueOf(listaParametri.get(position)));
        holder.livello_pericolosita.setText(String.valueOf(livello.get(position)));
       //holder.numeroRegola.setText(String.valueOf(position+1));

    }

    @Override
    public int getItemCount() {
        return valori.size();
    }

}
