package com.example.anhealth.InstanzeDeiTemplate;

import android.app.IntentService;
import android.app.ListActivity;
import android.content.Intent;
import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;


public class GeofencingService extends IntentService {

    public GeofencingService() {
        super("GeofencingService");
    }
    NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());



    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if(event.hasError()){

        }else{
            int transition = event.getGeofenceTransition();
            List<Geofence> geofences = event.getTriggeringGeofences();
            Geofence geofence = geofences.get(0);
            String requestID = geofence.getRequestId();
            switch (transition){
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    Toast.makeText(getApplicationContext(), "SERVICE Geofence transition ENTER", Toast.LENGTH_SHORT).show();
                    notificationHelper.sendHighPriorityNotification("GEOFENCE_TRANSITION_ENTER", "Sei entrato nell'area Geofence", MapsActivity.class);
                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    //il DWEEL va sampato ogni tot minuti
                    Toast.makeText(getApplicationContext(), "SERVICE Geofence transition DWEEL", Toast.LENGTH_SHORT).show();
                    notificationHelper.sendHighPriorityNotification("GEOFENCE_TRANSITION_DWELL", "Sei dentro l'area Geofence", MapsActivity.class);


                    //Inizio dell'esecuzione del processo di instanziazione

                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    Toast.makeText(getApplicationContext(), "SERVICE Geofence transition EXIT", Toast.LENGTH_SHORT).show();
                    notificationHelper.sendHighPriorityNotification("Sei uscito dall'area Geofence", "", MapsActivity.class);

                    break;
            }
        }

    }


}
