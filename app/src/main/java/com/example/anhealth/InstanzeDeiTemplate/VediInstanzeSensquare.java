package com.example.anhealth.InstanzeDeiTemplate;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;


import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.IstanzeAttiveSensquare;
import com.example.anhealth.R;



public class VediInstanzeSensquare extends AppCompatActivity {
    private SharedPreferences prefs;
    private TextView titoloText, id, descrizioneText, parametriTrattati, latitudineText, longitudineText, raggioText, stato;
    private Button instanziaTemplateBtn;
    private AppDatabase appDatabase;
    private static final String TAG = "VediInstanzeSensquare";
    private int idStringa = 0;
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vedi_instanze_sensquare);
        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "datiReportDB").build();
        stato = findViewById(R.id.statoIstanzaSensquare);

        titoloText = findViewById(R.id.titInstSens);
        id = findViewById(R.id.idInst);
        descrizioneText = findViewById(R.id.idDescripTem);
        parametriTrattati = findViewById(R.id.args);

        instanziaTemplateBtn = findViewById(R.id.attivaInstanza);

        latitudineText = findViewById(R.id.idLat);
        longitudineText = findViewById(R.id.idLong);
        raggioText = findViewById(R.id.idRaggio);

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        long idInstanzaSceltaDaVisualizzare = prefs.getLong("idInstanzaScelta", 1);

        String descrizione = getIntent().getStringExtra("descrizione");
        String titolo = getIntent().getStringExtra("titolo");
        idStringa = getIntent().getIntExtra("idInstanza", 0); //id dell'instaziazione
        String templateID = getIntent().getStringExtra("templateID"); //questo dovrà servire alla mappa per recuperare il template


        String coordinateERaggio = getIntent().getStringExtra("coordinateERaggio");
        String[] stringaSplittata = coordinateERaggio.split("\\s+");
        final double latitudineInstanza = Double.parseDouble(stringaSplittata[0]); //NELLA POSIZIONE 0 C'è LA LATITUDINE
        final double longitudineInstanza = Double.parseDouble(stringaSplittata[1]); //NELLA POSIZIONE 1 C'è LA LONGITUDINE
        final double raggioInstanza = Double.parseDouble(stringaSplittata[2]); //NELLA POSIZIONE 2 C'è IL RAGGIO


        Log.d("lat ","LATITUDINE: "+latitudineInstanza);
        Log.d("long ","LONGITUDINE: "+longitudineInstanza);
        Log.d("ragg ","RAGGIO: "+raggioInstanza);

        latitudineText.append(String.valueOf(latitudineInstanza));
        longitudineText.append(String.valueOf(longitudineInstanza));
        raggioText.append(String.valueOf(raggioInstanza));
        descrizioneText.append(" "+descrizione);
        titoloText.setText(titolo);
        id.setText(Integer.toString(idStringa));
        Log.d(TAG, "TEMPLATE ID SUL INERENTE A QUEST'INSTANZA: "+templateID);

        final Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        Log.d(TAG, "latitudineInstanza: "+latitudineInstanza);
        intent.putExtra("latitudineInstanza", latitudineInstanza);
        intent.putExtra("longitudineInstanza", longitudineInstanza);
        intent.putExtra("raggioInstanza", raggioInstanza);
        intent.putExtra("idInstanza", idStringa);


        final NumberPicker numberPicker = findViewById(R.id.numberPicker2);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(120);
        numberPicker.setTextSize(80);

        instanziaTemplateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                Log.d(TAG, "valore picker "+numberPicker.getValue());
                editor.putInt("pickerVal", numberPicker.getValue());
                editor.apply();
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        new AsyncOttieniStatoIstanza().execute();
    }

    IstanzeAttiveSensquare istanza ;
    public class AsyncOttieniStatoIstanza extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "doInBackground: "+idStringa);
            istanza = appDatabase.daoIstanzeAttiveSensquare().getIstanzaSensquareFiltrata(idStringa);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza recuperato");

            /**
             *
             * È probabile che l'istanza non sia nel database perchè vengono aggiunte solo se sono attive.
             * Non dovrebbero esserci nel database istanze di SENSQUARE con stato NON ATTIVO
             *
             * Quindi...
             * */
            if(istanza != null){
                Log.d(TAG, "stato: "+istanza.getStatoIstanza());

                stato.setText(istanza.getStatoIstanza());
                if(istanza.getStatoIstanza().equals("Attiva")){
                    instanziaTemplateBtn.setText("Visiona Istanza");
                }else{
                    instanziaTemplateBtn.setText("Attiva Istanza");

                }
            }else { //se l'istanza non è nel DB ha stato "Non Attivo
                stato.setText("Non Attiva");
                instanziaTemplateBtn.setText("Attiva Istanza");
            }




        }
    }
}
