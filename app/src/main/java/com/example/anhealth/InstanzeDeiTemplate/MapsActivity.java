package com.example.anhealth.InstanzeDeiTemplate;

import androidx.annotation.ColorInt;
import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.IstanzeAttiveSensquare;
import com.example.anhealth.Database.ValoriIstanze;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTuoiValori;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.MappaTuaIstanza;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.MyService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.anhealth.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private GeofencingClient geofencingClient;
    private GeofenceHelper geofenceHelper;

    GoogleApiClient googleApiClient = null;

    private static final String TAG = "MapsActivity"; 
    private String GEOFENCE_ID = "SOME_GEOFENCE_ID2";
    private int FINE_LOCATION_ACCESS_REQUEST_CODE = 10001;
    private int BACKGROUND_LOCATION_ACCESS_REQUEST_CODE = 10002;

    private float GEOFENCE_RADIUS = 100;


    private double latitudineInstanza;
    private double longitudineInstanza;
    private float raggioInstanza;
    LatLng locationInstanza;
    AppDatabase appDatabase;
    IstanzeAttiveSensquare istanzaSensquareAttivata;


    int idInstanza;

    private Button attivaIstanzaBottone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();


        geofencingClient = LocationServices.getGeofencingClient(this);
        geofenceHelper = new GeofenceHelper(this);

        //recupero le informazioni per impostare l'area del geofencing, data dall'instanza
        double latitudineInstanzaStringa = getIntent().getDoubleExtra("latitudineInstanza", 0.0);
        latitudineInstanza = latitudineInstanzaStringa;
        double longitudineInstanzaStringa = getIntent().getDoubleExtra("longitudineInstanza", 0.0);
        longitudineInstanza = longitudineInstanzaStringa;
        double raggioInstanzaStringa = getIntent().getDoubleExtra("raggioInstanza", 0.0);

        raggioInstanza = Float.parseFloat(String.valueOf(raggioInstanzaStringa));
        locationInstanza = new LatLng(latitudineInstanza, longitudineInstanza);

        int idStringa = getIntent().getIntExtra("idInstanza", 0);
        idInstanza = idStringa;
        istanzaSensquareAttivata = new IstanzeAttiveSensquare();

        mRecyclerView = findViewById(R.id.fragment_rec_vedi_valori2);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        attivaIstanzaBottone = findViewById(R.id.rimuoviGeofence2);
        attivaIstanzaBottone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(istanzaSensquare != null){
                    if(istanzaSensquare.getStatoIstanza().equals("Attiva")){
                        Log.d(TAG, "onStart: cambiato bottone2");
                        attivaIstanzaBottone.setText("Disattiva Istanza");
                    }else if(istanzaSensquare.getStatoIstanza().equals("Non Attiva")){
                        Log.d(TAG, "onStart: cambiato bottone3");

                        attivaIstanzaBottone.setText("Attiva Istanza");

                    }
                }else{
                    attivaIstanzaBottone.setText("Disattiva Istanza");
                }
                istanzaSensquare = null; //la devo ri-inizializzare per favorire usi successivi

                //prima controllo che l'istanza non sia già presente nel db
                new AsyncTaskGetStatoIstanzaSensquare().execute();

            }
        });




    }
    @Override
    protected void onStart() {
        super.onStart();
        // startService(new Intent(MappaTuaIstanza.this,MyService.class));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startForegroundService(new Intent(this, MyService.class));
        else
            startService(new Intent(this, MyService.class));
        new AsyncTaskGetStato().execute();





    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableUserLocation();
        mMap.setMyLocationEnabled(true);
        // Add a marker in Sydney and move the camera
        //LatLng bologna = new LatLng(44.49, 11.34);
        LatLng bologna = new LatLng(latitudineInstanza, longitudineInstanza);
        mMap.addMarker(new MarkerOptions().position(bologna).title("Marker di Bologna"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bologna, 14));

        //enableUserLocation();


        mMap.clear(); //tolgo i marker precedenti, COSI POSIZIONO UN SOLO MARKER SULLA MAPPA
        addMarker(locationInstanza);
        addCircle(locationInstanza, raggioInstanza);

        //devo ottenere lo stato dell'istanza



    }




    private void enableUserLocation(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            mMap.setMyLocationEnabled(true);
        }else{
            //chiedo i permessi
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                //mostro il dialog dove spiego perchè ho bisogno dei permessi e poi li chiedo all'utente
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);
            }else{
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_REQUEST_CODE);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == FINE_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                //mMap.setMyLocationEnabled(true);

            }else{
                //non ho i permessi
            }
        }


        //permessio di BACKGROUND_LOCATION_ACCESS_REQUEST_CODE
        if(requestCode == BACKGROUND_LOCATION_ACCESS_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //ho i permessi
                Toast.makeText(this, "Puoi aggiungere geofences", Toast.LENGTH_SHORT).show();

            }else{
                //non ho i permessi
                Toast.makeText(this, "Servono i permessi di background location access!!!!", Toast.LENGTH_SHORT).show();

            }
        }
    }



    private void addGeofence(LatLng latLng, float radius){
        Geofence geofence = geofenceHelper.getGeofence(GEOFENCE_ID, latLng, radius, Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_EXIT);
        GeofencingRequest geofencingRequest = geofenceHelper.getGeofencingRequest(geofence);
        //preparo l'intent

        Log.d(TAG, "ID INSTANZA "+idInstanza);
        PendingIntent pendingIntent = geofenceHelper.getPendingIntent(idInstanza);

        geofencingClient.addGeofences(geofencingRequest, pendingIntent)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: Geofence Added...");


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String errorMessage = geofenceHelper.getErrorString(e);
                        Log.d(TAG, "onFailure: "+errorMessage);
                    }
                });
    }

    private void addMarker(LatLng latLng){
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.addMarker(markerOptions);
    }
    private void addCircle(LatLng latLng, float radius){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(radius);
        circleOptions.strokeColor(Color.argb(255, 0, 128, 0));
        circleOptions.fillColor(Color.argb(64, 0, 128, 0));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);

    }

    private void removeGeofence(){
        geofencingClient.removeGeofences(geofenceHelper.getPendingIntent(idInstanza))
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                        Log.d(TAG, "onSuccess: GEOFENCE RIMOSSO!");
                        //fermo il service
                        Intent myService = new Intent(MapsActivity.this, MyService.class);

                        // -------------------- DEVO FERMARE IL SERVICE SOLO SE è L'ULTIMA ISTANZA DA RIMUOVERE --------------------
                        //stopService(myService);
                        // ---------------------------------------------------------------------------------------------------------

                        //rimuoviGeofence.setText("Attiva Istanza");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                        Log.d(TAG, "onFailure: GEOFENCE NON RIMOSSO!");
                    }
                });
    }




    /**
     * Gestione dello stato delle istanze di sensquare
     *
     * */
    IstanzeAttiveSensquare istanzaSensquare ;
    public class AsyncTaskGetStatoIstanzaSensquare extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "idInstanza: "+idInstanza);
            istanzaSensquare = appDatabase.daoIstanzeAttiveSensquare().getIstanzaSensquareFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza sensquare recuperato");
            if(istanzaSensquare == null){
                Log.d(TAG, "onPostExecute: sono qui 1");
                attivaIstanzaBottone.setText("Disattiva Istanza");
                istanzaSensquareAttivata.setIdIstanza(idInstanza);
                istanzaSensquareAttivata.setStatoIstanza("Attiva");
               new AsyncTaskInserisciInstanzaSensquare().execute(istanzaSensquareAttivata);
            }else{//vuol dire che l'istanza è gia attiva
                Log.d(TAG, "istanzaSensquare.get: "+istanzaSensquare.getStatoIstanza()+" "+istanzaSensquare.getIdIstanza());
                if(istanzaSensquare.getStatoIstanza().equals("Attiva")){
                    Log.d(TAG, "onPostExecute: sono qui 2");

                    //se è attiva devo disattivarla
                    new AsyncTaskAggiornaStatoIstanzaSensquareANonAttiva().execute(istanzaSensquare.getId());
                }else if(istanzaSensquare.getStatoIstanza().equals("Non attiva")){

                    Log.d(TAG, "onPostExecute: sono qui 3");

                    //se non è attiva devo attivarla
                    new AsyncTaskAggiornaStatoIstanzaSensquareAdAttiva().execute(istanzaSensquare.getId());
                }
            }

        }
    }
    //deve solo recuperare l'istanza senza fare operazioni al contrario dell'altro async task simile
    public class AsyncTaskGetStato extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "idInstanza: "+idInstanza);
            istanzaSensquare = appDatabase.daoIstanzeAttiveSensquare().getIstanzaSensquareFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Log.d(TAG, "Stato istanza sensquare recuperato + ho cambiato testo al bottone");
            if(istanzaSensquare != null){
                if(istanzaSensquare.getStatoIstanza().equals("Attiva")){
                    Log.d(TAG, "onStart: cambiato bottone");
                    attivaIstanzaBottone.setText("Disattiva Istanza");
                }else if(istanzaSensquare.getStatoIstanza().equals("Non Attiva")){
                    attivaIstanzaBottone.setText("Attiva Istanza");

                }

            }

        }
    }
    public class AsyncTaskAggiornaStatoIstanzaSensquareANonAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoIstanzeAttiveSensquare().aggiornaStatoIstanzaSensquare("Non Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza aggiornato a Non Attiva");

            //rimuovo il geofence dalla zona
            removeGeofence();

            /**
             * TODO: Devo rimuovere l'istanza dal database
             * */
            Log.d(TAG, "onPostExecute: Sto per rimuovere l'istanza con id: "+istanzaSensquare.getIdIstanza()); //importante notare a quale id faccio riferimento, non quello della chiave primaria
            new AsyncTaskRimuoviIstanzaAttiva().execute(idInstanza);




        }
    }
    public class AsyncTaskAggiornaStatoIstanzaSensquareAdAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoIstanzeAttiveSensquare().aggiornaStatoIstanzaSensquare("Attiva", integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza aggiornato ad Attiva");

            attivaIstanzaBottone.setText("Disattiva Istanza");

            //creo il geofence
            addGeofence(locationInstanza, raggioInstanza);

        }
    }
    public class AsyncTaskRimuoviIstanzaAttiva extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... integers) {
            appDatabase.daoIstanzeAttiveSensquare().rimuoviIstanza(integers[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Istanza rimossa dal db delle istanze attive");

            attivaIstanzaBottone.setText("Attiva Istanza");

        }
    }


    public class AsyncTaskInserisciInstanzaSensquare extends AsyncTask<IstanzeAttiveSensquare, Void, Void> {
        @Override
        protected Void doInBackground(IstanzeAttiveSensquare... istanzeAttiveSensquares) {
            appDatabase.daoIstanzeAttiveSensquare().aggiungiInstanzaSensquare(istanzeAttiveSensquares[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "Istanza aggiunta ",
                    //Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onPostExecute: Istanza Aggiunta");
            addGeofence(locationInstanza, raggioInstanza);
        }
    }




    private RecyclerView mRecyclerView;
    private AdapterTuoiValori mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public void costruisciRecyclerView(Context context, ArrayList<Double> listaValoriOttenuti, ArrayList<String> listaParametri, ArrayList<String> livello) {


        mAdapter = new AdapterTuoiValori(listaValoriOttenuti,listaParametri, livello);
        mRecyclerView.setAdapter(mAdapter);

    }



    public static MapsActivity mapsActivity;
    public static ArrayList<Double> listaMedie = new ArrayList<Double>();
    public static ArrayList<String> listaParametri = new ArrayList<String>();
    public static ArrayList<String> livello = new ArrayList<String>();

    public MapsActivity(){
        mapsActivity = this;
    }
    public static MapsActivity getMappaTuaIstanza(){
        return mapsActivity;
    }
    public static void setVettoreValori(ArrayList<Double> vettoreMedieOttenuti){
        listaMedie = vettoreMedieOttenuti;
    }
    public static void setListaParametri(ArrayList<String> listaParOttenuta){
        listaParametri = listaParOttenuta;
    }
    public static void setLivello(ArrayList<String> livelloOttenuto){
        livello = livelloOttenuto;
    }
    void updateUI(Intent intent) {
        for(int i = 0; i<listaMedie.size(); i++){
            //if(i<vettoreParametri.size()){ //evito l'index out of bound
            switch (listaParametri.get(i)){
                case "PM10":
                    if(listaMedie.get(i)>50){
                        livello.add("Critico");
                    }else{
                        livello.add("Normale");
                    }
                    break;
                case "PM25":
                    if(listaMedie.get(i)>25){ //anche se 25 è la media annuale
                        livello.add("Critico");
                    }else{
                        livello.add("Normale");
                    }
                    break;
                case "O3OZ":
                    if(listaMedie.get(i)>120){ //uso la media oraria(120), quella giornaliera è 180
                        livello.add("Critico");
                    }else{
                        livello.add("Normale");
                    }
                    break;
                case "COPR":
                    if(listaMedie.get(i)>10){
                        livello.add("Critico");
                    }else{
                        livello.add("Normale");
                    }
                    break;
                default:
                    livello.add("-");//caso in cui ho i parametri locali
                    break;

            }
            //}

        }


        Log.d(TAG, "updateUI: eseguo");
        if(listaMedie != null){
            if(!listaMedie.isEmpty()){

                Log.d(TAG, "liste medie size: "+listaMedie.size());
                Log.d(TAG, "liste listaParametri size: "+listaParametri.size());
                Log.d(TAG, "liste livello size: "+livello.size());

                costruisciRecyclerView(this, listaMedie, listaParametri, livello);


            }
        }
    }


}
