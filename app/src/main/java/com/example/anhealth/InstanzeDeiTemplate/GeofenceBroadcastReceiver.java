package com.example.anhealth.InstanzeDeiTemplate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceLocal;
import com.example.anhealth.Database.IstanzeAttiveSensquare;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.BroadcastGeofenceTuaIstanza;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.MappaTuaIstanza;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.MyService;
import com.example.anhealth.MostraTemplateCostruiti.SchermataTemplateCostruiti;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    int idInstanza;
    private Context context;
    NotificationHelper notificationHelper;
    private static final String TAG = "GeofenceBroadcastReceiv";
    ArrayList<String> valori_e_parametri = new ArrayList<String>();
    private AppDatabase appDatabase;
    private static Timer timer;
    private static TimerTask timerTask;
    private static boolean uscito = true; //per capire se sono dentro o fuori dalla zona
    private Intent intent;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        this.context = context;
        //Toast.makeText(context, "Geofence triggered... ", Toast.LENGTH_SHORT).show();
        notificationHelper = new NotificationHelper(context);
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "datiReportDB").build();
        this.intent = intent;

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        
        if(geofencingEvent.hasError()){
            Log.d(TAG, "onReceive: ERROR receiving geofence event... ");
            return;
        }



        List<Geofence> geofenceList = geofencingEvent.getTriggeringGeofences();
        for(Geofence geofence: geofenceList){
            Log.d(TAG, "onReceive: "+geofence.getRequestId());
        }

        //Location location = geofencingEvent.getTriggeringLocation();
        final int transitionType = geofencingEvent.getGeofenceTransition();

        switch (transitionType){
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                timer = new Timer();//creo il timer quando entro

                Toast.makeText(context, "Sei entrato nell'area", Toast.LENGTH_SHORT).show();
                //notificationHelper.sendHighPriorityNotification("SEI ENTRATO NELL'AREA DELL'INSTANZA", "Sei entrato nell'area Geofence", MapsActivity.class);
                uscito = false;
                idInstanza = intent.getIntExtra("idInstanza",0);

                SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                int minuti_shared = prefs.getInt("pickerVal", 5);
                //moltiplico il valore minuti_shared per 1000
                Log.d(TAG, "minuti_shared: "+minuti_shared);
                int minuti = minuti_shared*60*1000;
                timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(TAG, "SEI fuori dalla LA ZONA?: "+uscito);
                        Log.d(TAG, "transizione: "+Geofence.GEOFENCE_TRANSITION_ENTER);


                        //Prima controllo che il service sia in running. Se non è più in running, interrompo l'esecuzione del timer ...
                        if(!MyService.isRunning()){
                            cancel();
                        }else if(uscito == false && transitionType == Geofence.GEOFENCE_TRANSITION_ENTER){
                            //inizializzo
                            valori_e_parametri.clear();
                            Log.d(TAG, " --------------------------------- run: ALTRO GIRO ---------------------------------");

                            new AsyncTaskGetStatoIstanzaSens2().execute();


                        }else{
                            cancel();
                            timer.purge();
                        }

                    }
                };
                //Set the schedule function and rate
                timer.scheduleAtFixedRate(timerTask,0, minuti);

                break;

            case Geofence.GEOFENCE_TRANSITION_EXIT:
                uscito = true;
                Log.d(TAG, "HO SETTATO USCITO A: "+uscito);
                //Toast.makeText(context, "Sei uscito dall'area", Toast.LENGTH_SHORT).show();
                notificationHelper.sendHighPriorityNotification("SEI USCITO DALL'AREA DELL'INSTANZA", "Sei uscito dall'area monitorata", MapsActivity.class);
                timerTask.cancel();
                timer.purge();
                timer.cancel();
                break;
        }


    }
    double valoreCalcolato = 0.0;
    private  ArrayList<Double> listaMedieSenzaDoppioni = new ArrayList<Double>();
    private  ArrayList<String> vettoreParametri = new ArrayList<String>();
    private  ArrayList<String> vettoreLivelloPericolo = new ArrayList<String>();

    public void recuperaValori(){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://sensquare.disi.unibo.it/api/v1/instances/"+idInstanza+"/execute/";

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            valoreCalcolato = response.getDouble("value");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            JSONArray jsonArray = response.getJSONArray("args");
                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject args = jsonArray.getJSONObject(i);
                                String valoreOttenuto = args.getString("value");
                                Double value = args.getDouble("value");

                                Log.d(TAG, "VALORE INSTANZA: "+valoreOttenuto);
                                String dataClass = args.getString("data_class");
                                Log.d(TAG, "DATA CLASS: "+dataClass);
                                valori_e_parametri.add(valoreOttenuto);
                                valori_e_parametri.add(dataClass);

                                listaMedieSenzaDoppioni.add(value);
                                vettoreParametri.add(dataClass);

                                //ARRAY LIST FORMATO DA VALORE E PARAMETRO, alternati prima valore e poi parametro e cosi via
                                // valPar.add(valoreOttenuto);
                                //valPar.add(dataClass);
                            }

                            String descrizioneNotifica = "";
                            for(int i = 0; i<valori_e_parametri.size()-1; i++){
                                if(i % 2 == 0){ //prendo solo gli indici che danno resto 0, QUINDI PRENDO I VALORI PARI
                                    descrizioneNotifica = descrizioneNotifica+ "\nValore del "+valori_e_parametri.get(i+1) + " = " + valori_e_parametri.get(i) + "\n";

                                }
                            }
                            Log.d(TAG, "valori_e_parametri size "+valori_e_parametri.size());
                            //notificationHelper.sendHighPriorityNotification("Valori calcolati", ""+descrizioneNotifica, MapsActivity.class);
                            notificationHelper.sendHighPriorityNotification("Valore calcolato", ""+valoreCalcolato, MapsActivity.class);



                            MapsActivity.setListaParametri(vettoreParametri);
                            MapsActivity.setVettoreValori(listaMedieSenzaDoppioni);
                            MapsActivity.setLivello(vettoreLivelloPericolo);
                            MapsActivity.getMappaTuaIstanza().updateUI(intent);


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());

                    }
                }
        );
        queue.add(objectRequest);

    }

    IstanzeAttiveSensquare istanza ;
    public class AsyncTaskGetStatoIstanzaSens2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            istanza = appDatabase.daoIstanzeAttiveSensquare().getIstanzaSensquareFiltrata(idInstanza);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "Stato istanza sensquare recuperato");
            if(istanza!=null){
                if(istanza.getStatoIstanza().equals("Attiva")){
                    recuperaValori();
                }else{
                    Log.d(TAG, " timer.scheduleAtFixedRate STOPPATO perchè l'istanza è stata disattivata");
                    timer.cancel();
                }
            }else{
                timer.cancel();

                Log.d(TAG, " timer.scheduleAtFixedRate STOPPATO perchè l'istanza è stata disattivata");

            }


        }
    }


}
