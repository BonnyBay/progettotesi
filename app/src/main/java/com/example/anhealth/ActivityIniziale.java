package com.example.anhealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.anhealth.Database.AppDatabase;
//import com.example.anhealth.I_Tuoi_Template_Recycler.TuoiTemplateActivity;
import com.example.anhealth.Home.PaginaPrincipale;
import com.example.anhealth.MontaTemplate.SchermataCostruzione;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.SchermataTueInstanze;
import com.example.anhealth.MostraTemplateCostruiti.SchermataTemplateCostruiti;
import com.google.android.material.navigation.NavigationView;

public class ActivityIniziale extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static AppDatabase appDatabase;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences prefs;
    private Button tuoiTemplate, costruisciTemplate, tueInstanzeBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniziale);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //seleziono subito un item
        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_home);
        }

        tuoiTemplate = findViewById(R.id.tuoiTemplateBtn);
        costruisciTemplate = findViewById(R.id.costruisciTemplateBtn);
        tueInstanzeBtn = findViewById(R.id.tueInstanze);

        tueInstanzeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SchermataTueInstanze.class);
                startActivity(intent);
            }
        });

        tuoiTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SchermataTemplateCostruiti.class);
                startActivity(intent);
            }
        });

        costruisciTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SchermataCostruzione.class);
                startActivity(intent);
            }
        });


    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        /*
        *  |------------------------------------------------------------------------------------|
        *  |------- LA GESTIONE DEL MENU AVVIENE IN MODO DIFFERENTE DALLA MAIN ACTIVITY --------|
        *
        *   IN QUESTO CASO SETTO UNA VARIABILE PUTEXTRA E POI LANCIO LA MAINACTIVITY
        *   NELLO ONSTART DELLA MAIN ACTIVITY, IN BASE AL VALORE DELLA VARIABILE PUTEXTRA
        *   SI APRIRA LA VOCE CORRISPONDENDE DEL MENU
        *
        *  |-----------------------------------------------------------------------------------|
        *
        * */
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                Intent intent2 = new Intent(getApplicationContext(), PaginaPrincipale.class);
                // nuova versione
                intent2.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "home");
                editor.apply();
                startActivity(intent2);
                finish();
                break;

            case R.id.nav_salute:
                // nuova versione
                intent.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "home");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_viewReport:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "viewReport");
                editor.apply();
                startActivity(intent);
                finish();
                break;

            case R.id.nav_graficoBarre:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "graficoBarre");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_graficoTorta:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "graficoTorta");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_settings: //in questo caso aprò direttamente un activity e non un fragment, non ho bisogno dei putextra
                Intent intentSettings = new Intent(getApplicationContext(), SettingsActivity.class);
                drawerLayout.closeDrawer(GravityCompat.START);//chiudo il menu, cosi quando torno indietro sarà chiuso
                startActivity(intentSettings);
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (count == 0) {
            super.onBackPressed(); //finish
        } else if(count > 0){
            //simula il back
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}
