package com.example.anhealth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


public class HomeFragment extends Fragment implements View.OnClickListener{

    private Button aggiungiReport, visualizzaCalendario,visualizzaGraficoABArre;
    private SharedPreferences prefs;
    private Bundle savedInstanceState;
    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.savedInstanceState = savedInstanceState;
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        aggiungiReport = view.findViewById(R.id.aggiungiReport);
        prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        aggiungiReport.setOnClickListener(this);
        visualizzaCalendario = view.findViewById(R.id.btn_view_calendar);
        visualizzaCalendario.setOnClickListener(this);

        visualizzaGraficoABArre = view.findViewById(R.id.btn_view_cambiaGrafico);
        visualizzaGraficoABArre.setOnClickListener(this);
        return view;

    }

    @Override
    public void onClick(View view) {
        SharedPreferences prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        switch (view.getId()){
            case R.id.aggiungiReport:
                 MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new AggiungiDatoUtenteFragment()).commit();//non lo aggiungo allo stack, non potrò fare il back ma evito il problema del "OVERLAP"
                 break;
            case R.id.btn_view_cambiaGrafico:
                String tipologiaGrafico = prefs.getString("tipologia", "barre");
                if(tipologiaGrafico.equals("barre")){ //al click sul bottone le inverto
                    editor.putString("tipologia", "torta");
                    editor.apply();
                    getFragmentManager().beginTransaction().replace(R.id.containerHome_fragment, new Graf2()).commit(); //grafico a torta

                    Toast.makeText(getActivity(), " tipologiaGrafico: torta" , Toast.LENGTH_LONG).show();
                }else if(tipologiaGrafico.equals("torta")){
                    editor.putString("tipologia", "barre");
                    editor.apply();
                    getFragmentManager().beginTransaction().replace(R.id.containerHome_fragment, new Graf1()).commit(); //grafico a barre
                    Toast.makeText(getActivity(), " tipologiaGrafico: barre" , Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_view_calendar:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new calendarFragment()).commit(); //anche qui: niente add allo stack
                break;
        }
    }




    @Override
    public void onStart() {
        super.onStart();
        if(savedInstanceState == null){ //gestisco la rotazione
            //la tipologia del grafico può essere settata o di default o quando l'utente clicca su cambia grafico
            String tipologiaGrafico = prefs.getString("tipologia", "barre");
            //disegno il grafico sull'home fragment (nel suo frame layout containterHome_fragment)
            if (tipologiaGrafico.equals("barre")) {
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.containerHome_fragment, new Graf1()).commit(); // grafico a barre
            } else if (tipologiaGrafico.equals("torta")) {
                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.containerHome_fragment, new Graf2()).commit(); //grafico a torta

            }
        }

    }
}
