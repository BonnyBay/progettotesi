package com.example.anhealth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.room.Room;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Home.PaginaPrincipale;
import com.google.android.material.navigation.NavigationView;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static FragmentManager fragmentManager;
    public static AppDatabase appDatabase;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences prefs;
    private Bundle savInState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        int oreInt = prefs.getInt("orarioOre", 100);
        int minutiInt = prefs.getInt("orarioMin", 100);

        //setto navigation bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //seleziono subito un item
        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_home);
        }
        savInState = savedInstanceState; //cosi la passo nell'onStart

        fragmentManager = getSupportFragmentManager();
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build(); //.allowMainThreadQueries()

        //stampa parametro monitorato
        String parametroInserito = prefs.getString("paramtroInserito", "nessun valore selezionato");
        //oast.makeText(getApplicationContext(), "Parametro in osservazione: " + parametroInserito, Toast.LENGTH_LONG).show();T


        //setto la notifica solo se l'utente non l'ha settata
        String checkNotificaSettataDaUtente = prefs.getString("notifUserSetted", "non settata");
        if(oreInt == 100 && minutiInt == 100 ){ //se sono 100, non è stata settata
            setUpNotification(10, 24);
            Toast.makeText(getApplicationContext(), "MINUTI: "+ minutiInt, Toast.LENGTH_LONG).show();


        }else{
            setUpNotification(oreInt, minutiInt);
            Toast.makeText(getApplicationContext(), "La notifica arriverà alle "+oreInt+":"+ minutiInt, Toast.LENGTH_LONG).show();

            //Toast.makeText(getApplicationContext(), "Orario in cui riceverai la notifica: " + orarioore + ":" + orariominn, Toast.LENGTH_LONG).show();

        }

        /*
        if (checkNotificaSettataDaUtente.equals("non settata")) {
            setUpNotification(16, 35);
        }*/

       //fragmentManager.beginTransaction().add(R.id.fragment_container, new HomeFragment()).commit();



    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                Intent intent = new Intent(getApplicationContext(), PaginaPrincipale.class);
                drawerLayout.closeDrawer(GravityCompat.START);//chiudo il menu, cosi quando torno indietro sarà chiuso
                startActivity(intent);
                finish();
                /*
                getFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                drawerLayout.closeDrawer(GravityCompat.START); //chiudo il side menu
                */

                break;
            case R.id.nav_area_personale:
                Intent intent4 = new Intent(getApplicationContext(), ActivityIniziale.class);
                // nuova versione
                intent4.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "personale");
                editor.apply();
                startActivity(intent4);
                finish();
                break;
            case R.id.nav_viewReport:
                getFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RecVediRep()).commit();
                drawerLayout.closeDrawer(GravityCompat.START); //chiudo il side menu
                break;
            case R.id.nav_graficoBarre:
                getFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Graf1()).commit();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_graficoTorta:
                getFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Graf2()).commit();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_settings:
                Intent intent2 = new Intent(getApplicationContext(), SettingsActivity.class);
                drawerLayout.closeDrawer(GravityCompat.START);//chiudo il menu, cosi quando torno indietro sarà chiuso
                startActivity(intent2);
                finish();

                break;
        }
        return true;
    }

    // metodo che lancia la notifica passando i parametri: ore:minuti
    // qualora l'utente li abbia inseriti, se no gli passa i parametri
    // che stabilisco io di default
    public void setUpNotification(int oreInt, int minutiInt) {
        SharedPreferences prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("orarioOre", oreInt);
        editor.apply();
        editor.putInt("orarioMin", minutiInt);
        editor.apply();

        Calendar sceltaOrarioCalendario = Calendar.getInstance(); //mi servirà per trovare l'orario corretto
        sceltaOrarioCalendario.set(Calendar.HOUR_OF_DAY, oreInt);
        sceltaOrarioCalendario.set(Calendar.MINUTE, minutiInt);
        sceltaOrarioCalendario.set(Calendar.SECOND, 0); //non faccio settare i secondi all'utente
        //Toast.makeText(getApplicationContext(), "Orario in cui riceverai la notifica: " + oreInt + ":" + minutiInt, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, NoticationBroadcastreceiver.class);
        intent.setAction("MY_NOTIFICATION_MESSAGE");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //considero anche se il device va in sleep mode
        //come secondo parametro gli passo la data/ora scelta dall'utente
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, sceltaOrarioCalendario.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
       // Log.d("BACK ", count + "");
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (count == 0) {
            super.onBackPressed(); //finish
        } else if(count > 0){
            //simula il back
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (findViewById(R.id.fragment_container) != null) {
            if (savInState != null) {
                return;
            }
            Intent fromNotify = getIntent();
            boolean check = fromNotify.getBooleanExtra("fromNotify", false);


            boolean apriMenuConNuovaModalita = fromNotify.getBooleanExtra("nuovoMenuLaterale", false);
            if(apriMenuConNuovaModalita){
                prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
                String voceMenuDaAprire = prefs.getString("voceMenuDaAprire", "nonSelezionata");

                if(voceMenuDaAprire.equals("home")){
                    getFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                //altre voci menu
                if(voceMenuDaAprire.equals("viewReport")){
                    getFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RecVediRep()).commit();
                    drawerLayout.closeDrawer(GravityCompat.START); //chiudo il side menu
                }
                if(voceMenuDaAprire.equals("graficoBarre")){
                    getFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Graf1()).commit();
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                if(voceMenuDaAprire.equals("graficoTorta")){
                    getFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Graf2()).commit();
                    drawerLayout.closeDrawer(GravityCompat.START);
                }




                // ELSE ... per gestire l'apertura dell'app classica o proveniente dal click sulla notifica
            }else if(check){ //se clicco sulla notifica, mando l'utente ad aggiungere un report
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AggiungiDatoUtenteFragment()).commit();
            }else{//caso normale: l'utente apre l'app e si trova sull'home fragment
                fragmentManager.beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
            }





        }
    }
}
