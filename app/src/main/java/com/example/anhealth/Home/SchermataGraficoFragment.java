package com.example.anhealth.Home;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.ValoriIstanze;
import com.example.anhealth.Graf1;
import com.example.anhealth.Graf2;
import com.example.anhealth.R;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;


public class SchermataGraficoFragment extends Fragment {

    private Bundle savedInstanceState;
    private SharedPreferences prefs;
    private LineChart mChart;
    private View view;

    public SchermataGraficoFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_schermata_grafico, container, false);
        prefs = getActivity().getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);




        mChart = view.findViewById(R.id.line_chart);
        mChart.setTouchEnabled(true);
        mChart.setPinchZoom(true);
        mChart.getAxisRight().setDrawGridLines(false);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.animateX(700);
        mChart.getAxisLeft().setTextColor(getResources().getColor(R.color.white));

        //mChart.getAxisLeft().getTextSize(13);

        //mChart.getAxisRight().setTextColor(Color.argb(1,255,255,255));
        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextColor(getResources().getColor(R.color.white));//colore asse delle x top
        xAxis.setTextSize(10);
        xAxis.setAxisLineColor(getResources().getColor(R.color.white));
        String nome = "nome";
        xAxis.setLabelRotationAngle(-90);
        xAxis.setGranularity(1f);//rendo i valori, sull'asse delle ascisse, interi


        final ArrayList<String> xAxes = new ArrayList<>();
        for (int i=0; i < 10; i++) {
            xAxes.add(i, String.valueOf(i) + "_" + i); //Dynamic x-axis labels
        }

        YAxis rightYAxis = mChart.getAxisRight();

        rightYAxis.setEnabled(false);
        //XAxis xAxis = mChart.getXAxis();

        YAxis leftYAxis = mChart.getAxisLeft();
        leftYAxis.setEnabled(true);

        leftYAxis.setAxisLineColor(getResources().getColor(R.color.white));

        leftYAxis.setTextSize(10);
        String valSceSpinner = prefs.getString("valSceltSens","PM10");

        mChart.getDescription().setText(valSceSpinner);


        return view;
    }


    public void mostraDati(ArrayList<Entry> values){

        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {

            set1 = new LineDataSet(values, "");
            set1.setDrawIcons(false);
            set1.setValueTextSize(11);
//            set1.setValueTextColor(getResources().getColor(R.color.white));
  //          set1.setColors(getResources().getColor(R.color.design_default_color_secondary));

            set1.setCircleColor(Color.DKGRAY);
            set1.setLineWidth(4f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setDrawFilled(true);
            set1.setCircleColor(R.color.coloreMenuHeader);

            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_blue);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            mChart.setData(data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        String valSceltoSpinner = prefs.getString("valSceltSens","PM10");
        Log.d("grafico3", "valSceltoSpinner: "+valSceltoSpinner);
        new AsyncTaskGetValoriIstanza2().execute(valSceltoSpinner); //ottengo i valori delle istanze, e nell'on post disegno il grafico
    }


    ArrayList<Entry> valoriGrafico;
    private List<ValoriIstanze> valoriIstanze = new ArrayList<ValoriIstanze>();
    public class AsyncTaskGetValoriIstanza2 extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            valoriIstanze = PaginaPrincipale.appDatabase.daoValoriIstanze().getValoreIstanza(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "Istanze caricate",
            //Toast.LENGTH_SHORT).show();
            Log.d("PaginaPrincipale", "Istanze caricate");
            valoriGrafico = new ArrayList<>();
            //List<String> xAxisValues = new ArrayList<>(Arrays.asList("Jan", "Feb", "March", "April", "May", "June","July", "August", "September", "October", "November", "Decemeber"));
            List<String> xAxisValues = new ArrayList<String>();
            if(!valoriIstanze.isEmpty()){ //se ci sono dei valori, mostro il grafico
                Log.d("test", "sono qui: ");
                int i = 0;
                for(ValoriIstanze val:valoriIstanze){
                    Log.d("PaginaPrincipale", "onPostExecute: "+valoriIstanze.get(i).getValoreOttenuto());
                    valoriGrafico.add(new Entry(i, Float.parseFloat(String.valueOf(valoriIstanze.get(i).getValoreOttenuto()))));
                    xAxisValues.add(valoriIstanze.get(i).getDataInserimento());
                    i++;
                }

            }

            mChart.getXAxis().setValueFormatter(new com.github.mikephil.charting.formatter.IndexAxisValueFormatter(xAxisValues));

            mostraDati(valoriGrafico);


        }
    }

}

class FooFormatter extends ValueFormatter {
    private long referenceTimestamp; // minimum timestamp in your data set
    private DateFormat mDataFormat;
    private Date mDate;

    public FooFormatter(long referenceTimestamp) {
        this.referenceTimestamp = referenceTimestamp;
        this.mDataFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.mDate = new Date();
    }

    @Override
    public String getFormattedValue(float value) {
        // convertedTimestamp = originalTimestamp - referenceTimestamp
        long convertedTimestamp = (long) value;

        // Retrieve original timestamp
        long originalTimestamp = referenceTimestamp + convertedTimestamp;

        // Convert timestamp to hour:minute
        return getDateString(originalTimestamp);
    }

    private String getDateString(long timestamp) {
        try {
            mDate.setTime(timestamp);
            return mDataFormat.format(mDate);
        } catch(Exception ex) {
            return "xx";
        }
    }
}



