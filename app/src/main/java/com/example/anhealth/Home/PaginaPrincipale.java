package com.example.anhealth.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.ActivityIniziale;
import com.example.anhealth.AggiungiDatoUtenteFragment;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.Database.ValoriIstanze;
import com.example.anhealth.Graf1;
import com.example.anhealth.Graf2;
import com.example.anhealth.MainActivity;
import com.example.anhealth.MostraTemplateCostruiti.MappaTuoiTemplate;
import com.example.anhealth.R;
import com.example.anhealth.RecVediRep;
import com.example.anhealth.SettingsActivity;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class PaginaPrincipale extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    public static AppDatabase appDatabase;
    private Button bottoneSalute, bottoneLocale, bottoneSensquare, btnOk, restartValues;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SharedPreferences prefs;
    private AppCompatSpinner spinner;
    private ArrayAdapter arrayAdapter1;
    private List<String> tipiDiParametro = new ArrayList<String>();
    private String urlSecondaPagina = "";
    private Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstanceState = savedInstanceState;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_principale);
        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();
        //new AsyncTaskGetValoriIstanza().execute("PM10"); //ottengo i valori delle istanze, e nell'on post disegno il grafico

        this.savedInstanceState = savedInstanceState;


        Toolbar toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);

        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //seleziono subito un item
        if (savedInstanceState == null) {
            navigationView.setCheckedItem(R.id.nav_home);
        }

        bottoneSalute = findViewById(R.id.bottoneSalute);
        bottoneLocale = findViewById(R.id.bottoneLocale);

        bottoneSalute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        bottoneLocale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityIniziale.class);
                startActivity(intent);
            }
        });


        spinner = findViewById(R.id.parSenSpin);



        String url = "http://sensquare.disi.unibo.it/api/v1/showDataClasses";
        avviaAPIDataClasses(url);


        //Log.d("PaginaPrincipale", "spinner.getSelectedItem(): "+spinner.getSelectedItem().toString());

        btnOk = findViewById(R.id.idBtnOk);
        restartValues = findViewById(R.id.restartValues);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinner.getSelectedItem() == null){
                    Log.d("PaginaPrincipale", "spinner null ");
                    //spinner.performClick();

                }else{
                    Log.d("PaginaPrincipale", "spinner.getSelectedItem(): "+spinner.getSelectedItem().toString());
                    if(spinner.getSelectedItem().toString().length() != 0){
                        String valScelto = spinner.getSelectedItem().toString();
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("valSceltSens", valScelto);
                        Log.d("PaginaPrincipale", "val scelto spinner: "+valScelto);
                        editor.apply();
                        //getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_home, new SchermataGraficoFragment()).commit();
                        onStart();
                    }

                }

            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("PaginaPrincipale", "spinner.getSelectedItem(): "+spinner.getSelectedItem().toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        restartValues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTaskSvuotaTabella().execute();
            }
        });


    }


    private Context mContext;


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        /*
         *  |------------------------------------------------------------------------------------|
         *  |------- LA GESTIONE DEL MENU AVVIENE IN MODO DIFFERENTE DALLA MAIN ACTIVITY --------|
         *
         *   IN QUESTO CASO SETTO UNA VARIABILE PUTEXTRA E POI LANCIO LA MAINACTIVITY
         *   NELLO ONSTART DELLA MAIN ACTIVITY, IN BASE AL VALORE DELLA VARIABILE PUTEXTRA
         *   SI APRIRA LA VOCE CORRISPONDENDE DEL MENU
         *
         *  |-----------------------------------------------------------------------------------|
         *
         * */
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                /*
                Intent intent2 = new Intent(getApplicationContext(), PaginaPrincipale.class);
                // nuova versione
                intent2.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "home");
                editor.apply();
                startActivity(intent2);
                finish();*/
                break;

            case R.id.nav_viewReport:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "viewReport");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_area_personale:
                Intent intent2 = new Intent(getApplicationContext(), ActivityIniziale.class);
                // nuova versione
                intent2.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "personale");
                editor.apply();
                startActivity(intent2);
                finish();
                break;
            case R.id.nav_salute:
                // nuova versione
                intent.putExtra("nuovoMenuLaterale",true); //se è settato a true vorrà dire che l'onstart di mainactivity aprirà il valore settato nelle shared preferences
                //la main activity dopo aver aperto la voce del menu laterale indicata nelle shared, risetterà questa variabile "nuovoMenuLaterale" a false.
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
                //setto la voce del menu
                editor.putString("voceMenuDaAprire", "home");
                editor.apply();
                startActivity(intent);
                finish();
                break;


            case R.id.nav_graficoBarre:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "graficoBarre");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_graficoTorta:
                intent.putExtra("nuovoMenuLaterale",true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                editor.putString("voceMenuDaAprire", "graficoTorta");
                editor.apply();
                startActivity(intent);
                finish();
                break;
            case R.id.nav_settings: //in questo caso aprò direttamente un activity e non un fragment, non ho bisogno dei putextra
                Intent intentSettings = new Intent(getApplicationContext(), SettingsActivity.class);
                drawerLayout.closeDrawer(GravityCompat.START);//chiudo il menu, cosi quando torno indietro sarà chiuso
                startActivity(intentSettings);
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (count == 0) {
            super.onBackPressed(); //finish
        } else if(count > 0){
            //simula il back
            getFragmentManager().popBackStack();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_graf_home, new SchermataGraficoFragment()).commit();

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }


    public void avviaAPIDataClasses(String urlPaginaDaMostrare){ //metodo che recupera da sensquare i tipi di dato (dataclasses)
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                urlPaginaDaMostrare,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            for(int i = 0; i<jsonArray.length(); i++) {
                                JSONObject results = jsonArray.getJSONObject(i);
                                String id = results.getString("id");
                                String dataType = results.getString("data_type");
                                tipiDiParametro.add(id);
                            }

                            //if(!response.getString("next").equals("")){ // se non è vuoto..
                            if(!response.get("next").equals(null)){
                                String urlPaginaSuccessiva = response.getString("next");
                                urlSecondaPagina = urlPaginaSuccessiva;

                                avviaAPIDataClasses(urlSecondaPagina); //qui l'url sarà diverso dal primo, infatti conterrà le indicazioni per la pagina successiva da caricare
                            }


                            arrayAdapter1 = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, tipiDiParametro);
                            arrayAdapter1.notifyDataSetChanged();

                            spinner.setAdapter(arrayAdapter1);

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());
                    }
                }
        );
        queue.add(objectRequest);
    }


    public class AsyncTaskSvuotaTabella extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            appDatabase.daoValoriIstanze().svuotaTabella();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("PaginaPrincipale", "Tabella svuotata");
            onStart();

        }
    }


}
