package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoMisurazioniSalute {
    @Query("select * from misuarazioni_salute")
    public List<MisurazioniSalute> getMeasurements();
    @Insert
    public void aggiungiMisurazione(MisurazioniSalute misurazione);

    @Query("select * from misuarazioni_salute where data_class = :nomeParametro")
    public List<MisurazioniSalute> getMisurazioniSaluteFiltrate(String nomeParametro); //serve per vericare la presenza o meno di un certo parametro esempio del BATT

    //query che mi recupera l'ultimo valore inserito per un determinato tipo di parametro
    @Query("select * from misuarazioni_salute where data_class = :nomeParametro_inserito order by id DESC limit 1")
    public List<MisurazioniSalute> getUltimoValoreInserito(String nomeParametro_inserito);


}
