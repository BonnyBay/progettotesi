package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DaoCustomServiceLocal {
    @Query("select * from custom_service_local")
    public List<CustomServiceLocal> getInstanza();
    //ottengo una sola instanza sulla base dell'id ricevuto come paramtero
    @Query("select * from custom_service_local where id = :idTuaIstanza")
    public CustomServiceLocal getIstanzaFiltrata(int idTuaIstanza);

    @Insert
    public void aggiungiInstanza(CustomServiceLocal instanzaCreata);

    //@Update
    //void aggiornaReport(CustomServiceLocal instanzaCreata);

    @Query("UPDATE custom_service_local SET stato_istanza=:text WHERE id=:idInserito")
    void aggiornaStatoIstanza (String text, int idInserito);

}
