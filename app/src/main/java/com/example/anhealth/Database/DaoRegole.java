package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoRegole {
    @Insert
    public void aggiungiRegola(Regola regola);

    @Query("select * from regole")
    public List<Regola> getRegole();

    //ottengo le regole sulla base dell'id che gli passo come parametro
    @Query("select * from regole where template_id = :templateIdPassato")
    public List<Regola> getRegolaConFiltro(long templateIdPassato); // potrebbe ritornarmi piu di una regola

    @Delete
    void rimuoviRegola(Regola regola);
}
