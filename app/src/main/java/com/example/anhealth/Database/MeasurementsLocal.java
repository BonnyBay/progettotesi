package com.example.anhealth.Database;




import androidx.room.ColumnInfo;
        import androidx.room.Embedded;
        import androidx.room.Entity;
        import androidx.room.ForeignKey;
        import androidx.room.PrimaryKey;
        import androidx.room.TypeConverters;

        import java.util.ArrayList;
        import java.util.Date;
@Entity(tableName = "measurementsLocal")

//setto i valori (tabelle)
//classe che puo' essere usata per salvare i valori provenienti da sensquare
public class MeasurementsLocal {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "data_stream_id")
    public int data_stream_id;

    @ColumnInfo(name = "gps_latitude")
    public float gps_latitude;

    @ColumnInfo(name = "gps_longitude")
    public float gps_longitude;

    @ColumnInfo(name = "value")
    public double value;

    public int getId() {
        return id;
    }

    public float getGps_latitude() {
        return gps_latitude;
    }

    public float getGps_longitude() {
        return gps_longitude;
    }

    public int getData_stream_id() {
        return data_stream_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData_stream_id(int data_stream_id) {
        this.data_stream_id = data_stream_id;
    }

    public void setGps_latitude(float gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public void setGps_longitude(float gps_longitude) {
        this.gps_longitude = gps_longitude;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}


