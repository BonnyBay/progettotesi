package com.example.anhealth.Database;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "custom_service_template_local")
public class CustomServiceTemplateLocal {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="template_id")
    private long id;

    @ColumnInfo(name = "titolo")
    public String titolo;


    public long getId() {
        return id;
    }


    public String getTitolo() {
        return titolo;
    }

    public void setId(long id) {
        this.id = id;
    }


    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }


}
