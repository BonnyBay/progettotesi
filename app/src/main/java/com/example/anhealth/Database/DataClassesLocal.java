package com.example.anhealth.Database;




import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.ArrayList;
import java.util.Date;
//setto i valori (tabelle)
@Entity(tableName = "data_classes_local")
public class DataClassesLocal {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "data_type") // mi immagino come data type: battito cardiaco, temperatura ...
    public String data_type;

    @ColumnInfo(name = "unit_of_measure")
    public String unit_of_measure;

    public long getId() {
        return id;
    }

    public String getUnit_of_measure() {
        return unit_of_measure;
    }

    public String getData_type() {
        return data_type;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public void setUnit_of_measure(String unit_of_measure) {
        this.unit_of_measure = unit_of_measure;
    }
}


