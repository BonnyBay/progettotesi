package com.example.anhealth.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

//classe che rappresenta il Database
//@Database(entities = {DatoUtente.class}, version = 1)

@Database(entities ={
        DatoUtente.class,
        MeasurementsLocal.class,
        DataClassesLocal.class,
        CustomServiceTemplateLocal.class,
        Regola.class,
        CustomServiceLocal.class,
        MisurazioniSalute.class,
        ValoriIstanze.class,
        IstanzeAttiveSensquare.class
        },
        version = 1)
@TypeConverters({Converters.class})

public abstract class AppDatabase extends RoomDatabase {
    public abstract DataAccessObject dataAccessObject();
    public abstract DaoMeasurements daoMeasurements();
    public abstract DaoDataClassesLocal daoDataClassesLocal();
    public abstract DaoCustomServiceTemplateLocal daoCustomServiceTemplateLocal();
    public abstract DaoCustomServiceLocal daoCustomServiceLocal();
    public abstract DaoRegole daoRegole();
    public abstract DaoMisurazioniSalute daoMisurazioniSalute();
    public abstract DaoValoriIstanze daoValoriIstanze();
    public abstract DaoIstanzeAttiveSensquare daoIstanzeAttiveSensquare();


}
