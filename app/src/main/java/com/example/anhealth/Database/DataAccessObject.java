package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import java.util.List;
//prende le entità dal DB, serve per accedere al database
@Dao
public interface DataAccessObject {
    @Insert
    public void aggiungiDatoUtente(DatoUtente datoUtente);
    @Update
    void aggiornaReport(DatoUtente datoUtente);

    @Delete
    void rimuoviReport(DatoUtente datoUtente);

    @Query("select * from datiUtente") //datiUtente è il nome della tabella
    public List<DatoUtente> getDati();

    //filtro i dati
    @Query("select * from datiUtente where prioritaReport = :prio")
    public List<DatoUtente> getDatiFiltrati(int prio);


}

