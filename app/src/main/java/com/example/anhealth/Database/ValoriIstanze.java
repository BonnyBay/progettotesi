package com.example.anhealth.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "valori_istanze")
public class ValoriIstanze { //tabella che contiene i valori prodotti per ogni parametro ogni volta che si istanzia un template

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id_valore")
    private long id;

    @ColumnInfo(name = "nome_parametro")
    public String nomeParametro;

    @ColumnInfo(name = "valore_ottenuto")
    public Double valoreOttenuto;

    @ColumnInfo(name = "data_inserimento")
    private String dataInserimento;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDataInserimento(String dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public void setNomeParametro(String nomeParametro) {
        this.nomeParametro = nomeParametro;
    }

    public void setValoreOttenuto(Double valoreOttenuto) {
        this.valoreOttenuto = valoreOttenuto;
    }

    public String getDataInserimento() {
        return dataInserimento;
    }

    public String getNomeParametro() {
        return nomeParametro;
    }

    public Double getValoreOttenuto() {
        return valoreOttenuto;
    }
}
