package com.example.anhealth.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


//tabella utile a tener traccia degli stati delle istanze di sensquare
@Entity(tableName = "istanze_attive_sensquare")
public class IstanzeAttiveSensquare {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "id_istanza")
    public int idIstanza;

    @ColumnInfo(name = "stato")
    public String statoIstanza;

    public String getStatoIstanza() {
        return statoIstanza;
    }

    public int getId() {
        return id;
    }

    public int getIdIstanza() {
        return idIstanza;
    }

    public void setStatoIstanza(String statoIstanza) {
        this.statoIstanza = statoIstanza;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdIstanza(int idIstanza) {
        this.idIstanza = idIstanza;
    }
}
