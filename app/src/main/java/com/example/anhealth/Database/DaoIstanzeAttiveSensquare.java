package com.example.anhealth.Database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoIstanzeAttiveSensquare {
    @Query("select * from istanze_attive_sensquare")
    public List<IstanzeAttiveSensquare> getIstanzeAttive();

    @Query("DELETE FROM istanze_attive_sensquare where id_istanza=:idIstanzaDarRimuovere") //rimuovo l'istanza quando viene disattivata
    public void rimuoviIstanza(int idIstanzaDarRimuovere);

    @Query("select * from istanze_attive_sensquare where id_istanza = :idTuaIstanza") //mi serve per recuperare lo stato di quell'istanza, e vedere che valore ha: attiva o meno
    public IstanzeAttiveSensquare getIstanzaSensquareFiltrata(int idTuaIstanza);

    @Query("UPDATE istanze_attive_sensquare SET stato=:text WHERE id=:idInserito")
    void aggiornaStatoIstanzaSensquare (String text, int idInserito);

    @Insert
    public void aggiungiInstanzaSensquare(IstanzeAttiveSensquare istanzaSensquare);


}
