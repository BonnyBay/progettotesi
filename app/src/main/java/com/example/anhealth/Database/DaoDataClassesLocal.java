package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoDataClassesLocal {
    @Query("select * from data_classes_local")
    public List<DataClassesLocal> getDataClassesLocal();

    @Insert
    public void aggiungiDataClasses(DataClassesLocal data_class_added);
}
