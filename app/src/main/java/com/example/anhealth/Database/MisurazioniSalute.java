package com.example.anhealth.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "misuarazioni_salute")
public class MisurazioniSalute {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "data_class")
    public String data_class;

    @ColumnInfo(name = "value")
    public double valore;

    public double getValore() {
        return valore;
    }

    public int getId() {
        return id;
    }

    public String getData_class() {
        return data_class;
    }

    public void setValore(double valore) {
        this.valore = valore;
    }

    public void setData_class(String data_class) {
        this.data_class = data_class;
    }

    public void setId(int id) {
        this.id = id;
    }
}
