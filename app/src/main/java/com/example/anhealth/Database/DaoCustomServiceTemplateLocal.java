package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoCustomServiceTemplateLocal {
    @Query("select * from custom_service_template_local")
    public List<CustomServiceTemplateLocal> getTemplates();
    @Insert
    public void aggiungiTemplate(CustomServiceTemplateLocal templateCostruito);
}
