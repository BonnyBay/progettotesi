package com.example.anhealth.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "regole")
public class Regola {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="regola_id")
    private long id;

    @ColumnInfo(name = "parametro")
    public String parametro;

    @ColumnInfo(name = "operatorePrimo")
    public String operatorePrimo;

    @ColumnInfo(name = "operatoreSecondo")
    public String operatoreSecondo;

    @ColumnInfo(name = "costante")
    public double costante;

    @ColumnInfo(name = "template_id")
    public long template_id; //id del template al quale fa riferimento questa regola

    public long getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(long template_id) {
        this.template_id = template_id;
    }


    public long getId() {
        return id;
    }

    public String getParametro() {
        return parametro;
    }

    public String getOperatorePrimo() {
        return operatorePrimo;
    }

    public String getOperatoreSecondo() {
        return operatoreSecondo;
    }

    public double getCostante() {
        return costante;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public void setOperatorePrimo(String operatorePrimo) {
        this.operatorePrimo = operatorePrimo;
    }

    public void setOperatoreSecondo(String operatoreSecondo) {
        this.operatoreSecondo = operatoreSecondo;
    }

    public void setCostante(double costante) {
        this.costante = costante;
    }
}
