package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoMeasurements {
    @Query("select * from measurementsLocal")
    public List<MeasurementsLocal> getMeasurements();
    @Insert
    public void aggiungiMisurazione(MeasurementsLocal misurazione);

}
