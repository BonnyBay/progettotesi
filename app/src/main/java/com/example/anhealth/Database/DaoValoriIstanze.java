package com.example.anhealth.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DaoValoriIstanze {

    @Query("select * from valori_istanze where nome_parametro = :nomePar") //recupero tutti i valori di un determinato parametro
    public List<ValoriIstanze> getValoreIstanza(String nomePar);
    @Insert
    public void aggiungiValoreIstanza(ValoriIstanze valoriIstanze);

    @Query("DELETE FROM valori_istanze") //rimuovo tutti i valori
    public void svuotaTabella();


}
