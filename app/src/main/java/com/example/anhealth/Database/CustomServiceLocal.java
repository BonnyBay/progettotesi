package com.example.anhealth.Database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "custom_service_local")

public class CustomServiceLocal {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "titolo_instanza")
    public String titolo;

    @ColumnInfo(name = "gps_latitude_instanza")
    public double gps_latitude;

    @ColumnInfo(name = "gps_longitude_instanza")
    public double gps_longitude;

    @ColumnInfo(name = "deployment_radius")
    public double deployment_radius;

    @ColumnInfo(name = "descrizione")
    public String descrizione;

    @ColumnInfo(name = "args") //template id
    public String args;

    @ColumnInfo(name = "struttura_template")
    public String struttura_template;


    @ColumnInfo(name = "data_stream_associato") //contiene tutti i data stream dai quali prelevare le info
    public String id_data_stream_utili;

    @ColumnInfo(name = "nomi_parametri") //nomi dei parametri (data_class) che derivano da sensquare
    public String nomiParametri;

    @ColumnInfo(name = "nomi_parametri_locali") //per esempio battito cardiaco (BATT), ...
    public String nomiParametriLocali;

    @ColumnInfo(name = "stato_istanza") //per capire se l'istanza è attiva o non attiva
    public String statoIstanza;

    public int getId() {
        return id;
    }

    public String getTitolo() {
        return titolo;
    }

    public double getGps_longitude() {
        return gps_longitude;
    }

    public double getGps_latitude() {
        return gps_latitude;
    }

    public void setGps_longitude(double gps_longitude) {
        this.gps_longitude = gps_longitude;
    }

    public void setGps_latitude(double gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getDeployment_radius() {
        return deployment_radius;
    }

    public void setDeployment_radius(double deployment_radius) {
        this.deployment_radius = deployment_radius;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public void setId_data_stream_utili(String id_data_stream_utili) {
        this.id_data_stream_utili = id_data_stream_utili;
    }

    public String getId_data_stream_utili() {
        return id_data_stream_utili;
    }

    public String getStruttura_template() {
        return struttura_template;
    }

    public void setStruttura_template(String struttura_template) {
        this.struttura_template = struttura_template;
    }

    public void setNomiParametri(String nomiParametri) {
        this.nomiParametri = nomiParametri;
    }

    public String getNomiParametri() {
        return nomiParametri;
    }

    public void setNomiParametriLocali(String nomiParametriLocali) {
        this.nomiParametriLocali = nomiParametriLocali;
    }

    public String getNomiParametriLocali() {
        return nomiParametriLocali;
    }

    public String getStatoIstanza() {
        return statoIstanza;
    }
    public void setStatoIstanza(String statoIstanza) {
        this.statoIstanza = statoIstanza;
    }
}
