package com.example.anhealth;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.Database.DatoUtente;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {
    Context context;
    private List<DatoUtente> mDatoUtente;
    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView info, pMin, pMax, tempCorp, batt, id, priorita;
        public Button immagineElimina, btnModifica;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.idReport);
            pMin = itemView.findViewById(R.id.presMin);
            pMax = itemView.findViewById(R.id.presMax);
            tempCorp = itemView.findViewById(R.id.tempRep);
            batt = itemView.findViewById(R.id.batRep);
            info = itemView.findViewById(R.id.notRep);
            priorita = itemView.findViewById(R.id.priorita);

            immagineElimina = itemView.findViewById(R.id.imgElimina);
            btnModifica = itemView.findViewById(R.id.btnModifica);
            immagineElimina.setOnClickListener(this);
            btnModifica.setOnClickListener(this);

            immagineElimina.setBackgroundResource(R.drawable.ic_elimina); //setto l'icona del cestino su "elimina"
            btnModifica.setBackgroundResource(R.drawable.ic_edit_edit);
        }

        @Override
        public void onClick(View v) {
            DatoUtente report = new DatoUtente();
            //gestisco su quale bottoe ho cliccato
            if (v.getId() == R.id.imgElimina) {
                long iD = mDatoUtente.get(getAdapterPosition()).getId();
                report.setId(iD);
                new AsyncTaskRimuoviReport().execute(report);
            } else if (v.getId() == R.id.btnModifica) {
                long iD = mDatoUtente.get(getAdapterPosition()).getId();
                //report.setId(iD);
                //passo l'id al fragment che avrà il compito di aggiornare
                //serve in stringa per metterlo nel Bundle
                String idString = Long.toString(iD);
                Bundle args = new Bundle();
                args.putString("ID", idString);
                Fragment fr = new AggiornaReport();
                fr.setArguments(args);
                //passo il valore ID al fragment
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, fr).addToBackStack(null).commit();
            }
        }
    }

    public MyAdapter(List<DatoUtente> datoUtente, Context context) { //il context mi serve nel onBindViewHolder per le shared preference
        mDatoUtente = datoUtente;
        notifyDataSetChanged();
        this.context = context;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.riga, parent, false);

        MyHolder myHolder = new MyHolder(v);

        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        //mostro i dati all'interno di ogni riga
        holder.id.setText(String.valueOf(mDatoUtente.get(position).getId()));
        holder.pMin.setText(String.valueOf(mDatoUtente.get(position).getPressioneMinArteriosa().getValore()));
        holder.pMax.setText(String.valueOf(mDatoUtente.get(position).getPressioneMaxArteriosa().getValore()));
        holder.tempCorp.setText(String.valueOf(mDatoUtente.get(position).getTemperatura().getValore()));
        holder.batt.setText(String.valueOf(mDatoUtente.get(position).getBattitoCardiaco().getValore()));
        holder.info.setText(String.valueOf(mDatoUtente.get(position).getNote()));

        //priorità
        int prioBatt = mDatoUtente.get(position).getBattitoCardiaco().getPriorità();
        int prioPMin = mDatoUtente.get(position).getPressioneMinArteriosa().getPriorità();
        int prioPMax = mDatoUtente.get(position).getPressioneMaxArteriosa().getPriorità();
        int prioTemp = mDatoUtente.get(position).getTemperatura().getPriorità();

        //calcolop il massimo della priorità
        //int massimaPriorita = maxPriority(prioBatt, prioPMin, prioPMax, prioTemp, position); //position mio serve per recuperare la riga corrente
        //stampo quella maggiore
        holder.priorita.setText(String.valueOf(mDatoUtente.get(position).getPrioritaReport()));

    }

    @Override
    public int getItemCount() {
        return mDatoUtente.size();
    }

    //metodo che calcola il massimo della priorità
    public int maxPriority(int prioBatt, int prioPMin, int prioPMax, int prioTemp, int position) {
        int max = 0;
        if ((prioBatt > max) && (mDatoUtente.get(position).getBattitoCardiaco().getValore() != 0.0)) { //controllo che la priorià sia la maggiore di tutte e che il campo non sia vuoto e quindi non sia 0.0
            max = prioBatt;
        }
        if ((prioPMin > max) && (mDatoUtente.get(position).getPressioneMinArteriosa().getValore() != 0.0)) {
            max = prioPMin;
        }
        if ((prioPMax > max) && (mDatoUtente.get(position).getPressioneMaxArteriosa().getValore() != 0.0)) {
            max = prioPMax;
        }
        if ((prioTemp > max) && (mDatoUtente.get(position).getTemperatura().getValore() != 0.0)) {
            max = prioTemp;
        }
        return max;
    }


    public class AsyncTaskRimuoviReport extends AsyncTask<DatoUtente, Void, Void> {

        @Override
        protected Void doInBackground(DatoUtente... datoUtentes) {
            MainActivity.appDatabase.dataAccessObject().rimuoviReport(datoUtentes[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(context, "Report rimosso",
                    Toast.LENGTH_LONG).show();
            MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new RecVediRep(), null).commit();//non lo aggiungo allo stack

        }
    }

}
