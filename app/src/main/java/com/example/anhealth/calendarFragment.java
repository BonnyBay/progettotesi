package com.example.anhealth;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.DatoUtente;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class calendarFragment extends Fragment {
    private TextView txtReport;
    private CalendarView calendario;
    private TextView dataOdierana;
    public static FragmentManager fragmentManager;
    public static AppDatabase appDatabase;
    private Button apriDialogReport;
    private Button aggiungiReport, reportRiassuntivo;
    private AggiungiDatoUtenteFragment nextFrag = new AggiungiDatoUtenteFragment();
    private Double battitoMedia = 0.0, pMinMedia = 0.0, pMaxMedia = 0.0, tempMedia = 0.0;
    private int contaBatt = 0, contaPMin = 0, contaPMax = 0, contaTemp = 0; //var che contano il numero di campi inseriti per ciascun parametro
    private List<DatoUtente> datoUtente;
    int year = 0, month = 0, dayOfMonth = 0;
    public calendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        reportRiassuntivo = view.findViewById(R.id.reportRiassuntivo); //altro dialog
        apriDialogReport = view.findViewById(R.id.apriDialogReport); //dialog
        calendario = view.findViewById(R.id.calendario1);
        dataOdierana = view.findViewById(R.id.dataOggi1);
        calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int yearCalendar, int monthCalendar, int dayOfMonthCalendar) {
                year = yearCalendar;
                month = monthCalendar;
                dayOfMonth = dayOfMonthCalendar;
                new AsyncTaskGetDatiForCalendario().execute();
            }
        });
        contaPMin = 0;
        contaBatt = 0;
        contaPMax = 0;
        contaTemp = 0;

        return view;
    }


    public void openDialog(ArrayList<String> data) {
        DialogVediReport dialogVediReport = new DialogVediReport(data);
        dialogVediReport.show(getFragmentManager(), "Report informazioni");
    }

    public void mostraDati(int year, int month, int dayOfMonth){
        Date data = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(data);
        final ArrayList<String> vettoreInfo = new ArrayList<String>();
        String info = "";
        for (DatoUtente dato : datoUtente) {
            Long id = dato.getId();
            Double battitoCardiaco = dato.getBattitoCardiaco().getValore();
            Double temperatura = dato.getTemperatura().getValore();
            Double pressioneMinArteriosa = dato.getPressioneMinArteriosa().getValore();
            Double pressioneMaxArteriosa = dato.getPressioneMaxArteriosa().getValore();
            String note = dato.getNote();
            // cosi la data è nel formatp 00-00
            String datReport = dato.getDataInserimento();
            if (formattedDate.equals(dayOfMonth + "-" + (month + 1) + "-" + year)) {
                String dataAttuale = "Reports di oggi ";
                dataOdierana.setText(dataAttuale);
            } else {
                String dataAttuale = "Reports del " + dayOfMonth + "/" + month + "/" + year;
                dataOdierana.setText(dataAttuale);
            }
            // ho dovuto convertire da stringa a date a stringa per via della formattazione di default di dayofmonth/month/year
            String dataSelezionata = dayOfMonth + "-" + (month + 1) + "-" + year;
            Date startDate;
            try {
                startDate = df.parse(dataSelezionata);
                String newDateString = df.format(startDate);
                // month +1  perchè month è un array da 0 a 11
                if (datReport.equals(newDateString)) { //stampo solo i report con la data di oggi
                    info = info + ("\n\n" + "Report n°: " + id + "\n Battito cardiaco: " + battitoCardiaco + "\n Temperatura: " + temperatura + "\n Pressione minima: " + pressioneMinArteriosa + " \n Pressione massima: " + pressioneMaxArteriosa + "\n Data inserimento: " + datReport + "\n Note: " + note);
                    vettoreInfo.add(info);
                    //per il report riassuntivo mi calcolo i valori somma, volta per volta (poi farò la media fuori dal for)
                    //calcolo le medie solamente se i valori non sono 0.0, altrimenti invaliderei la media, quindi non sommerò il valore 0.0
                    // se c'è 0.0 vuol dire che quel dato non è stato inserito
                    if (battitoCardiaco != 0.0) {
                        battitoMedia = battitoMedia + battitoCardiaco;
                        //aggiungo un campo
                        contaBatt++;
                    }
                    if (temperatura != 0.0) {
                        tempMedia = tempMedia + temperatura;
                        contaTemp++;
                    }
                    if (pressioneMinArteriosa != 0.0) {
                        pMinMedia = pMinMedia + pressioneMinArteriosa;
                        contaPMin++;
                    }
                    if (pressioneMaxArteriosa != 0.0) {
                        pMaxMedia = pMaxMedia + pressioneMaxArteriosa;
                        contaPMax++;
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        apriDialogReport.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (!vettoreInfo.isEmpty()) {//controllo che non sia vuoto causa errore null
                    openDialog(vettoreInfo);
                } else {
                    Toast.makeText(getActivity(), "Nessun report da mostrare",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        final ArrayList<String> vettoreInfoSummary = new ArrayList<String>(); //vettore che contiene le medie dei valori
        if (!vettoreInfo.isEmpty()) { // se non è vuoto  ...
            //ora mi calcolo le medie effettive dei valori
            battitoMedia = battitoMedia / contaBatt;
            pMinMedia = pMinMedia / contaPMin;
            Log.d("PMin ", pMinMedia + "");
            Log.d("contaPMin ", "" + contaPMin);
            pMaxMedia = pMaxMedia / contaPMax;
            tempMedia = tempMedia / contaTemp;
            DecimalFormat formatDouble = new DecimalFormat("#.##");
            // le aggiungo ad un array list
            String infoSum = ("\n Battito medio: " + formatDouble.format(battitoMedia) + "\n Pressione minima media: " + formatDouble.format(pMinMedia) + "\n Pressione massima media: " + formatDouble.format(pMaxMedia) + "\n Temperatura corporea media: " + formatDouble.format(tempMedia));
            vettoreInfoSummary.add(infoSum);
            battitoMedia = 0.0;
            contaBatt = 0;
            pMinMedia = 0.0;
            contaPMin = 0;
            pMaxMedia = 0.0;
            contaPMax = 0;
            tempMedia = 0.0;
            contaTemp = 0;
        }

        reportRiassuntivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!vettoreInfo.isEmpty()) {//controllo che non sia vuoto causa errore null
                    openDialog(vettoreInfoSummary);
                } else {
                    Toast.makeText(getActivity(), "Nessun report riassuntivo da mostrare",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public class AsyncTaskGetDatiForCalendario extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            datoUtente = MainActivity.appDatabase.dataAccessObject().getDati();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mostraDati(year, month, dayOfMonth);
        }
    }
}


