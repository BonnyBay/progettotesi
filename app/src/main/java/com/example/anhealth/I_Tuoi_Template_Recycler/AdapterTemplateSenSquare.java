package com.example.anhealth.I_Tuoi_Template_Recycler;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.MainActivity;
import com.example.anhealth.MostraTemplateCostruiti.InstanzeTuoiTemplate.AdapterTueInstanze;
import com.example.anhealth.MyAdapter;
import com.example.anhealth.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTemplateSenSquare extends RecyclerView.Adapter<AdapterTemplateSenSquare.MyHolderTemplateSenSquare> {
    public ArrayList<String> mtitolo;
    public ArrayList<Integer> mId;
    public Context mContext;
    boolean mostraRemoti = false;
    private AdapterTueInstanze.OnNoteListener mOnNoteListener;
    public class MyHolderTemplateSenSquare extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titolo;
        AdapterTueInstanze.OnNoteListener mOnNoteListener;

        public MyHolderTemplateSenSquare(@NonNull View itemView, AdapterTueInstanze.OnNoteListener onNoteListener, boolean mostraRemoti) {
            super(itemView);
            if(mostraRemoti){
                titolo = itemView.findViewById(R.id.idTemplateTuoiText5);
            }else{
                titolo = itemView.findViewById(R.id.idTemplateTuoiText2);
            }
            mOnNoteListener = onNoteListener;
        }
        @Override
        public void onClick(View v) {
            mOnNoteListener.onItemClick(getAdapterPosition());
        }
    }

    public AdapterTemplateSenSquare(boolean mostraRemoti, ArrayList<String> titoloTem, ArrayList<Integer> id, Context context, AdapterTueInstanze.OnNoteListener mOnNoteListener) {
        this.mostraRemoti = mostraRemoti;
        mtitolo = titoloTem;
        mId = id;
        mContext = context;
        notifyDataSetChanged();
        this.mOnNoteListener = mOnNoteListener;

    }


    @NonNull
    @Override
    public MyHolderTemplateSenSquare onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(mostraRemoti){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedi_template_sensquare, parent, false);
            MyHolderTemplateSenSquare myHolder = new MyHolderTemplateSenSquare(v, mOnNoteListener, mostraRemoti);
            return myHolder;
        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedi_tuoi_template, parent, false);
            MyHolderTemplateSenSquare myHolder = new MyHolderTemplateSenSquare(v, mOnNoteListener, mostraRemoti);
            return myHolder;
        }


    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTemplateSenSquare.MyHolderTemplateSenSquare holder, int position) {
        String name = mtitolo.get(position);
        holder.titolo.setText(name);

    }


    @Override
    public int getItemCount() {
        return mtitolo.size();
    }
    public interface OnNoteListener{
        void onItemClick(int position);
    }
}
