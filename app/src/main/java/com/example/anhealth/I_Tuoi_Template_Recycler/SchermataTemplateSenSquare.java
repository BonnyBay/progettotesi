package com.example.anhealth.I_Tuoi_Template_Recycler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Classe che mostra i dettagli dei template sensquare, ovvero template REMOTI
 *
 * */
public class SchermataTemplateSenSquare extends AppCompatActivity {
    private  TextView titolo, id, descrizione, parametriTrattati, operazioni;
    private SharedPreferences prefs;
    private Button instanziaTemplateBtn;
    private String url = "";

    private static final String TAG = "SchermataTemplateSenSquare";
    private static final int ERROR_DIALOG_REQUEST = 9001;
    GoogleMap map;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_template_sen_square);


        prefs = getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
        titolo = findViewById(R.id.titTempSens);
        id = findViewById(R.id.idTempl);
        descrizione = findViewById(R.id.idDescrip);
        parametriTrattati = findViewById(R.id.args);
        operazioni = findViewById(R.id.idOpt);
        //instanziaTemplateBtn = findViewById(R.id.instanziaTemplateId);

        int idTemplateDaVisualizzare = prefs.getInt("idTemplateDaVisualizzare", 1);
        Log.e("idTemplateDaVisua", idTemplateDaVisualizzare+"");

        url ="http://sensquare.disi.unibo.it/api/v1/templates/"+idTemplateDaVisualizzare+"/";
        new AsyncMostraDettagliTemplateSensquare().execute();
    }
    public void mostraDettagliTemplate(String url){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("REST response", response.toString());
                        try {
                            int idTemplate = response.getInt("id");
                            String titoloTemplate = response.getString("title");
                            String descrizioneTemplate = response.getString("description");

                            id.setText(Integer.toString(idTemplate));
                            titolo.setText(titoloTemplate);
                            descrizione.setText(descrizioneTemplate);

                            JSONArray jsonArray = response.getJSONArray("args");
                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject results = jsonArray.getJSONObject(i);

                                String argsTemplate = results.getString("id");
                                parametriTrattati.append(argsTemplate+", ");

                                String opt = results.getString("opt");
                                operazioni.append(opt+", ");
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("REST response (error)", error.toString());

                    }
                }
        );
        queue.add(objectRequest);
    }
    /*
    private boolean isServicesOk() {
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(SchermataTemplateSenSquare.this);
        if(available == ConnectionResult.SUCCESS) {
            return true;
        }else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(SchermataTemplateSenSquare.this, available, ERROR_DIALOG_REQUEST);
        }else{
            Toast.makeText(this, "Non puoi fare la richiesta per la mappa", Toast.LENGTH_SHORT).show();
        }
        return false;

    }*/


    public class AsyncMostraDettagliTemplateSensquare extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            mostraDettagliTemplate(url);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

}
