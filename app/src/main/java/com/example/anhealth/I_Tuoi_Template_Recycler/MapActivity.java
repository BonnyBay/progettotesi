package com.example.anhealth.I_Tuoi_Template_Recycler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.google.android.gms.location.FusedLocationProviderClient;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.icu.text.DateFormat;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anhealth.R;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Map;
import java.util.jar.Pack200;

public class MapActivity extends AppCompatActivity{ // DA AGGIUNGERE:  implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener
    /*
    private static final String TAG = "MapActivity";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;

    private Boolean mLocationPermissionGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private double latitudine;
    private double longitudine;

    private Button aumentaRaggio, instanziaTemplateBtn;
    private String raggioInseritoStringa = "100";
    private int raggioInserito = 100;

    private int BACKGROUND_LOCATION_ACCESS_REQUEST_CODE = 10002;

    private LatLng locationTemplate;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mappa);
        mapFragment.getMapAsync(this);
        getLocationPermission();
        aumentaRaggio = findViewById(R.id.aumentaRaggioId);
        instanziaTemplateBtn = findViewById(R.id.instanziaId);

        aumentaRaggio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                costruisciAlertInserimentoRaggio();
            }
        });

        instanziaTemplateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               instanziaTemplate();
            }
        });


    }

    public void instanziaTemplate(){

    }


    public void costruisciAlertInserimentoRaggio(){
        AlertDialog.Builder dialogRaggio = new AlertDialog.Builder(MapActivity.this);
        dialogRaggio.setTitle("Inserisci il raggio");

        final EditText raggioInseritoEdit = new EditText(MapActivity.this);
        raggioInseritoEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialogRaggio.setView(raggioInseritoEdit);

        dialogRaggio.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                raggioInseritoStringa = raggioInseritoEdit.getText().toString();
                raggioInserito = Integer.parseInt(raggioInseritoStringa);
                onResume();
                getDeviceLocation();
            }
        });
        dialogRaggio.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialogRaggio.show();
    }

    public LatLng getDeviceLocation(){ //mi ritorna la posizione corrente
        //ottengo la posizione corrente del device
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            Task location = mFusedLocationProviderClient.getLastLocation();

            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful() ){ //task potrebbe essere null qualora il dispositivo sia nuovo o non sia mai stata usata un app delle MAPPE
                        Log.d(TAG,"onComplete: location trovata! ");
                        Location currentLocation = (Location) task.getResult();
                        //Toast.makeText(MapActivity.this, "currrent"+currentLocation, Toast.LENGTH_SHORT).show();


                        latitudine = currentLocation.getLatitude();
                        longitudine = currentLocation.getLongitude();
                        //Toast.makeText(MapActivity.this, "Posizione corrente trovata"+latitudine+" "+longitudine, Toast.LENGTH_LONG).show();

                        locationTemplate = new LatLng(latitudine, longitudine);
                        addCircle(locationTemplate, raggioInserito);


                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),DEFAULT_ZOOM);
                        //Toast.makeText(MapActivity.this, "Posizione corrente trovata", Toast.LENGTH_SHORT).show();
                        mMap.addCircle(new CircleOptions()
                                .center(new LatLng(latitudine , longitudine))
                                .radius(raggioInserito)
                                .strokeColor(Color.BLUE)
                                .fillColor(Color.argb(100, 0,0,255)));


                    }else{
                        Log.d(TAG,"onComplete: current location è null! ");
                        Toast.makeText(MapActivity.this, "non posso trovare la posizione corrente", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }catch (SecurityException e){
            Log.e(TAG, ""+e.getMessage());
        }
        return locationTemplate;
    }

    private void moveCamera(LatLng latlng, float zoom){
        Log.d(TAG,"move camera lat and lang");
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng,zoom));

    }
    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mappa);
        mapFragment.getMapAsync(MapActivity.this);
    }

    private void getLocationPermission(){
        String [] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mLocationPermissionGranted = true;
                initMap();
            }else{
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else{
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionGranted = true;
                    initMap();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Toast.makeText(this, "La mappa è pronta", Toast.LENGTH_SHORT).show();

        if(mLocationPermissionGranted){
            locationTemplate = getDeviceLocation();

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                return;

            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

            Log.d(TAG, "LOCATION TEMPLATE "+locationTemplate);


            //addCircle(locationTemplate, raggioInserito);
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        Toast.makeText(MapActivity.this, "Raggio "+raggioInserito, Toast.LENGTH_LONG).show();

        if(mMap != null){ //prevent crashing if the map doesn't exist yet (eg. on starting activity)
            mMap.clear();

            // add markers from database to the map
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(MapActivity.this, "Raggio "+raggioInserito, Toast.LENGTH_LONG).show();

    }

    private void addCircle(LatLng latLng, float radius){
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(radius);
        circleOptions.strokeColor(Color.argb(255, 0, 128, 0));
        circleOptions.fillColor(Color.argb(64, 0, 128, 0));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Toast.makeText(this, "CLick", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "click lungo 1");
        if(Build.VERSION.SDK_INT >= 29){
            //ho bisogno dei permessi di background
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED){
                //handleMapLongClick(latLng);
                handleMapLongClick(locationTemplate);
                Log.d(TAG, "click lungo 2");

            }else{
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)){
                    //mostro un dialog e chiedo i permessi
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                    Log.d(TAG, "click lungo 3");

                }else{
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, BACKGROUND_LOCATION_ACCESS_REQUEST_CODE);
                    Log.d(TAG, "click lungo 4");

                }
            }
        }else{
            //handleMapLongClick(latLng);
            handleMapLongClick(locationTemplate);
            Log.d(TAG, "click lungo 5");


        }
    }
    private void handleMapLongClick(LatLng latLng){
        mMap.clear(); //tolgo i marker precedenti, COSI POSIZIONO UN SOLO MARKER SULLA MAPPA
        //addMarker(latLng);
        addCircle(latLng, raggioInserito);
    }

*/


}
