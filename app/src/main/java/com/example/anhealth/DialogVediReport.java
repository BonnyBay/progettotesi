package com.example.anhealth;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.ArrayList;

public class DialogVediReport extends AppCompatDialogFragment {
    ArrayList<String> data = new ArrayList<>();
    public DialogVediReport(ArrayList<String> data){
        this.data = data;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        for(int i = 0; i<data.size(); i++){
            builder.setTitle("Report").setMessage("Ecco i report che hai pubblicato in questa giornata: "+data.get(i)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //non faccio niente
                }
            });
        }
        return builder.create();
    }
}
