package com.example.anhealth;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.room.Room;

import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.DatoUtente;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NoticationBroadcastreceiver extends BroadcastReceiver {
    private final String CHANNEL_ID = "report_notification";
    private AppDatabase appDatabase;
    private List<DatoUtente> datoUtente;
    boolean presenzaReportInDB = false;
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "datiReportDB").build();
        //per prima cosa controllo che nel DB non sia presente la data di oggi,
        //per controllare se sono stati inseriti o meno dei report
        //controllo se sono stati inseriti dei report oggi, se no lancio una notifica
        new AsyncTaskGetDati().execute();

    }
    public void checkReportInDB(){ //metodo che controlla se nel db sono stati inseriti dei report nella data odierna
        Date dataOggi = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(dataOggi); // data di oggi in stringa
        //List<DatoUtente> datoUtente = appDatabase.dataAccessObject().getDati();
        for (DatoUtente dato : datoUtente) {
            String datReport = dato.getDataInserimento();

            if (datReport.equals(formattedDate)) {
                presenzaReportInDB = true;
                break;
            }
        }
    }
    public void gestisciNotifica(Context context){
        if (presenzaReportInDB == false) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent repeating_intent = new Intent(context, MainActivity.class);
            repeating_intent.putExtra("fromNotify",true);
            repeating_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, repeating_intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent posponi = new Intent(context, SettingsActivity.class);
            posponi.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //faccio si che la nuova activity sostiuisca la vecchia
            PendingIntent posponiPendInt = PendingIntent.getActivity(context, 100, posponi, PendingIntent.FLAG_UPDATE_CURRENT);

            //test
            SharedPreferences prefs = context.getSharedPreferences("sharedPreferenceDiAnHealth", Context.MODE_PRIVATE);
            int ore = prefs.getInt("orarioOre", 0);
            int minuti = prefs.getInt("orarioMin", 0);

            //fine test

            NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID);

            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setSmallIcon(R.drawable.ic_notification);
            mBuilder.setContentTitle("Non hai inserito nessun report oggi");
            mBuilder.setContentText("Aggiungi un report! notifica delle:"  + ore +":" + minuti);
            mBuilder.setPriority(Notification.PRIORITY_MAX);
            mBuilder.setStyle(bigText);
            mBuilder.setAutoCancel(true); // se l'utente tocca la notifica, essa si chiude subito
            mBuilder.addAction(R.drawable.ic_launcher_background, "Aggiungi il report", pendingIntent);
            mBuilder.addAction(R.drawable.ic_launcher_background, "Posponi notifica", posponiPendInt);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(
                        CHANNEL_ID,
                        "Titolo canale",
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
                mBuilder.setChannelId(CHANNEL_ID);
                Log.i("Notify", "Alarm"); //Optional, used for debuging.
            }
            notificationManager.notify(100, mBuilder.build());
        }
    }
    public class AsyncTaskGetDati extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            datoUtente = appDatabase.dataAccessObject().getDati();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            checkReportInDB();
            gestisciNotifica(context);
        }
    }


}
