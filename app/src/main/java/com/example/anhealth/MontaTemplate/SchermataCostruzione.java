package com.example.anhealth.MontaTemplate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.room.Room;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anhealth.AggiungiDatoUtenteFragment;
import com.example.anhealth.Database.AppDatabase;
import com.example.anhealth.Database.CustomServiceTemplateLocal;
import com.example.anhealth.Database.DataClassesLocal;
import com.example.anhealth.Database.DatoUtente;
import com.example.anhealth.Database.Regola;
import com.example.anhealth.MainActivity;
import com.example.anhealth.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.Callback;

public class SchermataCostruzione extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "Schermata Costruzione";
    LinearLayout layoutList;
    Button buttonAddSensSquare, buttonAddTuoiParametri, costruisciBtn;
    private String url = "";
    List<String> teamList = new ArrayList<String>();
    List<String> teamList2 = new ArrayList<String>();

    List<String> tipiDiParametro = new ArrayList<String>();
    List<DataClassesLocal> tipiDiParametroLocali = new ArrayList<DataClassesLocal>();
    private ImageView imageClose;
    private String urlSecondaPagina = "";
    public int regoleInserite = 0; //contatore che tiene traccia di quante regole sono state inserite, e dalla seconda, aggiunge lo spinner centrale
    private static AppDatabase appDatabase;
    CustomServiceTemplateLocal template;
    public Regola regola;
    public ArrayAdapter arrayAdapter1, arrayAdapter2;
    public boolean nomeTemplateInserito = false; //serve per capire se il nome del template è già stato salvato nel database
    boolean tuttiICampiSonoCompletati = false; //serve per capire se tutti i campi sono pieni o meno

    EditText editText,titoloTemplate;
    AppCompatSpinner spinnerTeam1;
    AppCompatSpinner spinnerTeam2;
    AppCompatSpinner spinnerTeam4;

    private List<CustomServiceTemplateLocal> mTemplate; //conterrà i template ritornarti dal db grazie all'asynctasck

    long idUltimoTemplate;
    ArrayList<View> lastViewInserted = new ArrayList<View>(); //mi servirà per rimuovere l'ultima regola inserita

    ArrayList<Regola> regoleDaAggiungere = new ArrayList<Regola>(); //contiene le regole che saranno da aggiungere quando l'utente premerà su costruisci template
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schermata_costruzione);

        appDatabase = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "datiReportDB").build();

        regola = new Regola();

        layoutList = findViewById(R.id.layout_list);
        buttonAddSensSquare = findViewById(R.id.btnAddParSensquare);
        buttonAddSensSquare.setOnClickListener(this);

        buttonAddTuoiParametri = findViewById(R.id.buttonAddTuoiParametri);
        buttonAddTuoiParametri.setOnClickListener(this);

        costruisciBtn = findViewById(R.id.idCostruisci);

        costruisciBtn.setOnClickListener(this);

        teamList.add("=");
        teamList.add("<");
        teamList.add(">");
        teamList.add("+");
        teamList.add("-");
        teamList.add("*");
        teamList.add("/");

        teamList2.add("AND");
        teamList2.add("OR");
        teamList2.add("=");
        teamList2.add("<");
        teamList2.add(">");
        /*
        teamList2.add("+");
        teamList2.add("-");
        teamList2.add("*");
        teamList2.add("/");*/


        url = "http://sensquare.disi.unibo.it/api/v1/showDataClasses";
        //new AsyncTaskAPIOttieniDataClasses().execute();
        avviaAPIDataClasses(url);  //riempie l'array list dei parametri
        costruisciBtn.setEnabled(false);
        buttonAddTuoiParametri.setEnabled(false); //se no non funziona, è stato pensato per usare almeno un parametro di sensquare
        new AsyncTaskRecuperaDataClass().execute();


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAddParSensquare){ //DEVO DISABILITARE IL BOTTONE SE NON HA PREMUTO COSTRUISCI
            addViewSensquare();
            buttonAddSensSquare.setEnabled(false);
            buttonAddTuoiParametri.setEnabled(false);
            costruisciBtn.setEnabled(true);
            imageClose.setEnabled(false);


            //tuttiICampiSonoCompletati = false;
            if(regoleInserite == 1){
                controlloCampiPieni1(spinnerTeam1,spinnerTeam2,editText);
            }else if( regoleInserite >= 2){
                controlloCampiPieni2(spinnerTeam1,spinnerTeam2,editText,spinnerTeam4);
            }


        }else if(v.getId() == R.id.buttonAddTuoiParametri){
            //DA SISTEMARE CON LE COSE CORRETTE DEI TEMPLATE DEI TUOI REPORT
            buttonAddSensSquare.setEnabled(false);
            buttonAddTuoiParametri.setEnabled(false);
            costruisciBtn.setEnabled(true);
            addViewLocal();
            imageClose.setEnabled(false);


            if(regoleInserite == 1){
                controlloCampiPieni1(spinnerTeam1,spinnerTeam2,editText);
            }else if( regoleInserite >= 2){
                controlloCampiPieni2(spinnerTeam1,spinnerTeam2,editText,spinnerTeam4);
            }

        }else if(v.getId() == R.id.idCostruisci){//se ha premuto sul pulsante per confermare la costruzione del template
            //COME PRIMA COSA: salvo l'ultima regola inserita
            if(regoleInserite == 1){
                controlloCampiPieni1(spinnerTeam1,spinnerTeam2,editText);
            }else if( regoleInserite >= 2){
                imageClose.setEnabled(true);
                controlloCampiPieni2(spinnerTeam1,spinnerTeam2,editText,spinnerTeam4);
            }
            if(!tuttiICampiSonoCompletati){
                Toast.makeText(getApplicationContext(), "errore CI SONO CAMPI VUOTI. Regole inserite: "+ regoleInserite,
                        Toast.LENGTH_LONG).show();
            }

            //aggiungo le regole alla tabella delle regole ed i template alla tabella dei template
            titoloTemplate = findViewById(R.id.titoloTemplateInserito);
            //controllo che sia stato inserito il titolo del template
            if(titoloTemplate.getText().length() != 0 && nomeTemplateInserito==false) { //se il titolo del template non è ancora stato inserito..
                template = new CustomServiceTemplateLocal();
                template.setTitolo(titoloTemplate.getText().toString());
                if(tuttiICampiSonoCompletati){
                    new AsyncTaskAggiungiTemplate().execute(template);
                }

                nomeTemplateInserito = true;
            }else if(nomeTemplateInserito ==true){
                if(tuttiICampiSonoCompletati){
                    new AsyncTaskGetTemp().execute();
                }
            }
        }
    }

    private void controlloCampiPieni1(AppCompatSpinner spinnerTeam1, AppCompatSpinner spinnerTeam2, EditText editText) {
        Double costante = 0.0;
        if(spinnerTeam1.getSelectedItem().toString().length() != 0){
            regola.setParametro(spinnerTeam1.getSelectedItem().toString());
            if(spinnerTeam2.getSelectedItem().toString().length() != 0){
                regola.setOperatorePrimo(spinnerTeam2.getSelectedItem().toString());
                if(editText.getText().length() != 0){
                    costante = Double.parseDouble(editText.getText().toString());
                    regola.setCostante(costante);
                    regola.setOperatoreSecondo("Nessun operatore inserito");
                    tuttiICampiSonoCompletati = true;
                    regoleDaAggiungere.add(regola); // non le aggiungo ancora al db
                    buttonAddSensSquare.setEnabled(true);
                    buttonAddTuoiParametri.setEnabled(true);
                    costruisciBtn.setEnabled(false);

                }else{
                    tuttiICampiSonoCompletati = false;
                }
            }else{
                tuttiICampiSonoCompletati = false;
            }
        }else{
            tuttiICampiSonoCompletati = false;
        }
    }
    private void controlloCampiPieni2(AppCompatSpinner spinnerTeam1, AppCompatSpinner spinnerTeam2, EditText editText, AppCompatSpinner spinnerTeam4) {
        Double costante = 0.0;
        //if per verificare che non siano vuoti
        if(spinnerTeam1.getSelectedItem().toString().length() != 0){
            regola.setParametro(spinnerTeam1.getSelectedItem().toString());
            if(spinnerTeam2.getSelectedItem().toString().length() != 0){
                regola.setOperatorePrimo(spinnerTeam2.getSelectedItem().toString());
                if(editText.getText().length() != 0){
                    costante = Double.parseDouble(editText.getText().toString());
                    regola.setCostante(costante);
                    if(spinnerTeam4.getSelectedItem().toString().length() != 0){
                        regola.setOperatoreSecondo(spinnerTeam4.getSelectedItem().toString());
                        tuttiICampiSonoCompletati = true;
                        regoleDaAggiungere.add(regola);
                        buttonAddSensSquare.setEnabled(true);
                        buttonAddTuoiParametri.setEnabled(true);
                        costruisciBtn.setEnabled(false);
                    }else{
                        tuttiICampiSonoCompletati = false;
                    }
                }else{
                    tuttiICampiSonoCompletati = false;
                }
            }else{
                tuttiICampiSonoCompletati = false;
            }
        }else{
            tuttiICampiSonoCompletati = false;
        }
    }
    private void addViewSensquare(){
        final View cricketerView = getLayoutInflater().inflate(R.layout.nuova_regola_costruisci_template,null,false);
        editText = cricketerView.findViewById(R.id.edit_cricker_name2);
        spinnerTeam1 = cricketerView.findViewById(R.id.spinner_team1);
        spinnerTeam2 = cricketerView.findViewById(R.id.spinner_team2);
        //AppCompatSpinner spinnerTeam3 = cricketerView.findViewById(R.id.spinner_team3);
        spinnerTeam4 = cricketerView.findViewById(R.id.spinner_team4);
        if(regoleInserite < 1){
            spinnerTeam4.setVisibility(View.GONE); //nascondo lo spinner all'inizio, quando c'è solo una regola
        }else{ //se è già stata inserita una regola ...
            spinnerTeam4.setVisibility(View.VISIBLE);

            ArrayAdapter arrayAdapter4 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, teamList2);
            spinnerTeam4.setAdapter(arrayAdapter4);
        }
        imageClose = (ImageView) cricketerView.findViewById(R.id.image_remove);

        arrayAdapter1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tipiDiParametro);
        spinnerTeam1.setAdapter(arrayAdapter1);

        arrayAdapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, teamList);
        spinnerTeam2.setAdapter(arrayAdapter2);
        lastViewInserted.add(cricketerView);
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeViewSensSquare(lastViewInserted.get(lastViewInserted.size()-1));
            }
        });
        layoutList.addView(cricketerView);
        regoleInserite++;
    }

    private void addViewLocal(){
        final View cricketerView = getLayoutInflater().inflate(R.layout.nuova_regola_costruisci_template,null,false);
        editText = cricketerView.findViewById(R.id.edit_cricker_name2);
        spinnerTeam1 = cricketerView.findViewById(R.id.spinner_team1);
        spinnerTeam2 = cricketerView.findViewById(R.id.spinner_team2);
        //AppCompatSpinner spinnerTeam3 = cricketerView.findViewById(R.id.spinner_team3);
        spinnerTeam4 = cricketerView.findViewById(R.id.spinner_team4);
        if(regoleInserite < 1){
            spinnerTeam4.setVisibility(View.GONE); //nascondo lo spinner all'inizio, quando c'è solo una regola
        }else{ //se è già stata inserita una regola ...
            spinnerTeam4.setVisibility(View.VISIBLE);

            ArrayAdapter arrayAdapter4 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, teamList2);
            spinnerTeam4.setAdapter(arrayAdapter4);
        }
        imageClose = (ImageView) cricketerView.findViewById(R.id.image_remove);

        arrayAdapter1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, parametri_tuoi);
        spinnerTeam1.setAdapter(arrayAdapter1);

        arrayAdapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, teamList);
        spinnerTeam2.setAdapter(arrayAdapter2);

        lastViewInserted.add(cricketerView);
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeViewSensSquare(lastViewInserted.get(lastViewInserted.size()-1));
            }
        });
        layoutList.addView(cricketerView);
        regoleInserite++;
    }

    private void removeViewSensSquare(View view){
        if(regoleInserite > 1){//deve esserci almeno una regola
            layoutList.removeView(view);
            lastViewInserted.remove(view);//lo rimuovo anche dal vettore
            Log.d(TAG, "regole inserite: "+regoleInserite+" regoleDaAggiungere.size "+regoleDaAggiungere.size());
            regoleDaAggiungere.remove(regoleDaAggiungere.size()-1); //rimuovo l'ultima regola inserita quando l'utente preme sulla "x"
            //regoleDaAggiungere.remove(lastViewInserted.get(lastViewInserted.size()-1));
            //la rimuovo anche dal database
            Log.d(TAG, "regoleDaAggiungere.get(regoleDaAggiungere.size()-1): "+regoleDaAggiungere.get(regoleDaAggiungere.size()-1).getParametro());
            Log.d(TAG, "regoleDaAggiungere.get(regoleDaAggiungere.size()-1): "+regoleDaAggiungere.get(regoleDaAggiungere.size()-1).getId());

            //in questo modo ottengo le regole, e l'id al suo interno non è piu zero al contrario dell id contenuto dalle regole nel vettore regoleDaAggiungere
            new AsyncGetRegole().execute();//nell on post rimuoverò la regola

            regoleInserite--;
        }
    }

    public void avviaAPIDataClasses(String urlPaginaDaMostrare){ //metodo che recupera da sensquare i tipi di dato (dataclasses)
            RequestQueue queue = Volley.newRequestQueue(this);
            JsonObjectRequest objectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    urlPaginaDaMostrare,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONArray jsonArray = response.getJSONArray("results");

                                for(int i = 0; i<jsonArray.length(); i++) {
                                    JSONObject results = jsonArray.getJSONObject(i);
                                    String id = results.getString("id");
                                    String dataType = results.getString("data_type");
                                    tipiDiParametro.add(id);
                                }
                                String urlPaginaSuccessiva = response.getString("next");
                                if(!urlPaginaSuccessiva.equals("")){ // se non è vuoto..
                                    urlSecondaPagina = urlPaginaSuccessiva;

                                    avviaAPIDataClasses(urlSecondaPagina); //qui l'url sarà diverso dal primo, infatti conterrà le indicazioni per la pagina successiva da caricare
                                }

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("REST response (error)", error.toString());
                        }
                    }
            );
            queue.add(objectRequest);
    }

    //async per le regole
    public class AsyncTaskAggiungiRegola extends AsyncTask<Regola, Void, Void> {
        @Override
        protected Void doInBackground(Regola... regole) {
            appDatabase.daoRegole().aggiungiRegola(regole[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "regoleDaAggiungere.size(): "+regoleDaAggiungere.size(),
                    //Toast.LENGTH_SHORT).show();
            Log.d(TAG, "regoleDaAggiungere.size(): "+ regoleDaAggiungere.size());
        }
    }
    //async task per la rimozione delle regole
    public class AsyncTaskRimuoviRegola extends AsyncTask<Regola, Void, Void> {
        @Override
        protected Void doInBackground(Regola... regole) {
            appDatabase.daoRegole().rimuoviRegola(regole[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getApplicationContext(), "regoleDaAggiungere.size(): "+regoleDaAggiungere.size(),
                    //Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Regola rimossa");
        }
    }


    //async per il template
    public class AsyncTaskAggiungiTemplate extends AsyncTask<CustomServiceTemplateLocal, Void, Void> {
        @Override
        protected Void doInBackground(CustomServiceTemplateLocal... templates) {
            appDatabase.daoCustomServiceTemplateLocal().aggiungiTemplate(templates[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Template aggiunto ",
                    Toast.LENGTH_SHORT).show();
            //IMPORTANTE
            new AsyncTaskGetTemp().execute(); //ottengo i template e li metto nella lista in mTemplate
        }
    }

    //async per il  ottenere  i template (a me servirà solo l'id dell'ultimo template inserito)
    public class AsyncTaskGetTemp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            mTemplate = appDatabase.daoCustomServiceTemplateLocal().getTemplates();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) { //OTTENGO il template per recuperare il suo id
            super.onPostExecute(aVoid);
            int numTemplate = 0;
            //ora devo cercare di ottenere l'id dell'ultimo template e salvarlo nel apposito campo della tabella REGOLA
            for(CustomServiceTemplateLocal tem : mTemplate){
                numTemplate++;
            }
            idUltimoTemplate = mTemplate.get(numTemplate-1).getId();

            regola.setTemplate_id(idUltimoTemplate);
            new AsyncTaskAggiungiRegola().execute(regoleDaAggiungere.get(regoleInserite-1));
        }
    }
    //async task che si occupa di eseguire la chiamata API per ottenere le dataclasses da sensquare
    public class AsyncTaskAPIOttieniDataClasses extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            avviaAPIDataClasses(url); //riempie l'array list dei parametri
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }

    List<String> parametri_tuoi = new ArrayList<String>();
    public class AsyncTaskRecuperaDataClass extends AsyncTask<DataClassesLocal, Void, Void> {
        @Override
        protected Void doInBackground(DataClassesLocal... dataClassesLocal) {
            tipiDiParametroLocali = appDatabase.daoDataClassesLocal().getDataClassesLocal();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Toast.makeText(getActivity(), "Data Class aggiunta",
            //      Toast.LENGTH_LONG).show();
            Log.d("AggiungiDatoUtenteFrag", "Data class ottenuti ");
            for(DataClassesLocal mDataClass : tipiDiParametroLocali){
                parametri_tuoi.add(mDataClass.getData_type());
            }
        }

    }


    //mi serve per recuperare l'id delle regole
    private List<Regola> regoleRecuperate = new ArrayList<Regola>();
    public class AsyncGetRegole extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            regoleRecuperate = appDatabase.daoRegole().getRegole();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) { //OTTENGO il template per recuperare il suo id
            super.onPostExecute(aVoid);
            Log.d(TAG, "regoleRecuperate.get(regoleDaAggiungere.size()-1).id: "+regoleRecuperate.get(regoleDaAggiungere.size()-1).getId());
            new AsyncTaskRimuoviRegola().execute(regoleRecuperate.get(regoleRecuperate.size()-1));//rimuovo l'ultima regola inserita

        }
    }
}



